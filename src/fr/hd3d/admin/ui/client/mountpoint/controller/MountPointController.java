package fr.hd3d.admin.ui.client.mountpoint.controller;

import java.util.List;
import java.util.Map;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.Record;

import fr.hd3d.admin.ui.client.controller.SubController;
import fr.hd3d.admin.ui.client.mountpoint.model.MountPointModel;
import fr.hd3d.admin.ui.client.mountpoint.view.MountPointTab;
import fr.hd3d.common.ui.client.error.Hd3dException;

import fr.hd3d.common.ui.client.modeldata.asset.DynamicPathModelData;
import fr.hd3d.common.ui.client.modeldata.asset.MountPointModelData;
import fr.hd3d.common.ui.client.service.BulkRequests;


public class MountPointController extends SubController
{
    private final MountPointTab view;

    private final MountPointModel model;

    public MountPointController(MountPointTab tab, MountPointModel model)
    {
        super(tab);
        view = tab;
        this.model = model;
    }

    @Override
    protected void registerEvents()
    {
        super.registerEvents();
        registerEventTypes(MountPointEvents.TAB_MOUNT_POINT_SELECTED);
        registerEventTypes(MountPointEvents.ADD_MOUNTPOINT_CLICKED);
        registerEventTypes(MountPointEvents.DELETE_MOUNTPOINT_CLICKED);
        registerEventTypes(MountPointEvents.DELETE_MOUNTPOINT_CONFIRMED);
        registerEventTypes(MountPointEvents.AFTER_EDIT);
        registerEventTypes(MountPointEvents.REFRESH_CLICKED);
    }

    @Override
    protected void doHandleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == MountPointEvents.TAB_MOUNT_POINT_SELECTED)
        {
            this.model.reload();
        }
        else if (type == MountPointEvents.ADD_MOUNTPOINT_CLICKED)
        {
            this.addMountPointClicked();
        }
        else if (type == MountPointEvents.DELETE_MOUNTPOINT_CLICKED)
        {
            this.deleteMountPointClicked();
        }
        else if (type == MountPointEvents.DELETE_MOUNTPOINT_CONFIRMED)
        {
            this.deleteMountPointConfirmed();
        }
        else if (type == MountPointEvents.AFTER_EDIT)
        {
            this.afterEdit();
        }
        else if (type == MountPointEvents.REFRESH_CLICKED)
        {
            this.model.reload();
        }
    }

    private void afterEdit()
    {
        try
        {
            List<MountPointModelData> mountPoints = this.view.getMountPointSelected();
            List<Record> records = this.model.getMountPointStore().getModifiedRecords();
            if (records == null || records.isEmpty())
            {
                throw new Hd3dException("No data is modified.");
            }
            Record modifiedRecord = records.get(0);
            if (modifiedRecord == null || modifiedRecord.getChanges() == null)
            {
                throw new Hd3dException("No data is modified.");
            }

            Map<String, Object> change = modifiedRecord.getChanges();
            for (MountPointModelData mountPoint : mountPoints)
            {
                for (String key : change.keySet())
                {
                    mountPoint.set(key, modifiedRecord.get(key));
                }
            }
            BulkRequests.bulkPut(mountPoints, MountPointEvents.REFRESH_CLICKED);
            this.view.deselectGrid();
        }
        catch (Hd3dException e)
        {
            e.print();
        }
    }

    private void deleteMountPointConfirmed()
    {
        try
        {
            List<MountPointModelData> selection = this.view.getMountPointSelected();

            for (MountPointModelData mountPoint : selection)
            {
                mountPoint.delete(MountPointEvents.REFRESH_CLICKED);
            }
            this.view.deselectGrid();
        }
        catch (Hd3dException e)
        {
            e.print();
        }
    }

    private void deleteMountPointClicked()
    {
        this.view.confirmDelete(MountPointEvents.DELETE_MOUNTPOINT_CONFIRMED);
    }

    private void addMountPointClicked()
    {
        this.view.displayMountPointCreator();
    }

}
