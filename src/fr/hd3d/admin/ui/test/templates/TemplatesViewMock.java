package fr.hd3d.admin.ui.test.templates;

import fr.hd3d.admin.ui.client.controller.SubController;
import fr.hd3d.admin.ui.client.templates.view.IRoleTemplateView;
import fr.hd3d.admin.ui.client.templates.view.ISecurityTemplateView;
import fr.hd3d.admin.ui.client.view.ISubView;


public class TemplatesViewMock implements ISubView
{

    IRoleTemplateView own;
    IRoleTemplateView all;
    ISecurityTemplateView sec;

    public boolean init = false;

    public TemplatesViewMock(IRoleTemplateView own, IRoleTemplateView all, ISecurityTemplateView sec)
    {
        this.own = own;
        this.all = all;
        this.sec = sec;
    }

    public String getHeading()
    {
        return "heading";
    }

    public SubController init()
    {
        // no need for test
        return null;
    }

    public void initWidgets()
    {
        own.setup();
        all.setup();
        sec.setup();

        init = true;
    }

}
