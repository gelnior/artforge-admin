package fr.hd3d.admin.ui.test.perms;

import org.junit.Test;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.model.modeldata.SingleGenericReader;
import fr.hd3d.admin.ui.client.model.modeldata.SingleRoleReader;
import fr.hd3d.admin.ui.client.perms.model.PermModelData;
import fr.hd3d.admin.ui.client.perms.model.PermsTreeStoreUtil;
import fr.hd3d.admin.ui.client.rights.controller.RightsPermsController;
import fr.hd3d.admin.ui.client.rights.model.RightsPermsModel;
import fr.hd3d.admin.ui.test.modeldata.GenericReaderMock;
import fr.hd3d.admin.ui.test.modeldata.RolePagingReaderMock;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dModelDataReaderSingleton;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;
import fr.hd3d.common.ui.client.userrights.UserRightsResolver;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.widget.sheeteditor.mock.Hd3dModelDataReaderMock;


public class PermsControllerTest extends UITestCase
{

    RightsPermsModel pM;
    PermsViewMock pV;

    public void setUp()
    {
        super.setUp();

        // set super user
        PermissionUtil.setDebugOn();

        // set mock readers
        SingleRoleReader.setSimpleInstance(new RolePagingReaderMock());
        SingleGenericReader.setInstance(new GenericReaderMock());
        Hd3dModelDataReaderSingleton.setInstanceAsMock(new Hd3dModelDataReaderMock());

        // perms n bans
        pM = new RightsPermsModel();
        pV = new PermsViewMock(pM);
        RightsPermsController controller = new RightsPermsController(pM, pV);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    private void init()
    {
        PermsTreeStoreUtil.initServiceURLs();
        UserRightsResolver.acquireUserRights();

        EventDispatcher.forwardEvent(AdminEvents.SERVICE_URLS_INITIALIZED);
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);
    }

    private RoleModelData selectRole()
    {
        RoleModelData role = new RoleModelData("new role");
        role.getPermissions().add("v1:projects:12:*:create");
        role.getPermissions().add("v1:projects:*:read");
        role.getPermissions().add("*:persons:*:read");
        role.getBans().add("*:persons:2451:*:create,update");

        AppEvent sel = new AppEvent(AdminEvents.ROLE_SELECTED);
        sel.setData(role);
        EventDispatcher.forwardEvent(sel);
        return role;
    }

    @Test
    public void testInit()
    {
        init();

        assertTrue(pV.init);
    }

    @Test
    public void testSelectRole()
    {
        init();

        RoleModelData r = selectRole();

        assertEquals(r.getName(), pV.roleName);
        assertTrue(pM.getStore().contains(new PermModelData("v1", PermModelData.VERSION, "v1")));
        assertTrue(pV.gridShown);

        EventDispatcher.forwardEvent(AdminEvents.ALL_ROLES_CLICKED);

        assertEquals(pV.roleName, "");
        assertNull(pM.getRole());
        assertTrue(!pV.gridShown);
        assertTrue(pM.getStore().getChildCount() == 0);
    }

    @Test
    public void testChangeRight()
    {
        init();

        selectRole();

        PermModelData root = pM.getStore().getRootItems().get(0);
        PermModelData all = pM.getStore().getFirstChild(root); // gets the first 'ALL' node

        assertEquals(all.getCanDelete().getValue(), PermModelData.DEFAULT);

        AppEvent del = new AppEvent(pM.canDeleteClickedEvent());
        del.setData(all);
        EventDispatcher.forwardEvent(del);
        assertEquals(all.getCanDelete().getValue(), PermModelData.PERMITTED);

        EventDispatcher.forwardEvent(del);
        assertEquals(all.getCanDelete().getValue(), PermModelData.BANNED);

        EventDispatcher.forwardEvent(pM.undoEvent());
        EventDispatcher.forwardEvent(pM.undoEvent());
        assertEquals(all.getCanDelete().getValue(), PermModelData.DEFAULT);

        EventDispatcher.forwardEvent(pM.redoEvent());
        assertEquals(all.getCanDelete().getValue(), PermModelData.PERMITTED);

        EventDispatcher.forwardEvent(pM.undoEvent());
        assertEquals(all.getCanDelete().getValue(), PermModelData.DEFAULT);
    }

}
