package fr.hd3d.admin.ui.client.templates.model;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.common.ui.client.modeldata.technical.ProjectSecurityTemplateModelData;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;


/**
 * The specific model for handling the role templates owned by the selected security template. It provides a method for
 * removing roles from the security template.
 * 
 * @author HD3D
 * 
 */
public class OwnedRoleModel extends RoleTemplateModel
{

    public OwnedRoleModel(SecurityTemplateModel parent)
    {
        super(parent);
        parent.setOwned(this);
    }

    /**
     * Removes selected role from selected security template, actually removing the role from the owned store, adding it
     * to the other store and saving the security template afterwards.
     */
    public void removeRoleFromTemplate()
    {
        ProjectSecurityTemplateModelData template = getSelectedTemplate();
        template.getRoleIds().remove(selected.getId());
        store.remove(selected);
        parent.getAll().getStore().add(selected);
        selected = null;
        template.save();
    }

    public EventType refreshNeededEvent()
    {
        return AdminEvents.TEMP_OWN_SERVER_MODIFICATION;
    }

    public void refreshData(List<IUrlParameter> constraints)
    {
        ProjectSecurityTemplateModelData template = getSelectedTemplate();
        if (template != null && template.getRoleIds() != null && !template.getRoleIds().isEmpty())
        {
            super.refreshData(constraints);
        }
        else
        {
            store.removeAll();
        }
    }

    public EventType addingClickEvent()
    {
        return AdminEvents.REMAIN_IDLE;
    }

    public EventType creationClickEvent()
    {
        return AdminEvents.TEMP_OWN_CREATE_CLICKED;
    }

    public EventType creationConfirmEvent()
    {
        return AdminEvents.TEMP_OWN_CREATE_CONFIRMED;
    }

    public EventType deleteClickEvent()
    {
        return AdminEvents.TEMP_OWN_DELETE_CLICKED;
    }

    public EventType editionClickEvent()
    {
        return AdminEvents.TEMP_OWN_EDIT_CLICKED;
    }

    public EventType editionConfirmEvent()
    {
        return AdminEvents.TEMP_OWN_EDIT_CONFIRMED;
    }

    public EventType removeClickEvent()
    {
        return AdminEvents.TEMP_OWN_REMOVE_CLICKED;
    }

    public EventType allEventType()
    {
        return AdminEvents.REMAIN_IDLE;
    }

    public EventType selectionEventType()
    {
        return AdminEvents.TEMP_OWN_SELECTED;
    }

    public EventType refreshAllEvent()
    {
        return AdminEvents.TEMP_OWN_REFRESH_CLICKED;
    }

}
