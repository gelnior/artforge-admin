package fr.hd3d.admin.ui.client.users.view;

import com.extjs.gxt.ui.client.binding.FieldBinding;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldSetEvent;
import com.extjs.gxt.ui.client.event.Listener;

import fr.hd3d.admin.ui.client.users.model.AccountFormModel;
import fr.hd3d.admin.ui.client.view.AdminMainView;


/**
 * A dialog for user account creation. Account password can optionally be specified by expanding the dialog. If not, the
 * server will automatically set a default password.
 * 
 * @author HD3D
 * 
 */
public class CreateAccountDialog extends AccountFormDialog
{

    public CreateAccountDialog(String username)
    {
        super(AdminMainView.CONSTANTS.CreateAccountDialog(), username, true);
    }

    protected void setFormFields()
    {
        subFieldSet.setHeading(AdminMainView.CONSTANTS.DefineAccountPassword());
        subFieldSet.setCheckboxToggle(false);
        subFieldSet.setExpanded(false);

        super.setFormFields();

        subFieldSet.addListener(Events.BeforeExpand, new ExpandListener());
        subFieldSet.addListener(Events.BeforeCollapse, new ExpandListener());
    }

    protected void setFieldBinding()
    {
        passwordBinding.addFieldBinding(new FieldBinding(password, AccountFormModel.PASSWORD_ATTRIBUTE));

        super.setFieldBinding();
    }

    class ExpandListener implements Listener<FieldSetEvent>
    {
        public void handleEvent(FieldSetEvent be)
        {
            if (be.getType() == Events.BeforeExpand)
            {
                password.setAllowBlank(false);
                repeat.setAllowBlank(false);
            }
            if (be.getType() == Events.BeforeCollapse)
            {
                password.setAllowBlank(true);
                repeat.setAllowBlank(true);
            }
        }
    }

}
