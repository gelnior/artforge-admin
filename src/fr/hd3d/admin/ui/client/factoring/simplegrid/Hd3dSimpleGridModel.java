package fr.hd3d.admin.ui.client.factoring.simplegrid;

import java.util.List;

import com.extjs.gxt.ui.client.data.PagingLoader;

import fr.hd3d.admin.ui.client.config.AdminConfig;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IPagingReader;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.store.BaseStore;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.service.store.ServicesPagingStore;


/**
 * A simple model for accessing and storing HD3D server data with or without paging.
 * 
 * @author HD3D
 * 
 * @param <D>
 *            the model data type
 */
public class Hd3dSimpleGridModel<D extends Hd3dModelData>
{
    /** the model data store */
    protected BaseStore<D> store;
    protected boolean pagingMode = false;

    private String path;

    /**
     * Creates a <code>Hd3dSimpleGridModel</code> with or without paging.
     */
    public Hd3dSimpleGridModel()
    {}

    /**
     * Initializes the model with a paging proxy and a paging loader.
     * 
     * @param reader
     *            the suitable paging reader for the data type
     */
    public void initWithPaging(IPagingReader<D> reader)
    {
        pagingMode = true;
        store = new ServicesPagingStore<D>(reader);
        store.setPath(path);
        ((ServicesPagingStore<D>) store).setLimit(AdminConfig.PAGING_LIMIT);
        store.reload();

    }

    /**
     * Initializes the model with a simple list proxy and a simple list loader.
     * 
     * @param reader
     *            the suitable list reader for the data type
     */
    public void initWithoutPaging(IReader<D> reader)
    {
        pagingMode = false;
        store = new ServiceStore<D>(reader);
        store.setPath(path);
        store.reload();
    }

    /**
     * Sets the default server path for data acquisition.
     * 
     * @param path
     */
    public void setPath(String path)
    {
        this.path = path;
        if (store != null)
            store.setPath(path);
    }

    /**
     * @return the data objects store
     */
    public BaseStore<D> getStore()
    {
        return store;
    }

    /**
     * Refreshes the model data without constraints.
     */
    public void refreshData()
    {
        store.reload();
    }

    /**
     * Refreshes the model data according to a list of constraints.
     * 
     * @param constraints
     *            a list of URL constraint parameters
     */
    public void refreshData(List<IUrlParameter> constraints)
    {
        store.clearParameters();
        for (IUrlParameter constraint : constraints)
        {
            store.addParameter(constraint);
        }

        if (pagingMode)
        {
            ((ServicesPagingStore<D>) store).reload(getLoader().getOffset(), AdminConfig.PAGING_LIMIT);
        }
        else
        {
            store.reload();
        }
    }

    /**
     * When the loader is a paging loader, resets the offset to 0.
     */
    public void resetOffset()
    {
        if (pagingMode)
        {
            ((ServicesPagingStore<D>) store).setLoaderOffset(0);
        }
    }

    /**
     * In case of paginated model, this method will be used for getting the paging loader.
     * 
     * @return the paging loader, or null if the model does not accept pagination
     */
    public PagingLoader<?> getLoader()
    {
        return ((ServicesPagingStore<D>) store).getPagingLoader();
    }
}
