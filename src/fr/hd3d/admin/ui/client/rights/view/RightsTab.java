package fr.hd3d.admin.ui.client.rights.view;

import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;

import fr.hd3d.admin.ui.client.rights.controller.ResourceGroupController;
import fr.hd3d.admin.ui.client.rights.controller.RightsController;
import fr.hd3d.admin.ui.client.rights.controller.RightsPermsController;
import fr.hd3d.admin.ui.client.rights.controller.RoleController;
import fr.hd3d.admin.ui.client.rights.model.ResourceGroupModel;
import fr.hd3d.admin.ui.client.rights.model.RightsPermsModel;
import fr.hd3d.admin.ui.client.rights.model.RoleModel;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.admin.ui.client.view.SubView;


/**
 * The 'Roles and Rights Management' user interface.
 * 
 * @author HD3D
 * 
 */
public class RightsTab extends SubView
{

    // roles init
    /** the role model */
    private RoleModel roModel = new RoleModel();
    /** the role grid panel */
    private RolePanel roPanel = new RolePanel(roModel);
    /** the role events controller */
    private RoleController roController = new RoleController(roModel, roPanel);

    // resource groups init
    /** the resource group model */
    private ResourceGroupModel rgModel = new ResourceGroupModel();
    /** the resource group grid panel */
    private ResourceGroupPanel rgPanel = new ResourceGroupPanel(rgModel);
    /** the resource group event controller */
    private ResourceGroupController rgController = new ResourceGroupController(rgModel, rgPanel);

    // perms n bans
    /** the permissions and bans model */
    private RightsPermsModel peModel = new RightsPermsModel();
    /** the permissions and bans grid panel */
    private RightsPermsPanel pePanel = new RightsPermsPanel(peModel);
    /** the permissions and bans controller */
    private RightsPermsController peController = new RightsPermsController(peModel, pePanel);

    public RightsController init()
    {
        RightsController controller = new RightsController(this);
        controller.addChild(roController);
        controller.addChild(rgController);
        controller.addChild(peController);
        return controller;
    }

    public void initWidgets()
    {
        super.initWidgets();

        BorderLayoutData data = this.addWest(rgPanel, 380);
        data.setMinSize(380);
        this.addCenter(roPanel);
        this.addEast(pePanel, 550);
    }

    protected void setStyle()
    {
        setHeading(AdminMainView.CONSTANTS.RightsTabHeader());
        super.setStyle();
    }

}
