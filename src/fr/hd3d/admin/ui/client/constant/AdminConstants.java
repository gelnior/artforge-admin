package fr.hd3d.admin.ui.client.constant;

/**
 * Interface to represent the constants contained in resource bundle:'/home/florent-della-valle/donnees/eclipse/workspace/Admin/war/WEB-INF/classes/fr/hd3d/admin/ui/client/constant/AdminConstants.propert
 * i e s ' .
 */
public interface AdminConstants extends com.google.gwt.i18n.client.Constants
{

    /**
     * Translated "Account Information".
     * 
     * @return translated "Account Information"
     */
    @DefaultStringValue("Account Information")
    @Key("AccountInfo")
    String AccountInfo();

    /**
     * Translated "Account name".
     * 
     * @return translated "Account name"
     */
    @DefaultStringValue("Account name")
    @Key("AccountPrincipal")
    String AccountPrincipal();

    /**
     * Translated "Account Status".
     * 
     * @return translated "Account Status"
     */
    @DefaultStringValue("Account Status")
    @Key("AccountStatus")
    String AccountStatus();

    /**
     * Translated "Admin".
     * 
     * @return translated "Admin"
     */
    @DefaultStringValue("Admin")
    @Key("Admin")
    String Admin();

    /**
     * Translated "ALL".
     * 
     * @return translated "ALL"
     */
    @DefaultStringValue("ALL")
    @Key("AllButton")
    String AllButton();

    /**
     * Translated "Other Role Templates".
     * 
     * @return translated "Other Role Templates"
     */
    @DefaultStringValue("Other Role Templates")
    @Key("AllRoles")
    String AllRoles();

    /**
     * Translated "Apply Template...".
     * 
     * @return translated "Apply Template..."
     */
    @DefaultStringValue("Apply Template...")
    @Key("ApplyTemplate")
    String ApplyTemplate();

    /**
     * Translated "Authorization needed".
     * 
     * @return translated "Authorization needed"
     */
    @DefaultStringValue("Authorization needed")
    @Key("AuthzNeeded")
    String AuthzNeeded();

    /**
     * Translated "Ban String Creator".
     * 
     * @return translated "Ban String Creator"
     */
    @DefaultStringValue("Ban String Creator")
    @Key("BanCreator")
    String BanCreator();

    /**
     * Translated "banned".
     * 
     * @return translated "banned"
     */
    @DefaultStringValue("banned")
    @Key("Banned")
    String Banned();

    /**
     * Translated "Bans".
     * 
     * @return translated "Bans"
     */
    @DefaultStringValue("Bans")
    @Key("Bans")
    String Bans();

    /**
     * Translated "Create".
     * 
     * @return translated "Create"
     */
    @DefaultStringValue("Create")
    @Key("CanCreate")
    String CanCreate();

    /**
     * Translated "Delete".
     * 
     * @return translated "Delete"
     */
    @DefaultStringValue("Delete")
    @Key("CanDelete")
    String CanDelete();

    /**
     * Translated "Read".
     * 
     * @return translated "Read"
     */
    @DefaultStringValue("Read")
    @Key("CanRead")
    String CanRead();

    /**
     * Translated "Update".
     * 
     * @return translated "Update"
     */
    @DefaultStringValue("Update")
    @Key("CanUpdate")
    String CanUpdate();

    /**
     * Translated "Cancel".
     * 
     * @return translated "Cancel"
     */
    @DefaultStringValue("Cancel")
    @Key("Cancel")
    String Cancel();

    /**
     * Translated "Change password".
     * 
     * @return translated "Change password"
     */
    @DefaultStringValue("Change password")
    @Key("ChangePasswordDialog")
    String ChangePasswordDialog();

    /**
     * Translated "AdminConstants".
     * 
     * @return translated "AdminConstants"
     */
    @DefaultStringValue("AdminConstants")
    @Key("ClassName")
    String ClassName();

    /**
     * Translated "COL".
     * 
     * @return translated "COL"
     */
    @DefaultStringValue("COL")
    @Key("CollapseNode")
    String CollapseNode();

    /**
     * Translated "Create new account".
     * 
     * @return translated "Create new account"
     */
    @DefaultStringValue("Create new account")
    @Key("CreateAccountDialog")
    String CreateAccountDialog();

    /**
     * Translated "Added data".
     * 
     * @return translated "Added data"
     */
    @DefaultStringValue("Added data")
    @Key("DataToAdd")
    String DataToAdd();

    /**
     * Translated "Manually define account password".
     * 
     * @return translated "Manually define account password"
     */
    @DefaultStringValue("Manually define account password")
    @Key("DefineAccountPassword")
    String DefineAccountPassword();

    /**
     * Translated "Delete Account".
     * 
     * @return translated "Delete Account"
     */
    @DefaultStringValue("Delete Account")
    @Key("DeleteAccountDialog")
    String DeleteAccountDialog();

    /**
     * Translated "Delete Security Template".
     * 
     * @return translated "Delete Security Template"
     */
    @DefaultStringValue("Delete Security Template")
    @Key("DeleteTemplateWindow")
    String DeleteTemplateWindow();

    /**
     * Translated "Define new password".
     * 
     * @return translated "Define new password"
     */
    @DefaultStringValue("Define new password")
    @Key("EnterNewPassword")
    String EnterNewPassword();

    /**
     * Translated "Other data".
     * 
     * @return translated "Other data"
     */
    @DefaultStringValue("Other data")
    @Key("ExistingData")
    String ExistingData();

    /**
     * Translated "EXP".
     * 
     * @return translated "EXP"
     */
    @DefaultStringValue("EXP")
    @Key("ExpandNode")
    String ExpandNode();

    /**
     * Translated "Expanding node:".
     * 
     * @return translated "Expanding node:"
     */
    @DefaultStringValue("Expanding node:")
    @Key("ExpandNodeDialog")
    String ExpandNodeDialog();

    /**
     * Translated "filter by...".
     * 
     * @return translated "filter by..."
     */
    @DefaultStringValue("filter by...")
    @Key("FilterBy")
    String FilterBy();

    /**
     * Translated "First Name".
     * 
     * @return translated "First Name"
     */
    @DefaultStringValue("First Name")
    @Key("FirstName")
    String FirstName();

    /**
     * Translated "Resource Group".
     * 
     * @return translated "Resource Group"
     */
    @DefaultStringValue("Resource Group")
    @Key("Group")
    String Group();

    /**
     * Translated "Resource Groups".
     * 
     * @return translated "Resource Groups"
     */
    @DefaultStringValue("Resource Groups")
    @Key("Groups")
    String Groups();

    /**
     * Translated "Key:".
     * 
     * @return translated "Key:"
     */
    @DefaultStringValue("Key:")
    @Key("Key")
    String Key();

    /**
     * Translated "Label".
     * 
     * @return translated "Label"
     */
    @DefaultStringValue("Label")
    @Key("Label")
    String Label();

    /**
     * Translated "Last registered connection".
     * 
     * @return translated "Last registered connection"
     */
    @DefaultStringValue("Last registered connection")
    @Key("LastConnection")
    String LastConnection();

    /**
     * Translated "Last Name".
     * 
     * @return translated "Last Name"
     */
    @DefaultStringValue("Last Name")
    @Key("LastName")
    String LastName();

    /**
     * Translated "Login".
     * 
     * @return translated "Login"
     */
    @DefaultStringValue("Login")
    @Key("Login")
    String Login();

    /**
     * Translated "Name".
     * 
     * @return translated "Name"
     */
    @DefaultStringValue("Name")
    @Key("Name")
    String Name();

    /**
     * Translated "New password".
     * 
     * @return translated "New password"
     */
    @DefaultStringValue("New password")
    @Key("NewPassword")
    String NewPassword();

    /**
     * Translated "no connection registered yet".
     * 
     * @return translated "no connection registered yet"
     */
    @DefaultStringValue("no connection registered yet")
    @Key("NoConnection")
    String NoConnection();

    /**
     * Translated "Old password".
     * 
     * @return translated "Old password"
     */
    @DefaultStringValue("Old password")
    @Key("OldPassword")
    String OldPassword();

    /**
     * Translated "Owned Role Templates".
     * 
     * @return translated "Owned Role Templates"
     */
    @DefaultStringValue("Owned Role Templates")
    @Key("OwnedRoles")
    String OwnedRoles();

    /**
     * Translated "any object of this type and/or any of its children".
     * 
     * @return translated "any object of this type and/or any of its children"
     */
    @DefaultStringValue("any object of this type and/or any of its children")
    @Key("PermTypeAll")
    String PermTypeAll();

    /**
     * Translated "any version".
     * 
     * @return translated "any version"
     */
    @DefaultStringValue("any version")
    @Key("PermTypeAnyVersion")
    String PermTypeAnyVersion();

    /**
     * Translated "Node Type".
     * 
     * @return translated "Node Type"
     */
    @DefaultStringValue("Node Type")
    @Key("PermTypeHeader")
    String PermTypeHeader();

    /**
     * Translated "a data object instance".
     * 
     * @return translated "a data object instance"
     */
    @DefaultStringValue("a data object instance")
    @Key("PermTypeId")
    String PermTypeId();

    /**
     * Translated "a data object type or class".
     * 
     * @return translated "a data object type or class"
     */
    @DefaultStringValue("a data object type or class")
    @Key("PermTypeObject")
    String PermTypeObject();

    /**
     * Translated "method".
     * 
     * @return translated "method"
     */
    @DefaultStringValue("method")
    @Key("PermTypeOp")
    String PermTypeOp();

    /**
     * Translated "a known object, yet not a type nor an instance".
     * 
     * @return translated "a known object, yet not a type nor an instance"
     */
    @DefaultStringValue("a known object, yet not a type nor an instance")
    @Key("PermTypeOther")
    String PermTypeOther();

    /**
     * Translated "unknown object".
     * 
     * @return translated "unknown object"
     */
    @DefaultStringValue("unknown object")
    @Key("PermTypeUnknown")
    String PermTypeUnknown();

    /**
     * Translated "a version identifier".
     * 
     * @return translated "a version identifier"
     */
    @DefaultStringValue("a version identifier")
    @Key("PermTypeVersion")
    String PermTypeVersion();

    /**
     * Translated "Permission String Creator".
     * 
     * @return translated "Permission String Creator"
     */
    @DefaultStringValue("Permission String Creator")
    @Key("PermissionCreator")
    String PermissionCreator();

    /**
     * Translated "Permissions".
     * 
     * @return translated "Permissions"
     */
    @DefaultStringValue("Permissions")
    @Key("Permissions")
    String Permissions();

    /**
     * Translated "permitted".
     * 
     * @return translated "permitted"
     */
    @DefaultStringValue("permitted")
    @Key("Permitted")
    String Permitted();

    /**
     * Translated "persons...".
     * 
     * @return translated "persons..."
     */
    @DefaultStringValue("persons...")
    @Key("Pers")
    String Pers();

    /**
     * Translated "projects...".
     * 
     * @return translated "projects..."
     */
    @DefaultStringValue("projects...")
    @Key("Proj")
    String Proj();

    /**
     * Translated "Confirm password".
     * 
     * @return translated "Confirm password"
     */
    @DefaultStringValue("Confirm password")
    @Key("RepeatPassword")
    String RepeatPassword();

    /**
     * Translated "Rights".
     * 
     * @return translated "Rights"
     */
    @DefaultStringValue("Rights")
    @Key("RightsTabHeader")
    String RightsTabHeader();

    /**
     * Translated "Role".
     * 
     * @return translated "Role"
     */
    @DefaultStringValue("Role")
    @Key("Role")
    String Role();

    /**
     * Translated "Role Properties".
     * 
     * @return translated "Role Properties"
     */
    @DefaultStringValue("Role Properties")
    @Key("RoleProp")
    String RoleProp();

    /**
     * Translated "Roles".
     * 
     * @return translated "Roles"
     */
    @DefaultStringValue("Roles")
    @Key("Roles")
    String Roles();

    /**
     * Translated "Save".
     * 
     * @return translated "Save"
     */
    @DefaultStringValue("Save")
    @Key("Save")
    String Save();

    /**
     * Translated "templates...".
     * 
     * @return translated "templates..."
     */
    @DefaultStringValue("templates...")
    @Key("SecTemp")
    String SecTemp();

    /**
     * Translated "Security Templates".
     * 
     * @return translated "Security Templates"
     */
    @DefaultStringValue("Security Templates")
    @Key("SecurityTemplates")
    String SecurityTemplates();

    /**
     * Translated "Select Project:".
     * 
     * @return translated "Select Project:"
     */
    @DefaultStringValue("Select Project:")
    @Key("SelectProject")
    String SelectProject();

    /**
     * Translated "Resource Tree".
     * 
     * @return translated "Resource Tree"
     */
    @DefaultStringValue("Resource Tree")
    @Key("ServiceTree")
    String ServiceTree();

    /**
     * Translated "Solve...".
     * 
     * @return translated "Solve..."
     */
    @DefaultStringValue("Solve...")
    @Key("Solve")
    String Solve();

    /**
     * Translated "Comment:".
     * 
     * @return translated "Comment:"
     */
    @DefaultStringValue("Comment:")
    @Key("TemplateComment")
    String TemplateComment();

    /**
     * Translated "Successful Application".
     * 
     * @return translated "Successful Application"
     */
    @DefaultStringValue("Successful Application")
    @Key("TemplateSuccessfullyApplied")
    String TemplateSuccessfullyApplied();

    /**
     * Translated "Templates".
     * 
     * @return translated "Templates"
     */
    @DefaultStringValue("Templates")
    @Key("TemplatesTabHeader")
    String TemplatesTabHeader();

    /**
     * Translated "Undo all".
     * 
     * @return translated "Undo all"
     */
    @DefaultStringValue("Undo all")
    @Key("UndoAll")
    String UndoAll();

    /**
     * Translated "Account informations".
     * 
     * @return translated "Account informations"
     */
    @DefaultStringValue("Account informations")
    @Key("UserAccountFormInfos")
    String UserAccountFormInfos();

    /**
     * Translated "User Information".
     * 
     * @return translated "User Information"
     */
    @DefaultStringValue("User Information")
    @Key("UserInfos")
    String UserInfos();

    /**
     * Translated "Registered Users List".
     * 
     * @return translated "Registered Users List"
     */
    @DefaultStringValue("Registered Users List")
    @Key("Users")
    String Users();

    /**
     * Translated "Accounts".
     * 
     * @return translated "Accounts"
     */
    @DefaultStringValue("Accounts")
    @Key("UsersTabHeader")
    String UsersTabHeader();

    /**
     * Translated "E-Mail Address".
     * 
     * @return translated "E-Mail Address"
     */
    @DefaultStringValue("E-Mail Address")
    @Key("eMail")
    String eMail();

    @DefaultStringValue("Mount points")
    @Key("MountPointTabHeader")
    String MountPointTabHeader();
}
