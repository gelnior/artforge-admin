package fr.hd3d.admin.ui.client.templates.controller;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.templates.model.SecurityTemplateModel;
import fr.hd3d.admin.ui.client.templates.view.ISecurityTemplateView;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ProjectSecurityTemplateModelData;
import fr.hd3d.common.ui.client.service.PermissionPath;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;


/**
 * The main security templates controller, capable of handling all the operations and requests on security templates
 * plus the application of the selected security template on the selected project.
 * 
 * @author HD3D
 * 
 */
public class SecurityTemplateController extends Controller
{

    /** the main security template model */
    private SecurityTemplateModel model;
    /** the main security template view */
    private ISecurityTemplateView view;

    /**
     * This constructor affects a model and a view to the controller and register all handled events.
     * 
     * @param model
     *            the adequate model
     * @param panel
     *            the adequate view
     */
    public SecurityTemplateController(SecurityTemplateModel model, ISecurityTemplateView panel)
    {
        this.model = model;
        this.view = panel;
        registerEvents();
    }

    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        // treat prior to children
        if (type == AdminEvents.TEMPLATE_SELECTED)
        {
            onTemplateSelection(event.<ProjectSecurityTemplateModelData> getData());
        }
        else if (type == AdminEvents.TEMPLATE_CREATE_CONFIRMED)
        {
            String name = event.<String> getData();
            onTemplateCreation(name);
        }
        else if (type == AdminEvents.TEMPLATE_DELETE_CONFIRMED)
        {
            onTemplateDeletion();
        }
        else if (type == AdminEvents.TEMP_OWN_SERVER_MODIFICATION)
        {
            model.refreshTemplate();
        }

        forwardToChild(event);

        // treat after children
        if (type == CommonEvents.PERMISSION_INITIALIZED)
        {
            onPermissionsInitialized();
        }
        else if (type == AdminEvents.TEMPLATE_CREATE_CLICKED)
        {
            onCreationDemand();
        }
        else if (type == AdminEvents.TEMPLATE_EDIT_CLICKED)
        {
            onEditionDemand();
        }
        else if (type == AdminEvents.TEMPLATE_EDIT_CONFIRMED)
        {
            String name = event.<String> getData();
            onTemplateEdition(name);
        }
        else if (type == AdminEvents.TEMPLATE_DELETE_CLICKED)
        {
            onDeletionDemand();
        }
        else if (type == AdminEvents.TEMPLATE_COMMENT_SAVE)
        {
            String comment = event.<String> getData();
            onCommentSavingDemand(comment);
        }
        else if (type == AdminEvents.TEMP_PROJECT_SELECTED)
        {
            onProjectSelection(event.<ProjectModelData> getData());
        }
        else if (type == AdminEvents.APPLY_TO_PROJECT)
        {
            onApplyToProjectDemand();
        }
    }

    /**
     * Register all the events handled by this controller.
     */
    protected void registerEvents()
    {
        registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
        registerEventTypes(AdminEvents.TEMPLATE_CREATE_CLICKED);
        registerEventTypes(AdminEvents.TEMPLATE_CREATE_CONFIRMED);
        registerEventTypes(AdminEvents.TEMPLATE_DELETE_CLICKED);
        registerEventTypes(AdminEvents.TEMPLATE_DELETE_CONFIRMED);
        registerEventTypes(AdminEvents.TEMPLATE_EDIT_CLICKED);
        registerEventTypes(AdminEvents.TEMPLATE_EDIT_CONFIRMED);
        registerEventTypes(AdminEvents.TEMPLATE_SELECTED);
        registerEventTypes(AdminEvents.TEMP_PROJECT_SELECTED);
        registerEventTypes(AdminEvents.APPLY_TO_PROJECT);
        registerEventTypes(AdminEvents.TEMP_OWN_SERVER_MODIFICATION);
        registerEventTypes(AdminEvents.TEMPLATE_COMMENT_SAVE);
    }

    /**
     * Called when the permissions have been acquired by the main controller, this method initializes the view.
     */
    private void onPermissionsInitialized()
    {
        view.setup();
    }

    /**
     * Called upon template selection, this methods orders the model to select the security template and assures the
     * view takes the selection into account.
     * 
     * @param template
     *            the selected security template
     */
    private void onTemplateSelection(ProjectSecurityTemplateModelData template)
    {
        if (template != null)
        {
            model.setSelected(template);
            view.onTemplateSelection(template.getUserCanUpdate(), template.getUserCanDelete());
        }
    }

    /**
     * Demands that the view show a creation dialog on creation request.
     */
    private void onCreationDemand()
    {
        view.onCreateDemand();
    }

    /**
     * Demands that the view show an edition dialog on edition request.
     */
    private void onEditionDemand()
    {
        view.onEditDemand();
    }

    /**
     * Demands that the view show a confirmation dialog on deletion request.
     */
    private void onDeletionDemand()
    {
        view.onDeleteDemand();
    }

    /**
     * Handles security template creation, ordering the actual creation to the model and a refreshment to the view.
     * 
     * @param name
     *            the new security template name
     */
    private void onTemplateCreation(String name)
    {
        model.createLine(name);
        view.selectNewTemplate();
    }

    /**
     * Handles security template name edition, ordering the actual edition to the model and a refreshment to the view.
     * 
     * @param name
     *            the new name
     */
    private void onTemplateEdition(String name)
    {
        view.deselectTemplate();
        model.editLine(name);
        view.reselectTemplate();
    }

    /**
     * Handles security template deletion, ordering the actual deletion to the model and a refreshment to the view.
     */
    private void onTemplateDeletion()
    {
        view.onTemplateDeletion();
        model.deleteLine();
        view.onTemplateDeselection();
    }

    /**
     * Handles security template comment edition, ordering the actual edition to the model and a refreshment to the
     * view.
     * 
     * @param comment
     *            the new comment string
     */
    private void onCommentSavingDemand(String comment)
    {
        model.editComment(comment);
    }

    /**
     * Called upon project selection, this methods orders the model to select the project and assures the view takes the
     * selection into account.
     * 
     * @param project
     *            the selected project
     */
    private void onProjectSelection(ProjectModelData project)
    {
        model.setProject(project);
        view.onProjectSelection(PermissionUtil.hasCreateRightsOnPath(PermissionPath.getPath("Apply Template") + ":"
                + model.getSelected().getId() + ":" + project.getId()));
    }

    /**
     * Handles the operation of applying a security template to a project, ordering the model to send the proper
     * request.
     */
    private void onApplyToProjectDemand()
    {
        model.applyTemplate();
    }

}
