package fr.hd3d.admin.ui.test;

import fr.hd3d.admin.ui.client.model.AbstractRoleModel;
import fr.hd3d.admin.ui.client.view.IAbstractRoleView;
import fr.hd3d.admin.ui.test.rights.SubGridViewMock;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;


public abstract class AbstractRoleViewMock extends SubGridViewMock<RoleModelData> implements IAbstractRoleView
{

    public int createCount = 0;
    public int editCount = 0;
    public boolean actionsEnabled = false;

    public AbstractRoleViewMock(AbstractRoleModel model)
    {
        super(model);
    }

    public void onItemSelection(boolean canUpdate, boolean canDelete)
    {
        actionsEnabled = true;
    }

    public void onFullDeselection()
    {
        actionsEnabled = false;
    }

    public void onCreateDemand()
    {
        createCount++;
    }

    public void onEditDemand()
    {
        editCount++;
    }

}
