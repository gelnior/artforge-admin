package fr.hd3d.admin.ui.client.users.view;

import com.extjs.gxt.ui.client.binding.FormBinding;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.FormButtonBinding;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.Validator;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.event.dom.client.KeyCodes;

import fr.hd3d.admin.ui.client.users.model.AccountFormModel;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.admin.ui.client.view.ConfirmationDialog;


/**
 * A factoring class for the user account creation and edition form dialogs.
 * 
 * @author HD3D
 * 
 */
public abstract class AccountFormDialog extends ConfirmationDialog
{

    /** whether the dialog handles account creation */
    protected boolean isCreateType;
    /** the selected user's account model data */
    protected AccountFormModel model;

    protected FormPanel form;
    /** the field set for account informations */
    protected FieldSet fieldSet;
    protected LabelField userField;
    protected FormData formData = new FormData("-10");

    protected FormBinding passwordBinding;

    /** the field set for passwords */
    protected FieldSet subFieldSet;
    /** the password field */
    protected TextField<String> password;
    /** the 'repeat password' field */
    protected TextField<String> repeat;

    /**
     * Creates the account form dialog and readies it for display.
     * 
     * @param header
     *            the dialog heading
     * @param username
     *            the selected user's name
     * @param isCreateType
     *            whether last event was a creation or an edition
     */
    public AccountFormDialog(String header, String username, boolean isCreateType)
    {
        super(header, username, null, null);

        this.isCreateType = isCreateType;
        setupModel(username);
    }

    /**
     * Initializes an <code>AccountFormModel</code> for the dialog.
     * 
     * @param username
     *            the user account's principal
     */
    public void setupModel(String username)
    {
        if (username != null)
        {
            message = username;
            model = new AccountFormModel(username);
            passwordBinding = new FormBinding(form);
            setFieldBinding();

            setHeaderAdding(username);
        }
    }

    public void show()
    {
        password.clearInvalid();
        password.setValue("");
        repeat.clearInvalid();
        repeat.setValue("");

        userField.setText("<span style='color:purple'>" + message + "</span>");
        super.show();
    }

    protected void setup()
    {
        form = new FormPanel();
        super.setup();

        fieldSet = new FieldSet();
        fieldSet.setHeading(AdminMainView.CONSTANTS.UserAccountFormInfos());

        FormLayout layout = new FormLayout();
        layout.setLabelWidth(150);
        fieldSet.setLayout(layout);

        userField = new LabelField();
        userField.setFieldLabel(AdminMainView.CONSTANTS.AccountPrincipal());
        userField.setReadOnly(true);
        userField.setLabelSeparator(" - ");
        fieldSet.add(userField, formData);

        fieldSet.setWidth(400);

        form.add(fieldSet);

        subFieldSet = new FieldSet();
        setFormFields();
        form.add(subFieldSet);

        add(form);
    }

    /**
     * Sets the form style.
     */
    protected void setStyle()
    {
        form.setLayout(new FlowLayout());
        form.setHeaderVisible(false);
        form.setFrame(true);
        form.setWidth(460);
        form.setAutoHeight(true);

        setWidth(440);
        setAutoHeight(true);
    }

    protected void setupOkButton(Button ok)
    {
        ok.addListener(Events.Select, new AccountFormListener());

        FormButtonBinding binding = new FormButtonBinding(form);
        binding.addButton(ok);
    }

    /**
     * Sets the main form field and allows an extending object to add other fields to the field set.
     */
    protected void setFormFields()
    {
        FormLayout layout = new FormLayout();
        layout.setLabelWidth(150);
        subFieldSet.setLayout(layout);

        password = new TextField<String>();
        password.setFieldLabel(AdminMainView.CONSTANTS.NewPassword());
        password.setPassword(true);
        password.setAllowBlank(true);
        password.setValidator(new PassValidator());
        subFieldSet.add(password, formData);

        repeat = new TextField<String>();
        repeat.setFieldLabel(AdminMainView.CONSTANTS.RepeatPassword());
        repeat.setPassword(true);
        repeat.setAllowBlank(true);
        repeat.setValidator(new RepeatValidator());
        repeat.addKeyListener(new AccountFormKeyListener());
        subFieldSet.add(repeat, formData);

        subFieldSet.setWidth(400);
    }

    /**
     * Sets a default password binding and allows an extending object to add new field bindings to the dialog's model.
     */
    protected void setFieldBinding()
    {
        passwordBinding.autoBind();
        passwordBinding.bind(model);
    }

    /**
     * A validator specifically designed for the password field.
     * 
     * @author HD3D
     * 
     */
    class PassValidator implements Validator
    {
        private static final int PASSWORD_MIN_LENGTH = 3;

        public String validate(Field<?> field, String value)
        {
            if (subFieldSet.isExpanded())
            {
                if (value.equals(""))
                {
                    return AdminMainView.MESSAGES.PasswordMustNotBeNull();
                }
                if (value.length() < PASSWORD_MIN_LENGTH)
                {
                    return AdminMainView.MESSAGES.PasswordInvalidInput();
                }
            }
            return null;
        }
    }

    /**
     * A validator that will check if the 'repeat password' field is equal to the password field.
     * 
     * @author HD3D
     * 
     */
    class RepeatValidator implements Validator
    {
        public String validate(Field<?> field, String value)
        {
            if (subFieldSet.isExpanded())
            {
                if (value == null || !value.equals(password.getValue()))
                {
                    return AdminMainView.MESSAGES.RepeatPasswordError();
                }
            }
            return null;
        }
    }

    /**
     * A listener which can trigger account creation or edition depending on the account form dialog type.
     * 
     * @author HD3D
     * 
     */
    class AccountFormListener extends SelectionListener<ButtonEvent>
    {
        public void componentSelected(ButtonEvent ce)
        {
            if (isCreateType)
            {
                model.createAccount();
            }
            else
            {
                model.editAccount();
            }
        }
    }

    class AccountFormKeyListener extends KeyListener
    {
        @Override
        public void componentKeyUp(ComponentEvent event)
        {
            if (event.getKeyCode() == KeyCodes.KEY_ENTER)
            {
                if (isCreateType)
                {
                    model.createAccount();
                }
                else
                {
                    model.editAccount();
                }
                hide();
            }
        }
    }
}
