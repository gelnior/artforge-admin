package fr.hd3d.admin.ui.client.model.modeldata;

import fr.hd3d.common.ui.client.modeldata.reader.IPagingReader;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.reader.RoleReader;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;


/**
 * This singleton class provides the application with the adequate role reader, paging or not.
 * 
 * @author HD3D
 * 
 */
public abstract class SingleRoleReader
{

    private static IReader<RoleModelData> lReader;
    private static IPagingReader<RoleModelData> pReader;

    /**
     * Call in order to get a role paging reader.
     * 
     * @return the adequate role paging reader
     */
    public static IPagingReader<RoleModelData> getPagingReader()
    {
        if (pReader == null)
        {
            pReader = new RolePagingReader();
        }
        return pReader;
    }

    /**
     * Call in order to get a role reader.
     * 
     * @return the adequate role reader
     */
    public static IReader<RoleModelData> getReader()
    {
        if (lReader == null)
        {
            lReader = new RoleReader();
        }
        return lReader;
    }

    /**
     * Sets the inner instance to the specified parameter.
     * 
     * @param reader
     *            the new reader instance
     */
    public static void setSimpleInstance(IReader<RoleModelData> reader)
    {
        if (reader != null)
        {
            lReader = reader;
        }
    }

    /**
     * Sets the inner instance to the specified parameter.
     * 
     * @param reader
     *            the new paging reader instance
     */
    public static void setInstance(IPagingReader<RoleModelData> reader)
    {
        if (reader != null)
        {
            pReader = reader;
        }
    }

}
