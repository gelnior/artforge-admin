package fr.hd3d.admin.ui.client.templates.controller;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.controller.SubController;
import fr.hd3d.admin.ui.client.view.ISubView;


/**
 * The controller for the 'Security Templates Management' application tab.
 * 
 * @author HD3D
 * 
 */
public class TemplatesController extends SubController
{

    public TemplatesController(ISubView tab)
    {
        super(tab);
    }

    protected void registerEvents()
    {
        super.registerEvents();
    }

    protected void doHandleEvent(AppEvent event)
    {}

}
