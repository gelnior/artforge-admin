package fr.hd3d.admin.ui.client.model.callback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.google.gwt.core.client.GWT;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.model.ServiceURLs;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


/**
 * A specifically designed call-back method for handling the service URL tree initialization.
 * 
 * @author HD3D
 * 
 */
public class ServiceURLsCallBack extends BaseCallback
{

    /** the URL tree to initialize */
    private ServiceURLs tree;

    /**
     * Creates a callback that will send initialization data the specified URL tree and fire an event upon server
     * response.
     * 
     * @param tree
     *            the tree to initialize
     */
    public ServiceURLsCallBack(ServiceURLs tree)
    {
        this.tree = tree;
    }

    protected void onSuccess(Request request, Response response)
    {
        try
        {
            List<List<String>> result = new ArrayList<List<String>>();

            String entity = response.getEntity().getText();
            if (entity != null)
            {
                entity = entity.substring(2, entity.length() - 2); // suppress the brackets
                entity = entity.replace('[', '£'); // µ%é$in' brackets !
                entity = entity.replace(']', '£'); // µ%é$in' brackets !

                String[] data = entity.split("£, £"); // data is an array containing all the URL patterns

                String piece;
                List<String> pattern;
                for (int j = 0; j < data.length; j++)
                {
                    piece = data[j];
                    pattern = new LinkedList<String>(Arrays.asList(piece.split(", "))); // pattern is a list of URL
                    // parts
                    result.add(pattern);
                }
                tree.init(result); // send the result to the service URLs tree
            }
            EventDispatcher.forwardEvent(AdminEvents.SERVICE_URLS_INITIALIZED);
        }
        catch (Exception e)
        {
            GWT.log("Errors occurs in ServicesURLsCallback.", null);
        }
    }

}
