package fr.hd3d.admin.ui.client.perms.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.google.gwt.core.client.GWT;

import fr.hd3d.admin.ui.client.config.AdminConfig;
import fr.hd3d.admin.ui.client.model.ServiceURLs;
import fr.hd3d.admin.ui.client.perms.view.ManualVersionFilter;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.util.CollectionUtils;

/**
 * This class contains all the dirty work about permission tree stores. Its main
 * functions are to be able to transform a list of permission strings into a
 * tree store and vice-versa, and to update a tree store when an action is
 * performed on it. The underlying algorithm are somehow complex and need to be
 * extremely cautious when used or modified.
 * 
 * @author HD3D
 * 
 */
public final class PermsTreeStoreUtil {

	private static ServiceURLs serviceURLs;

	/**
	 * Sets the reference tree for the service URLs.
	 */
	public static void initServiceURLs() {
		serviceURLs = ServiceURLs.getInstance();
	}

	/**
	 * Checks whether specified node is a leaf as regards the service URLs tree.
	 * 
	 * @param node
	 *            a tree store node
	 * @return true if the node has no children in the service URLs tree
	 */
	public static boolean isLeaf(PermModelData node) {
		List<String> ways;
		if (node.getType() == PermModelData.VERSION) {
			ways = serviceURLs.getRoots(); // version node's children are found
			// at root level
		} else {
			ways = serviceURLs.getFold(node.getPath());
		}
		return CollectionUtils.isEmpty(ways);
	}

	/**
	 * Gets the previous permission node in a semantic way. If the node is of
	 * the ALL type, returns the node's parent. Else it returns the upper ALL
	 * sibling node.
	 * 
	 * @param store
	 *            the tree store
	 * @param node
	 *            the initial node
	 * @return the node's upper node in permission terms
	 */
	public static PermModelData getPreviousNode(TreeStore<PermModelData> store,
			PermModelData node) {
		PermModelData parent = store.getParent(node);
		if (node.getType() == PermModelData.ALL) {
			return parent;
		} else {
			if (parent != null) {
				return store.getFirstChild(parent);
			}
			return null;
		}
	}

	/**
	 * Returns the list of the nodes that depend on the specified node in a
	 * semantic way. If the node is of the ALL type, returns the list of its
	 * siblings and their first ALL node son. Else it returns only the first ALL
	 * child.
	 * 
	 * @param store
	 *            the tree store
	 * @param node
	 *            the initial node
	 * @return the list of the node's depending nodes in permission terms
	 */
	public static List<PermModelData> getNextNodes(
			TreeStore<PermModelData> store, PermModelData node) {
		List<PermModelData> nexts = new LinkedList<PermModelData>();
		PermModelData child = node;
		PermModelData all_child;
		if (node.getType() == PermModelData.ALL) {
			while ((child = store.getNextSibling(child)) != null) {
				nexts.add(child);
				all_child = store.getFirstChild(child);
				if (all_child != null) // also adds the first ALL child of each
				// sibling
				{
					nexts.add(all_child);
				}
			}
		} else {
			all_child = store.getFirstChild(child); // ALL child is always the
			// first one
			if (all_child != null) {
				nexts.add(all_child);
			}
		}
		return nexts;
	}

	/**
	 * Checks whether the node contains raw ID model data whose label must be
	 * updated.
	 * 
	 * @param store
	 *            the tree store
	 * @param node
	 *            the parent node
	 * @return true if at least one child node is of the ID type
	 */
	public static boolean mustBeUpdated(TreeStore<PermModelData> store,
			PermModelData node) {
		if (serviceURLs.hasIdChild(node.getPath())) {
			for (PermModelData child : store.getChildren(node)) {
				if (child.getType() == PermModelData.ID) // the node has a
				// default label,
				// not an
				// understandable
				// one
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Recursively propagates the tree store's consistency starting from the
	 * specified node.
	 * 
	 * @param store
	 *            the tree store
	 * @param node
	 *            the node from which to pass on
	 * @param init
	 *            whether the propagation is done upon initialization
	 */
	private static void passOnShields(TreeStore<PermModelData> store,
			PermModelData node, boolean init) {
		if (node.getType() == PermModelData.ALL
				|| node.getType() == PermModelData.VERSION) {
			for (PermModelData child : getNextNodes(store, node)) {
				/* deb */// GWT.log(node.getLabel() + " --> " +
				// child.getLabel(), null);
				node.transferShields(child);
				passOnShields(store, child, init);
			}
		} else {
			if (node.getCanAll().hasValue()) // checks rapidly if a value is met
			{
				if (init) // for the first round, also transfers shields
				// horizontally
				{
					/* deb */// GWT.log(node.getLabel() + " --> horizontal",
					// null);
					node.getCanAll().transferShields(node.getCanRead());
					node.getCanAll().transferShields(node.getCanUpdate());
					node.getCanAll().transferShields(node.getCanDelete());
					node.getCanAll().transferShields(node.getCanCreate());
				}
				PermModelData child = store.getFirstChild(node); // gets the
				// 'ALL'
				// child
				// node
				if (child != null) {
					/* deb */// GWT.log(node.getLabel() + " --> " +
					// child.getLabel(), null);
					node.transferShields(child);
					// passOnShields(store, child, init);
				}
			}
		}
	}

	/**
	 * Recursively propagates the store's consistency throughout the tree store
	 * elements upon store initialization.
	 * 
	 * @param store
	 *            the tree store
	 */
	private static void passOnShields(TreeStore<PermModelData> store) {
		for (PermModelData root : store.getRootItems()) {
			passOnShields(store, root, true);
		}
	}

	/**
	 * Expands the store at the node level, displaying all the node children
	 * that can be found on the server side. If the path contains an Id marker,
	 * then the actual data objects will be loaded from the server.
	 * 
	 * @param store
	 *            the store to expand
	 * @param node
	 *            the node to expand
	 * @param ifIDchild
	 *            the event type to forward in case an ID child is requested
	 * @param expansionFinished
	 *            the event type to forward in case the full expansion has been
	 *            performed
	 */
	public static void expandNode(TreeStore<PermModelData> store,
			PermModelData node, EventType ifIDchild, EventType expansionFinished) {
		String path = node.getPath();
		List<String> ways;
		boolean isVersion = node.getType() == PermModelData.VERSION;
		if (isVersion) {
			ways = serviceURLs.getRoots();
		} else {
			ways = serviceURLs.getFold(path);
		}
		if (ways != null && !ways.isEmpty()) // only expand if the node is not a
		// leaf according to the service
		// URLs tree
		{
			boolean expansionOver = true;
			List<PermModelData> children = new ArrayList<PermModelData>();

			PermModelData child;
			PermModelData all_child = store.getFirstChild(node); // get the ALL
			// child
			// node
			if (all_child == null) {
				all_child = new PermModelData(node.getLabel() + " - "
						+ AdminMainView.CONSTANTS.AllButton(),
						PermModelData.ALL, path + ":*");
				PermModelData parent = store.getParent(node);
				if (parent != null) {
					parent = store.getFirstChild(parent); // get the ALL parent
					// node, which
					// cannot be null
					parent.transferShields(all_child);
				}
				node.transferShields(all_child);
				store.add(node, all_child, false);
			}
			for (String part : ways) {
				if (part.equals(ServiceURLs.ID_MARK)) // child nodes include
				// data object instances
				{
					expansionOver = false;
					AppEvent event = new AppEvent(ifIDchild);
					event.setData(node);
					EventDispatcher.forwardEvent(event); // calls upon the
					// controller to
					// send a request
				} else {
					if (serviceURLs.isException(part)) // checks whether the
					// node is a data object
					// or not
					{
						child = new PermModelData(part, PermModelData.OTHER,
								path + ":" + part);
					} else {
						child = new PermModelData(part, PermModelData.OBJECT,
								path + ":" + part);
					}
					if (!store.getChildren(node).contains(child)) {
						all_child.transferShields(child);
						children.add(child);
					}
				}
			}
			if (isVersion) // if expanding a version node, let the user manually
			// control expansion
			{
				ManualVersionFilter.getInstance().show(children, store, node,
						expansionFinished);
			} else {
				store.add(node, children, false);
				if (expansionOver) // if instance child nodes are still awaited,
				// do not send an 'expansion is over'
				// event
				{
					node.expanded = true;
					AppEvent event = new AppEvent(expansionFinished);
					event.setData(node);
					EventDispatcher.forwardEvent(event); // announce the
					// expansion
					// achievement
				}
			}
		}
	}

	/**
	 * Appends the server freshly arrived children IDs to specified node.
	 * 
	 * @param store
	 *            the tree store
	 * @param node
	 *            the parent node
	 * @param childrenIds
	 *            the list children IDs
	 */
	@Deprecated
	public static void appendChildrenIds(TreeStore<PermModelData> store,
			PermModelData node, List<String> childrenIds) {
		PermModelData child;
		PermModelData all_child = store.getFirstChild(node); // get the 'ALL'
		// child node
		String path = node.getPath();
		for (String id : childrenIds) {
			child = new PermModelData(id, PermModelData.ID, path + ":" + id);
			if (!store.getChildren(node).contains(child)) {
				all_child.transferShields(child);
				store.add(node, child, false);
			}
		}
		node.expanded = true;
	}

	/**
	 * Appends all the permission model data child nodes in the list to the
	 * specified store at a certain node level.
	 * 
	 * @param store
	 *            the tree store
	 * @param node
	 *            the tree node to which the children are added
	 * @param children
	 *            the list of permission model data to add
	 * @param expansionFinished
	 *            the event type to forward in case the full expansion has been
	 *            performed
	 */
	public static void appendChosenChildren(TreeStore<PermModelData> store,
			PermModelData node, List<PermModelData> children,
			EventType expansionFinished) {
		PermModelData all_child = store.getFirstChild(node); // get the 'ALL'
		// child node
		for (PermModelData child : children) {
			if (!store.getChildren(node).contains(child)) {
				all_child.transferShields(child);
				store.add(node, child, false);
			}
		}
		node.expanded = true;
		AppEvent event = new AppEvent(expansionFinished);
		event.setData(node);
		EventDispatcher.forwardEvent(event); // here expansion is assumed to be
		// finished
	}

	/**
	 * Tries to collapse the branch starting from the specified node. It
	 * recursively tries to collapse all the child nodes and will only do so if
	 * the child node does not contain any value.
	 * 
	 * @param store
	 *            the store to collapse
	 * @param node
	 *            the node to collapse
	 * @return true if the children of the node have been successfully collapsed
	 */
	public static boolean collapseNode(TreeStore<PermModelData> store,
			PermModelData node) {
		List<PermModelData> children = store.getChildren(node); // get the ALL
		// child node
		boolean result = true;
		boolean local_result;
		List<PermModelData> toRemove = new LinkedList<PermModelData>();
		if (children != null && !children.isEmpty()) {
			for (PermModelData child : children) {
				if (child.getType() != PermModelData.ALL) // conserve the ALL
				// child node
				{
					local_result = collapseNode(store, child);
					if (!local_result || child.hasValues()) // if child node has
					// been fully
					// collapsed
					{
						result = false;
					} else {
						toRemove.add(child);
					}
				}
			}
		}
		if (result) // all child nodes have been removed but the ALL child node
		{
			PermModelData child = store.getFirstChild(node); // get the ALL
			// child node
			// again...
			if (child != null && !child.hasValues()) {
				toRemove.add(child); // check whether the ALL node is removable
			} else if (child != null) {
				result = false;
			}
		}
		for (PermModelData perm : toRemove) {
			store.remove(node, perm); // remove all removable items at last
		}
		node.expanded = false;
		return result;
	}

	/**
	 * Translates the tree store into a list of permissions and bans.
	 * 
	 * @param store
	 *            the store to transform
	 * @param perms
	 *            the pre-existing permission list
	 * @param bans
	 *            the pre-existing ban list
	 */
	public static void translateStore(TreeStore<PermModelData> store,
			List<String> perms, List<String> bans) {
		perms.clear();
		bans.clear();
		for (PermModelData root : store.getRootItems()) {
			perms.addAll(lookForRight(store, root, false, true, true, true,
					true)); // translate permissions
			bans
					.addAll(lookForRight(store, root, true, true, true, true,
							true)); // translate bans
		}
	}

	/**
	 * Recursively retrieves the rights bearing nodes and translate them into a
	 * list of right strings.
	 * 
	 * @param store
	 *            the store in which to search
	 * @param node
	 *            the starting node
	 * @param isBan
	 *            the value to match (either ban or permission)
	 * @param create
	 *            whether to look for a create right
	 * @param read
	 *            whether to look for a read right
	 * @param update
	 *            whether to look for an update right
	 * @param delete
	 *            whether to look for a delete right
	 * @return the right strings list
	 */
	private static List<String> lookForRight(TreeStore<PermModelData> store,
			PermModelData node, boolean isBan, boolean create, boolean read,
			boolean update, boolean delete) {
		List<String> rights = new ArrayList<String>();
		if (node.hasValues(isBan)) {
			if (node.getCanAll().hasValue()) // if a 'can ALL' right is met, cut
			// the branch
			{
				rights.add(getRightString(node, isBan));
				return rights;
			} else if (node.getType() == PermModelData.ALL
					&& (node.getCanAll().getValue() == PermModelData.DEDUCED_BAN || (!isBan && node
							.getCanAll().getValue() == PermModelData.DEDUCED_PERMISSION))) {
				return rights; // stops the descent whenever the ALL node has
				// been validated from above
			} else {
				create = create && !node.getCanCreate().hasValue(isBan); // update
				// all
				// 'looking
				// for...'
				// informations
				read = read && !node.getCanRead().hasValue(isBan);
				update = update && !node.getCanUpdate().hasValue(isBan);
				delete = delete && !node.getCanDelete().hasValue(isBan);
				rights.add(getRightString(node, isBan));
			}
		}
		if ((create || read || update || delete)
				&& (node.getType() == PermModelData.ALL || node.getType() == PermModelData.VERSION)) {
			List<PermModelData> children = getNextNodes(store, node);
			for (PermModelData child : children) {
				rights.addAll(lookForRight(store, child, isBan, create, read,
						update, delete));
			}
		}

		/* debug */
		// GWT.log("node " + node.getPath() + " with " + create + read + update
		// + delete + " and -> " +
		// rights.toString(),
		// null);
		return rights;
	}

	/**
	 * Initializes the tree store from a list of permissions and a list of bans.
	 * 
	 * @param store
	 *            the cleared tree store
	 * @param perms
	 *            the permission list
	 * @param bans
	 *            the ban list
	 */
	public static void insertInStore(TreeStore<PermModelData> store,
			List<String> perms, List<String> bans) {
		if ((perms == null || perms.isEmpty())
				&& (bans == null || bans.isEmpty())) // needs to be initialized
		// ab nihilo
		{
			store.add(new PermModelData(AdminMainView.CONSTANTS
					.PermTypeAnyVersion(), PermModelData.VERSION, "*"), false);
		} else {
			insertListInStore(store, perms, false);
			insertListInStore(store, bans, true);
			passOnShields(store);
		}

		/* debug */
		// printStore(store);
	}

	/**
	 * Initializes the tree store from a list of rights.
	 * 
	 * @param store
	 *            the tree store
	 * @param list
	 *            the right strings list
	 * @param isBan
	 *            whether the list contains bans or permissions
	 */
	private static void insertListInStore(TreeStore<PermModelData> store,
			List<String> list, boolean isBan) {
		String[] split; // the permission string split
		String part; // a permission string piece
		int type; // a permission model data type
		String path; // the growing permission path
		String star = "*"; // the 'star' label
		PermModelData node = null; // the current node
		PermModelData previous = null; // the parent node
		for (String right : list) {
			split = right.split(":"); // splits the permission string in parts
			// which will become the tree store
			// nodes
			part = split[0]; // the first part is the version
			path = part;
			if (part.equals("*")) // the version is '*', so the permission
			// string applies to all versions
			{
				node = new PermModelData("v*", PermModelData.VERSION, path);
			} else {
				node = new PermModelData(part, PermModelData.VERSION, path);
			}
			if (!store.getRootItems().contains(node)) // add the version to the
			// root list
			{
				store.add(node, false);
			}
			for (int k = 1; k < split.length - 1; k++) {
				previous = node; // next node will be a child of current node
				node = new PermModelData(previous.getIdentifier() + " - "
						+ star, PermModelData.ALL, path + ":*");
				if (!store.getChildren(previous).contains(node)) {
					store.add(previous, node, false); // always add a 'all' leaf
					// to the node
				}
				part = split[k].split(",")[0]; // does not support object
				// appending inside right body
				if (!part.equals("*")) // does not support this kind of
				// permission
				{
					// can choose a type from the service URLs tree
					if (serviceURLs.hasChild(path, part)) {
						if (serviceURLs.isException(part)) {
							type = PermModelData.OTHER;
						} else {
							type = PermModelData.OBJECT;
						}
					} else if (serviceURLs.hasIdChild(path)) {
						type = PermModelData.ID;
					} else {
						type = PermModelData.UNKNOWN;
					}
					path += ":" + part;
					node = new PermModelData(part, type, path);
					if (!store.getChildren(previous).contains(node)) {
						store.add(previous, node, false); // add the node as a
						// child
					} else {
						for (PermModelData child : store.getChildren(previous)) {
							if (child.equals(node)) {
								node = child; // if the store already contains
								// the node, then select it
								break;
							}
						}
					}
				} else {
					node = store.getFirstChild(previous); // select the 'ALL'
					// child as next
					// node
				}
			}
			if (split.length > 1) {
				part = split[split.length - 1]; // the last part stands for the
				// actual right
				if (previous == null) // the string only contains a version
				{
					parseMethods(part, node, isBan);
				} else {
					parseMethods(part, node, isBan);
					previous = null; // for next list iteration
				}
			}
		}
	}

	/**
	 * Parses the methods in the specified string and sets the permission model
	 * data properties accordingly.
	 * 
	 * @param methods
	 *            the methods-containing string
	 * @param perm
	 *            the permission model data to set
	 * @param isBan
	 *            whether the status to put is 'banned' or 'permitted'
	 */
	private static void parseMethods(String methods, PermModelData perm,
			boolean isBan) {
		int status;
		if (isBan) {
			status = PermModelData.BANNED;
		} else {
			status = PermModelData.PERMITTED;
		}
		String[] split = methods.split(",");
		List<String> met = Arrays.asList(split);

		if (met.contains(AdminConfig.READ)) {
			perm.setCanRead(status);
		}
		if (met.contains(AdminConfig.CREATE)) {
			perm.setCanCreate(status);
		}
		if (met.contains(AdminConfig.DELETE)) {
			perm.setCanDelete(status);
		}
		if (met.contains(AdminConfig.UPDATE)) {
			perm.setCanUpdate(status);
		}
		if (met.contains("*")) {
			perm.setCanAll(status);
		}
	}

	/**
	 * Transforms the permission object into a right string.
	 * 
	 * @param perm
	 *            the permission model data
	 * @param isBan
	 *            the value to match (either ban or permission)
	 * @return a valid right string
	 */
	private static String getRightString(PermModelData perm, boolean isBan) {
		if (perm.getCanAll().hasValue(isBan)) {
			return perm.getPath() + ":*";
		} else {
			String suffix = ":";
			if (perm.getCanCreate().hasValue(isBan)) {
				suffix += AdminConfig.CREATE + ",";
			}
			if (perm.getCanRead().hasValue(isBan)) {
				suffix += AdminConfig.READ + ",";
			}
			if (perm.getCanUpdate().hasValue(isBan)) {
				suffix += AdminConfig.UPDATE + ",";
			}
			if (perm.getCanDelete().hasValue(isBan)) {
				suffix += AdminConfig.DELETE + ",";
			}
			suffix = suffix.substring(0, suffix.length() - 1);
			return perm.getPath() + suffix;
		}
	}

	/**
	 * Gets a server resource path from the permission model data path.
	 * 
	 * @param perm
	 *            the reference permission model data
	 * @return a valid server resource URL path
	 */
	public static String getServerPath(PermModelData perm) {
		String path = perm.getPath();
		int first = path.indexOf(":") + 1;
		path = path.substring(first); // remove version info, which is already
		// in configuration file
		path = path.replace(":", "/");

		return path + "/";
		// return serviceURLs.redirect(path) + "/";
	}

	/* debug */
	public static void printStore(TreeStore<PermModelData> store) {
		String log = "";
		for (PermModelData item : store.getAllItems()) {
			log += item.getIdentifier() + "\n";
			log += "    - " + item.getPath() + "\n";
			PermModelData parent = store.getParent(item);
			if (parent != null)
				log += "        parent = " + parent.getIdentifier() + "\n";
		}
		Throwable e = new Throwable(log);
		GWT.log("store print", e);
	}
}
