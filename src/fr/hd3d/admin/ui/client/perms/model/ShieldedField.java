package fr.hd3d.admin.ui.client.perms.model;

/**
 * This class is a permission model data dedicated field wrapper. It contains the right value protected by two shields:
 * one for to account for the permission influences and another for the ban influences. A 'banned' or 'permitted' right
 * value cannot be set nor gotten from a field whose shields are not null. In case the 'permission shield' is raised,
 * the returned value will be a 'deduced permission', i.e for the 'ban shield'. If both shields are on, the 'ban shield'
 * always prevail.
 * 
 * @author HD3D
 * 
 */
public class ShieldedField
{

    /** the wrapped value */
    int value;

    /** whether the field is enabled or not */
    private boolean enabled;

    // shields
    /** the permission shield */
    private int permShield;
    /** the ban shield */
    private int banShield;

    /**
     * Creates a field wrapper providing a permission and a ban shield.
     * 
     * @param enabled
     *            whether the field is enabled or not
     */
    public ShieldedField(boolean enabled)
    {
        value = PermModelData.NONE;
        this.enabled = enabled;

        clearShields();
    }

    /**
     * Gets the field value, depending on the shields' respective levels.
     * 
     * @return the wrapped field value
     */
    public int getValue()
    {
        if (!enabled)
        {
            return PermModelData.NONE;
        }
        else if (hasBanShield())
        {
            return PermModelData.DEDUCED_BAN;
        }
        else if (value == PermModelData.BANNED)
        {
            return value;
        }
        else if (hasPermShield())
        {
            return PermModelData.DEDUCED_PERMISSION;
        }
        else
        {
            return value;
        }
    }

    /**
     * Sets the wrapped field value, depending on the shield's respective levels.
     * 
     * @param value
     *            the value to set
     */
    public void setValue(int value)
    {
        if (enabled)
        {
            this.value = PermModelData.DEFAULT;
            if (!hasBanShield())
            {
                if (!hasPermShield() || value == PermModelData.BANNED)
                {
                    this.value = value;
                }
            }
        }
    }

    /**
     * Revokes the shields, nullifies their values.
     */
    public void clearShields()
    {
        permShield = 0;
        banShield = 0;
    }

    /**
     * Increases the ban shield's level.
     */
    public void increaseBanShield()
    {
        if (banShield < 3)
            banShield++;
    }

    /**
     * Decreases the ban shield's level.
     */
    public void decreaseBanShield()
    {
        if (banShield > 0)
            banShield--;
    }

    /**
     * Increases the permission shield's level.
     */
    public void increasePermShield()
    {
        if (permShield < 3)
            permShield++;
    }

    /**
     * Decreases the permission shield's level.
     */
    public void decreasePermShield()
    {
        if (permShield > 0)
            permShield--;
    }

    /**
     * Checks whether the field's permission shield is raised or not.
     * 
     * @return true if the shield is raised
     */
    public boolean hasPermShield()
    {
        return permShield > 0;
    }

    /**
     * Checks whether the field's ban shield is raised or not.
     * 
     * @return true if the shield is raised
     */
    public boolean hasBanShield()
    {
        return banShield > 0;
    }

    /**
     * Transfers the shields' levels to their respective counterparts in the specified field.
     * 
     * @param field
     *            the field which the shields' values are transferred to
     */
    public void transferShields(ShieldedField field)
    {
        if (value == PermModelData.BANNED)
        {
            field.increaseBanShield();
        }
        else if (value == PermModelData.PERMITTED)
        {
            field.increasePermShield();
        }
        field.banShield += banShield;
        field.permShield += permShield;
    }

    /**
     * Checks if the wrapped value is of the 'permitted' or 'banned' type. Did-you-know-bonus: let f be a boolean
     * function having one boolean argument, then f(f(true)) = f(true) || f(false) and f(f(false)) = f(true) &&
     * f(false). Amazing, huh?
     * 
     * @return false if the wrapped value is 'default'
     */
    public boolean hasValue()
    {
        return hasValue(hasValue(true));
    }

    /**
     * Checks if the wrapped value is of the 'permitted' or 'banned' type, depending on the parameter's value.
     * 
     * @param isBan
     *            whether the wrapped value must match the 'permitted' (false) or 'banned' (true) value
     * @return true if the wrapped value is of the right type
     */
    public boolean hasValue(boolean isBan)
    {
        if (isBan)
        {
            return value == PermModelData.BANNED;
        }
        else
        {
            return value == PermModelData.PERMITTED;
        }
    }

    /**
     * Checks if the field is enabled, i.e can contain a value.
     * 
     * @return true if the field is enabled.
     */
    public boolean isEnabled()
    {
        return enabled;
    }

}
