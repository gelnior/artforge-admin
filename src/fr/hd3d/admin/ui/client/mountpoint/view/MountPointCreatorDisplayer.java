package fr.hd3d.admin.ui.client.mountpoint.view;

import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;


public class MountPointCreatorDisplayer
{

    /** Window needed for sheet editing. */
    private static MountPointCreatorDialog mountPointCreator;

    /**
     * @return Asset creation dialog.
     */
    public static MountPointCreatorDialog getDialog()
    {
        if (mountPointCreator == null)
        {
            mountPointCreator = new MountPointCreatorDialog();
        }
        return mountPointCreator;
    }

    public static void show(EventType okEvent)
    {

        MountPointCreatorDialog dialog = getDialog();

        dialog.reset();
        dialog.setOkEvent(okEvent);

        dialog.show();
    }

}
