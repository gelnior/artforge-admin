package fr.hd3d.admin.ui.client.model.modeldata;

import fr.hd3d.common.ui.client.modeldata.reader.IPagingReader;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.reader.ResourceGroupReader;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;


/**
 * This singleton class provides the application with the adequate resource group reader, paging or not.
 * 
 * @author HD3D
 * 
 */
public class SingleResourceGroupReader
{

    private static IReader<ResourceGroupModelData> lReader;
    private static IPagingReader<ResourceGroupModelData> pReader;

    /**
     * Call in order to get a paging resource group reader.
     * 
     * @return the adequate paging resource group reader
     */
    public static IPagingReader<ResourceGroupModelData> getPagingReader()
    {
        if (pReader == null)
        {
            pReader = new ResourceGroupPagingReader();
        }
        return pReader;
    }

    /**
     * Call in order to get a resource group reader.
     * 
     * @return the adequate resource group reader
     */
    public static IReader<ResourceGroupModelData> getReader()
    {
        if (lReader == null)
        {
            lReader = new ResourceGroupReader();
        }
        return lReader;
    }

    /**
     * Sets the inner instance to the specified parameter.
     * 
     * @param reader
     *            the new reader instance
     */
    public static void setSimpleInstance(IReader<ResourceGroupModelData> reader)
    {
        if (reader != null)
        {
            lReader = reader;
        }
    }

    /**
     * Sets the inner instance to the specified parameter.
     * 
     * @param reader
     *            the new paging reader instance
     */
    public static void setInstance(IPagingReader<ResourceGroupModelData> reader)
    {
        if (reader != null)
        {
            pReader = reader;
        }
    }

}
