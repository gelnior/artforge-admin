package fr.hd3d.admin.ui.client.rights.model;

import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.admin.ui.client.factoring.simplegrid.Hd3dSimpleGridModel;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IPagingReader;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;


/**
 * This model factors all methods for representing a record model data grid, supporting selection and refreshment.
 * 
 * @author HD3D
 * 
 * @param <D>
 *            the model data type
 */
public abstract class SubGridModel<D extends Hd3dModelData> extends Hd3dSimpleGridModel<D>
{

    protected D selected;

    /**
     * Creates a model fit for representing the grid
     */
    public SubGridModel()
    {
        super();
    }

    /**
     * Defines the paging reader for the model data type.
     * 
     * @return the adequate paging reader
     */
    protected abstract IPagingReader<D> getPagingReader();

    /**
     * Defines the list reader for the model data type.
     * 
     * @return the adequate list reader
     */
    protected abstract IReader<D> getSimpleReader();

    /**
     * Defines whether the grid must be paginated or not.
     * 
     * @return true in this case, but it can be overridden by children
     */
    public boolean wantsPaging()
    {
        return true;
    }

    /**
     * Initializes the model's proxy and loads the data for the grid. The type of initialization (i.e paginated or not)
     * will depend on the result of the {@link #wantsPaging()} method.
     */
    public void initProxyData()
    {
        if (wantsPaging())
        {
            initWithPaging(getPagingReader());
        }
        else
        {
            initWithoutPaging(getSimpleReader());
        }
    }

    /**
     * Set the selected <code>D</code> instance.
     * 
     * @param selected
     *            the new instance to select
     */
    public void setSelected(D selected)
    {
        this.selected = selected;
    }

    /**
     * Gets the selected <code>D</code> instance.
     * 
     * @return the selected instance
     */
    public D selected()
    {
        return selected;
    }

    /**
     * @return the event type for full reload without constraints
     */
    public abstract EventType allEventType();

    /**
     * @return the event type for single item selection
     */
    public abstract EventType selectionEventType();

}
