package fr.hd3d.admin.ui.client.users.controller;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.controller.SubController;
import fr.hd3d.admin.ui.client.view.ISubView;


/**
 * The controller for the 'User Accounts Management' application tab.
 * 
 * @author HD3D
 * 
 */
public class UsersController extends SubController
{

    public UsersController(ISubView tab)
    {
        super(tab);
    }

    protected void registerEvents()
    {
        super.registerEvents();
    }

    protected void doHandleEvent(AppEvent event)
    {}

}
