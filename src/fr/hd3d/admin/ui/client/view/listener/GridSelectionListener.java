package fr.hd3d.admin.ui.client.view.listener;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * A listener specifically designed for grid selection.
 * 
 * @author HD3D
 * 
 * @param <D>
 *            the model data type
 */
public class GridSelectionListener<D extends Hd3dModelData> implements Listener<GridEvent<D>>
{

    /** the relayed event type */
    protected EventType eventType;

    public GridSelectionListener(EventType type)
    {
        eventType = type;
    }

    public void handleEvent(GridEvent<D> ge)
    {
        AppEvent event = new AppEvent(eventType);
        D object = ge.getGrid().getStore().getAt(ge.getRowIndex());
        event.setData(object);
        EventDispatcher.forwardEvent(event);
    }

}
