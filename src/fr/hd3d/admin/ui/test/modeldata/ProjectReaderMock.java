package fr.hd3d.admin.ui.test.modeldata;

import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.Hd3dListJsonReaderMock;


public class ProjectReaderMock extends Hd3dListJsonReaderMock<ProjectModelData>
{

    public ProjectReaderMock()
    {
        super();
        setModelType(ProjectModelData.getModelType());
    }

    public ProjectModelData newModelInstance()
    {
        return new ProjectModelData();
    }

}
