package fr.hd3d.admin.ui.client.templates.view;

import fr.hd3d.admin.ui.client.perms.view.PermsPanel;
import fr.hd3d.admin.ui.client.templates.model.TemplatesPermsModel;


/**
 * The permissions panel specifically designed for the 'Security Templates Management' application.
 * 
 * @author HD3D
 * 
 */
public class TemplatesPermsPanel extends PermsPanel
{

    public TemplatesPermsPanel(TemplatesPermsModel model)
    {
        super(model);
    }

}
