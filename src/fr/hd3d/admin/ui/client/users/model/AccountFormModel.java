package fr.hd3d.admin.ui.client.users.model;

import org.restlet.client.data.Form;
import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.BaseModelData;

import fr.hd3d.admin.ui.client.config.AdminConfig;
import fr.hd3d.admin.ui.client.model.callback.AccountEventCallBack;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;


/**
 * The model data type for a single user account.
 * 
 * @author HD3D
 * 
 */
public class AccountFormModel extends BaseModelData
{

    /** the account principal */
    private String principal;

    private static final long serialVersionUID = -1445481463050374010L;

    /** the account password field */
    public final static String PASSWORD_ATTRIBUTE = "password_attribute";

    /** the account optional new password field */
    public final static String NEWPASS_ATTRIBUTE = "new_pass_attribute";

    /**
     * Creates a new model data with specified principal.
     * 
     * @param principal
     *            the model data principal
     */
    public AccountFormModel(String principal)
    {
        super();
        this.principal = principal;
    }

    /**
     * Gets the principal of the user account this account form model stands for
     * 
     * @return the account's principal
     */
    public String getPrincipal()
    {
        return principal;
    }

    /**
     * Gets the account form model data password.
     * 
     * @return the account password
     */
    public String getPassword()
    {
        return (String) get(PASSWORD_ATTRIBUTE);
    }

    /**
     * Sets the account form model data password.
     */
    public void setPassword(String password)
    {
        set(PASSWORD_ATTRIBUTE, password);
    }

    /**
     * Gets the account form model data new password.
     * 
     * @return the account new password
     */
    public String getNewPass()
    {
        return (String) get(NEWPASS_ATTRIBUTE);
    }

    /**
     * Sets the account form model data new password.
     */
    public void setNewPass(String password)
    {
        set(NEWPASS_ATTRIBUTE, password);
    }

    /**
     * Ensures memory protection on sensible data by removing password fields content.
     */
    public void clear()
    {
        String boom = remove(NEWPASS_ATTRIBUTE);
        boom = null;
        boom = remove(PASSWORD_ATTRIBUTE);
        if (boom != null)
        {
            boom = null;
        }
    }

    /**
     * Sends a put request to the server so that it changes the account password into the new password field value.
     */
    public void editAccount()
    {
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();

        Form form = new Form();
        form.add(AdminConfig.USERACCOUNT_PASSWORD_ATTRIBUTE, getPassword());
        form.add(AdminConfig.USERACCOUNT_NEWPASS_ATTRIBUTE, getNewPass());
        clear();

        // FIXME should be HTTPS
        requestHandler.handleAttributeRequest(Method.PUT, getAccountPath(), form, new AccountEventCallBack());
        form.clear();
    }

    /**
     * Sends a post request to the server so that it creates an account with optionally specified password field value.
     */
    public void createAccount()
    {
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();

        Form form = new Form();
        form.add(AdminConfig.USERACCOUNT_PASSWORD_ATTRIBUTE, getPassword());
        clear();

        // FIXME should be HTTPS
        requestHandler.handleAttributeRequest(Method.POST, getAccountPath(), form, new AccountEventCallBack());
        form.clear();
    }

    /**
     * Calculates the account path with its principal.
     * 
     * @return the account resource path
     */
    private String getAccountPath()
    {
        if (principal != null)
        {
            return AdminConfig.USERACCOUNTS_PATH + principal;
        }
        return null;
    }

}
