package fr.hd3d.admin.ui.client.templates.view;

import fr.hd3d.admin.ui.client.templates.model.RoleTemplateModel;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;


/**
 * This panel contains the grid with the roles owned by the selected security template.
 * 
 * @author HD3D
 * 
 */
public class OwnedRolePanel extends RoleTemplatePanel
{

    /**
     * Builds a role panel without 'add' button yet provided with a 'remove' button
     * 
     * @param model
     *            the adequate role model
     */
    public OwnedRolePanel(RoleTemplateModel model)
    {
        super(model, false, true);
    }

    protected void setStyle()
    {
        setHeading(AdminMainView.CONSTANTS.OwnedRoles());
        super.setStyle();
    }

    protected void setActionToolBar()
    {
        super.setActionToolBar();
        createButton.disable();
    }

    public void onTemplateDeselection()
    {
        super.onTemplateDeselection();
        createButton.disable();
    }

    public void onTemplateSelection(boolean canUpdateTemplate)
    {
        super.onTemplateSelection(canUpdateTemplate);
        if (canUpdateTemplate && PermissionUtil.hasCreateRights(model.getRoleClassName()))
        {
            createButton.enable();
        }
    }

}
