package fr.hd3d.admin.ui.client.perms.view;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;
import com.extjs.gxt.ui.client.widget.treegrid.WidgetTreeGridCellRenderer;
import com.google.gwt.user.client.ui.Widget;

import fr.hd3d.admin.ui.client.model.modeldata.GenericModelData;
import fr.hd3d.admin.ui.client.perms.model.PermModelData;
import fr.hd3d.admin.ui.client.perms.model.PermsTreeStoreUtil;
import fr.hd3d.admin.ui.client.perms.model.ShieldedField;
import fr.hd3d.admin.ui.client.view.AdminMainView;


/**
 * This class is a provider for all the useful column rendering objects of the permission grid.
 * 
 * @author HD3D
 * 
 */
public final class PermsColumnRenderers
{

    /**
     * Creates a rendering object for the tree cell.
     * 
     * @return a renderer taking the model data type into account
     */
    public static WidgetTreeGridCellRenderer<PermModelData> getTypeRenderer()
    {
        return new TypeRenderer();
    }

    /**
     * Creates a rendering object for the cell.
     * 
     * @return a renderer taking the model data type into account
     */
    public static GridCellRenderer<GenericModelData> getShortTypeRenderer()
    {
        return new ShortTypeRenderer();
    }

    /**
     * Creates a rendering object for the expansion/collapsing column.
     * 
     * @return a two-state renderer
     */
    public static GridCellRenderer<PermModelData> getExpandRenderer()
    {
        return new ExpandRenderer();
    }

    /**
     * Creates a rendering object for the permission's right values.
     * 
     * @return a renderer for right values
     */
    public static GridCellRenderer<PermModelData> getPermRenderer()
    {
        return new PermissionRenderer();
    }

    static class TypeRenderer extends WidgetTreeGridCellRenderer<PermModelData>
    {
        public Widget getWidget(PermModelData model, String property, ColumnData config, int rowIndex, int colIndex,
                ListStore<PermModelData> store, Grid<PermModelData> grid)
        {
            String style;
            String message;
            int type = model.getType();
            switch (type)
            {
                case PermModelData.VERSION:
                    message = AdminMainView.CONSTANTS.PermTypeVersion();
                    style = "darkgreen";
                    break;
                case PermModelData.ALL:
                    message = AdminMainView.CONSTANTS.PermTypeAll();
                    style = "crimson";
                    break;
                case PermModelData.OBJECT:
                    message = AdminMainView.CONSTANTS.PermTypeObject();
                    style = "black";
                    break;
                case PermModelData.ID:
                    message = AdminMainView.CONSTANTS.PermTypeId();
                    style = "indigo";
                    break;
                case PermModelData.OTHER:
                    message = AdminMainView.CONSTANTS.PermTypeOther();
                    style = "darkslategrey";
                    break;
                default:
                    message = AdminMainView.CONSTANTS.PermTypeUnknown();
                    style = "grey";
            }
            LabelToolItem item = new LabelToolItem("<span style='color:" + style + "'>" + model.getLabel() + "</span>");
            item.setToolTip(message);
            return item;
        }
    }

    static class ShortTypeRenderer implements GridCellRenderer<GenericModelData>
    {
        public Object render(GenericModelData model, String property, ColumnData config, int rowIndex, int colIndex,
                ListStore<GenericModelData> store, Grid<GenericModelData> grid)
        {
            return "<span style='color:black'>" + model.getLabel() + "</span>";
        }
    }

    static class ExpandRenderer implements GridCellRenderer<PermModelData>
    {
        public Object render(PermModelData model, String property, ColumnData config, int rowIndex, int colIndex,
                ListStore<PermModelData> store, Grid<PermModelData> grid)
        {
            if (model.getType() == PermModelData.OBJECT || model.getType() == PermModelData.ID
                    || model.getType() == PermModelData.VERSION || model.getType() == PermModelData.OTHER)
            {
                if (!PermsTreeStoreUtil.isLeaf(model))
                {
                    if (model.expanded)
                    {
                        return "<span style='color:red'>" + AdminMainView.CONSTANTS.CollapseNode() + "</span>";
                    }
                    return "<span style='color:teal'>" + AdminMainView.CONSTANTS.ExpandNode() + "</span>";
                }
            }
            return "";
        }
    }

    static class PermissionRenderer implements GridCellRenderer<PermModelData>
    {
        public Object render(PermModelData model, String property, ColumnData config, int rowIndex, int colIndex,
                ListStore<PermModelData> store, Grid<PermModelData> grid)
        {
            ShieldedField value = (ShieldedField) model.get(property);
            int status = value.getValue();

            String empty = "";
            /* debug? */
            boolean debug = false; // true to activate debugging
            if (debug)
            {
                int permshield = getPermShield(value);
                int banshield = getBanShield(value);
                empty = permshield + " ---- " + banshield;
            }

            String image;
            switch (status)
            {
                case PermModelData.DEFAULT:
                    image = "default-check";
                    break;
                case PermModelData.PERMITTED:
                    image = "permitted-check";
                    break;
                case PermModelData.BANNED:
                    image = "banned-check";
                    break;
                case PermModelData.DEDUCED_PERMISSION:
                    image = "deduced-perm-check";
                    break;
                case PermModelData.DEDUCED_BAN:
                    image = "deduced-ban-check";
                    break;
                default:
                    return "<span style='color:gray'>" + empty + "</span>";
            }
            return "<div class='" + image + "'>" + empty + "&nbsp;</div>";
        }
    }

    /* debug */
    private static int getPermShield(ShieldedField field)
    {
        int s = 0;
        while (field.hasPermShield())
        {
            field.decreasePermShield();
            s++;
        }
        for (int i = 0; i < s; i++)
        {
            field.increasePermShield();
        }
        return s;
    }

    /* debug */
    private static int getBanShield(ShieldedField field)
    {
        int s = 0;
        while (field.hasBanShield())
        {
            field.decreaseBanShield();
            s++;
        }
        for (int i = 0; i < s; i++)
        {
            field.increaseBanShield();
        }
        return s;
    }

}
