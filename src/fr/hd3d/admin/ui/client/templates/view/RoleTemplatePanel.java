package fr.hd3d.admin.ui.client.templates.view;

import fr.hd3d.admin.ui.client.templates.model.RoleTemplateModel;
import fr.hd3d.admin.ui.client.view.AbstractRolePanel;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;


/**
 * A specification of the <code>AbstractRolePanel</code> that can adapt to security template selection.
 * 
 * @author HD3D
 * 
 */
public abstract class RoleTemplatePanel extends AbstractRolePanel implements IRoleTemplateView
{

    /** whether a security template has been selected or not */
    public boolean templateSelected;
    /** whether the user can update selected template */
    private boolean canUpdateTemplate;

    public RoleTemplatePanel(RoleTemplateModel model, boolean hasAdd, boolean hasRemove)
    {
        super(model, hasAdd, hasRemove);
    }

    protected void setActionToolBar()
    {
        super.setActionToolBar();
        allButton.enable();
        allButton.setText("");
        allButton.removeAllListeners();
        allButton.addSelectionListener(new ButtonClickListener(((RoleTemplateModel) model).refreshAllEvent()));
    }

    public void onItemSelection(boolean canUpdate, boolean canDelete)
    {
        super.onItemSelection(canUpdate, canDelete);
        if (templateSelected)
        {
            if (hasAdd && canUpdateTemplate)
            {
                addButton.enable();
            }
            if (hasRemove && canUpdateTemplate)
            {
                removeButton.enable();
            }
        }
    }

    public void onFullDeselection()
    {
        super.onFullDeselection();
        if (hasAdd)
        {
            addButton.disable();
        }
    }

    public void onTemplateSelection(boolean canUpdate)
    {
        templateSelected = true;
        canUpdateTemplate = canUpdate;
    }

    public void onTemplateDeselection()
    {
        templateSelected = false;
    }

}
