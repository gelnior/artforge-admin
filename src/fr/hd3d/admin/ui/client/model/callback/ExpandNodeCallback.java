package fr.hd3d.admin.ui.client.model.callback;

import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.hd3d.admin.ui.client.model.modeldata.GenericModelData;
import fr.hd3d.admin.ui.client.perms.model.PermModelData;
import fr.hd3d.admin.ui.client.perms.model.PermsModel;
import fr.hd3d.admin.ui.client.perms.view.ManualFilterDialog;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * This call-back is used when a request is sent to the server upon tree store node expansion. This request asks the
 * server for the children IDs of the specified node and then the call-back will transmit all the returned
 * <code>Hd3dModelData</code> to the permission model upon server response.
 * 
 * @author HD3D
 * 
 */
public class ExpandNodeCallback implements AsyncCallback<ListLoadResult<GenericModelData>>
{
    private PermsModel model;
    private PermModelData node;

    private boolean update;

    /**
     * Creates a call-back for the specified node's expansion.
     * 
     * @param model
     *            the permission model to which the response is passed on
     * @param node
     *            the node to expand
     * @param update
     *            whether incoming data will serve as update or new input
     */
    public ExpandNodeCallback(PermsModel model, PermModelData node, boolean update)
    {
        this.node = node;
        this.model = model;
        this.update = update;
    }

    public void onFailure(Throwable caught)
    {
        EventDispatcher.forwardEvent(CommonEvents.ERROR);
    }

    public void onSuccess(ListLoadResult<GenericModelData> result)
    {
        if (update)
        {
            model.updateData(node, result.getData());
        }
        else
        {
            ManualFilterDialog.getInstance().show(result.getData(), model, node);
        }
    }

}
