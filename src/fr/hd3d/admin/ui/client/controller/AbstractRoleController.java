package fr.hd3d.admin.ui.client.controller;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.model.AbstractRoleModel;
import fr.hd3d.admin.ui.client.view.IAbstractRoleView;
import fr.hd3d.admin.ui.client.rights.controller.SubGridController;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;


/**
 * The controller that will handle the events for an {@link fr.hd3d.admin.ui.client.view.IAbstractRoleView}.
 * 
 * @author HD3D
 * 
 * @param <M>
 *            the model type
 * @param <V>
 *            the view type
 */
public abstract class AbstractRoleController<M extends AbstractRoleModel, V extends IAbstractRoleView> extends
        SubGridController<RoleModelData, M, V>
{

    public AbstractRoleController(M model, V view)
    {
        super(model, view);
    }

    protected void doHandleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == model.creationClickEvent())
        {
            onCreationDemand();
        }
        else if (type == model.creationConfirmEvent())
        {
            String name = event.<String> getData();
            onCreation(name);
            fullyLoaded = false;
        }
        else if (type == model.editionClickEvent())
        {
            onEditionDemand();
        }
        else if (type == model.editionConfirmEvent())
        {
            String name = event.<String> getData();
            onEdition(name);
        }
        else if (type == model.deleteClickEvent())
        {
            onDeletion();
        }
        else if (type == model.refreshNeededEvent())
        {
            onItemRefreshment();
        }
        else if (type == model.removeClickEvent())
        {
            view.onFullDeselection();
            onRemovalDemand();
        }
        else if (type == model.addingClickEvent())
        {
            view.onFullDeselection();
            onAddingDemand();
        }
    }

    protected void registerEvents()
    {
        super.registerEvents();
        registerEventTypes(model.refreshNeededEvent());
        registerEventTypes(model.editionClickEvent());
        registerEventTypes(model.creationClickEvent());
        registerEventTypes(model.deleteClickEvent());
        registerEventTypes(model.creationConfirmEvent());
        registerEventTypes(model.editionConfirmEvent());
        registerEventTypes(model.removeClickEvent());
        registerEventTypes(model.addingClickEvent());
    }

    /**
     * Reacts to a creation demand by asking the view to show a 'create dialog'.
     */
    protected void onCreationDemand()
    {
        view.onCreateDemand();
    }

    /**
     * Handles a role creation request.
     * 
     * @param name
     *            the new role's name
     */
    protected void onCreation(String name)
    {
        model.createLine(name);
    }

    /**
     * Handles a role deletion request.
     */
    protected void onDeletion()
    {
        view.onFullDeselection();
        model.deleteLine();
    }

    /**
     * Reacts to a creation demand by asking the view to show a 'edit dialog'.
     */
    protected void onEditionDemand()
    {
        view.onEditDemand();
    }

    /**
     * Handles a role's name edition request.
     * 
     * @param name
     *            the role's new name
     */
    protected void onEdition(String name)
    {
        model.editLine(name);
    }

    /**
     * This abstract method is called upon an 'add role' request.
     */
    protected abstract void onAddingDemand();

    /**
     * This abstract method is called upon a 'remove role' request.
     */
    protected abstract void onRemovalDemand();

    /**
     * Refreshes the constraints on the role store when the store is refreshed.
     */
    protected void onItemRefreshment()
    {
        clearConstraints();
        setupConstraints();
        refreshStore();
    }

}
