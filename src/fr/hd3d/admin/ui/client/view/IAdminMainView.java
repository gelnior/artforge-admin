package fr.hd3d.admin.ui.client.view;

import fr.hd3d.common.ui.client.widget.mainview.IMainView;


/**
 * This interface represents the main application view.
 * 
 * @author HD3D
 */
public interface IAdminMainView extends IMainView
{
    /**
     * Builds all widgets before display.
     */
    public void initWidgets();

}
