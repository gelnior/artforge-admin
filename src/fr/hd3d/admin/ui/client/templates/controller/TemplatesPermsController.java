package fr.hd3d.admin.ui.client.templates.controller;

import java.util.LinkedList;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.perms.controller.PermsController;
import fr.hd3d.admin.ui.client.perms.view.IPermsView;
import fr.hd3d.admin.ui.client.templates.model.TemplatesPermsModel;


/**
 * A controller capable of handling all the events related to the permission and bans panel.
 * 
 * @author HD3D
 * 
 */
public class TemplatesPermsController extends PermsController
{

    public TemplatesPermsController(TemplatesPermsModel model, IPermsView view)
    {
        super(model, view);
    }

    protected List<EventType> defineRoleDeselectionEvents()
    {
        List<EventType> events = new LinkedList<EventType>();
        events.add(AdminEvents.TEMP_OWN_DELETE_CLICKED);
        events.add(AdminEvents.TEMP_OWN_REFRESH_CLICKED);
        events.add(AdminEvents.TEMP_OWN_REMOVE_CLICKED);
        events.add(AdminEvents.TEMP_ALL_DELETE_CLICKED);
        events.add(AdminEvents.TEMP_ALL_REFRESH_CLICKED);
        return events;
    }

    protected List<EventType> defineRoleModificationEvents()
    {
        List<EventType> events = new LinkedList<EventType>();
        events.add(AdminEvents.TEMP_OWN_SERVER_MODIFICATION);
        events.add(AdminEvents.TEMP_ALL_SERVER_MODIFICATION);
        return events;
    }

    protected List<EventType> defineRoleSelectionEvents()
    {
        List<EventType> events = new LinkedList<EventType>();
        events.add(AdminEvents.TEMP_OWN_SELECTED);
        events.add(AdminEvents.TEMP_ALL_SELECTED);
        return events;
    }

}
