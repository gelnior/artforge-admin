package fr.hd3d.admin.ui.client.model.modeldata;

import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.ListLoadResult;

import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ProjectReader;


/**
 * This singleton class provides the application with the adequate Project reader.
 * 
 * @author HD3D
 * 
 */
public abstract class SingleProjectReader
{

    private static DataReader<ListLoadResult<ProjectModelData>> lReader;

    /**
     * Call in order to get a Project reader.
     * 
     * @return the adequate Project reader
     */
    public static DataReader<ListLoadResult<ProjectModelData>> getReader()
    {
        if (lReader == null)
        {
            lReader = new ProjectReader();
        }
        return lReader;
    }

    /**
     * Sets the inner instance to the specified parameter.
     * 
     * @param reader
     *            the new reader instance
     */
    public static void setInstance(DataReader<ListLoadResult<ProjectModelData>> reader)
    {
        if (reader != null)
        {
            lReader = reader;
        }
    }

}
