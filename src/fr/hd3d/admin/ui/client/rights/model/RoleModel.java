package fr.hd3d.admin.ui.client.rights.model;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.model.AbstractRoleModel;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;


/**
 * This model specifies the role model provided by {@link fr.hd3d.admin.ui.client.model.AbstractRoleModel} inasmuch as
 * it takes the resource group selection into account.
 * 
 * @author HD3D
 * 
 */
public class RoleModel extends AbstractRoleModel
{

    /** the selected resource group */
    ResourceGroupModelData selectedGroup;

    protected EventType onCreationWhileAssociated(RoleModelData role)
    {
        if (selectedGroup != null)
        {
            List<Long> groups = role.getResourceGroupIds();
            groups.add(selectedGroup.getId());
            role.setResourceGroupIds(groups);
            return AdminEvents.SERVER_SELECTED_GROUP_MODIFICATION;
        }
        return super.onCreationWhileAssociated(role);
    }

    /**
     * Removes the selected role from the selected resource group.
     */
    public void removeLine()
    {
        List<Long> groups = selected.getResourceGroupIds();
        groups.remove(selectedGroup.getId());
        selected.setResourceGroupIds(groups);
        selected.save(AdminEvents.SERVER_SELECTED_GROUP_MODIFICATION);
        setSelected(null);
    }

    /**
     * Sets the selected resource group.
     * 
     * @param rGroup
     *            the resource group to select
     */
    public void setSelectedGroup(ResourceGroupModelData rGroup)
    {
        selectedGroup = rGroup;
    }

    /**
     * Gets the selected resource group.
     * 
     * @return the selected resource group.
     */
    public ResourceGroupModelData getSelectedGroup()
    {
        return selectedGroup;
    }

    public EventType allEventType()
    {
        return AdminEvents.ALL_ROLES_CLICKED;
    }

    public EventType selectionEventType()
    {
        return AdminEvents.ROLE_SELECTED;
    }

    public EventType addingClickEvent()
    {
        return AdminEvents.ADD_ROLE_CLICKED;
    }

    public EventType creationClickEvent()
    {
        return AdminEvents.CREATE_ROLE_CLICKED;
    }

    public EventType creationConfirmEvent()
    {
        return AdminEvents.CREATE_ROLE_CONFIRMED;
    }

    public EventType deleteClickEvent()
    {
        return AdminEvents.DELETE_ROLE_CLICKED;
    }

    public EventType editionClickEvent()
    {
        return AdminEvents.EDIT_ROLE_CLICKED;
    }

    public EventType editionConfirmEvent()
    {
        return AdminEvents.EDIT_ROLE_CONFIRMED;
    }

    public EventType removeClickEvent()
    {
        return AdminEvents.REMOVE_ROLE_CLICKED;
    }

    public EventType refreshNeededEvent()
    {
        return AdminEvents.SERVER_ROLES_MODIFICATION;
    }

}
