package fr.hd3d.admin.ui.client.templates.model;

import java.util.Arrays;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.config.AdminConfig;
import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.perms.model.PermModelData;
import fr.hd3d.admin.ui.client.perms.model.PermsModel;
import fr.hd3d.admin.ui.client.perms.model.PermsTreeStoreUtil;
import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * The permissions and bans management model for the 'Security Templates Management' application.
 * 
 * @author HD3D
 * 
 */
public class TemplatesPermsModel extends PermsModel
{

    public boolean isTemplate()
    {
        return true;
    }

    public void requestData(PermModelData node, boolean update)
    {
        String path = node.getPath();
        if (path.endsWith(":projects")) // in case of templates, project is uninstantiated
        {
            if (!update)
            {
                PermModelData project = new PermModelData(AdminConfig.PROJECT_ID_MARK, PermModelData.ID, path + ":"
                        + AdminConfig.PROJECT_ID_MARK);
                PermsTreeStoreUtil.appendChosenChildren(tree, node, Arrays.asList(project), expansionFinishedEvent());
            }
        }
        else if (path.contains(":projects:" + AdminConfig.PROJECT_ID_MARK)) // do nothing
        {
            if (!update)
            {
                node.expanded = true;
                AppEvent event = new AppEvent(expansionFinishedEvent());
                event.setData(node);
                EventDispatcher.forwardEvent(event);
            }
        }
        else
        {
            super.requestData(node, update);
        }
    }

    /* event types */

    public EventType fullUndoEvent()
    {
        return AdminEvents.TEMP_PERM_LIST_FULL_UNDO;
    }

    public EventType redoEvent()
    {
        return AdminEvents.TEMP_PERM_LIST_REDO;
    }

    public EventType saveEvent()
    {
        return AdminEvents.TEMP_PERM_LIST_SAVE;
    }

    public EventType undoEvent()
    {
        return AdminEvents.TEMP_PERM_LIST_UNDO;
    }

    public EventType canCreateClickedEvent()
    {
        return AdminEvents.TEMP_CAN_CREATE_CLICKED;
    }

    public EventType canReadClickedEvent()
    {
        return AdminEvents.TEMP_CAN_READ_CLICKED;
    }

    public EventType canDeleteClickedEvent()
    {
        return AdminEvents.TEMP_CAN_DELETE_CLICKED;
    }

    public EventType canAllClickedEvent()
    {
        return AdminEvents.TEMP_CAN_ALL_CLICKED;
    }

    public EventType canUpdateClickedEvent()
    {
        return AdminEvents.TEMP_CAN_UPDATE_CLICKED;
    }

    public EventType expandNodeEvent()
    {
        return AdminEvents.TEMP_EXPAND_NODE;
    }

    public EventType collapseNodeEvent()
    {
        return AdminEvents.TEMP_REDUCE_NODE;
    }

    public EventType getNodeChidrenIDs()
    {
        return AdminEvents.TEMP_GET_NODE_CHILDREN;
    }

    public EventType expansionFinishedEvent()
    {
        return AdminEvents.TEMP_EXPANSION_FINISHED;
    }

    public EventType refreshGridEvent()
    {
        return AdminEvents.TEMP_REFRESH_PERM_GRID;
    }

}
