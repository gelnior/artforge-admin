package fr.hd3d.admin.ui.test;

import org.restlet.client.Request;
import org.restlet.client.Response;

import fr.hd3d.admin.ui.client.model.AdminMainModel;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


/**
 * A simple call back function that assumes the model is connected to the server on success.
 * 
 * @author HD3D
 * 
 */
public class TestCallback extends BaseCallback
{

    /** the model to connect */
    AdminMainModel model;

    public TestCallback(AdminMainModel model)
    {
        this.model = model;
    }

    protected void onSuccess(Request request, Response response)
    {
        model.setConnected();
    }

}
