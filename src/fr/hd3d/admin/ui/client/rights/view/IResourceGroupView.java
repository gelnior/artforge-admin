package fr.hd3d.admin.ui.client.rights.view;

import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;


/**
 * The <code>ISubGridView</code> specification for a resource group model data containing grid panel.
 * 
 * @author HD3D
 * 
 */
public interface IResourceGroupView extends ISubGridView<ResourceGroupModelData>
{

    /**
     * Resets the constraints-defining combo boxes
     * 
     * @param person
     *            whether to reset the person combo box
     * @param project
     *            whether to reset the project combo box
     */
    public void resetComboBoxes(boolean person, boolean project);

}
