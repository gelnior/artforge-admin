package fr.hd3d.admin.ui.client.perms.view;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.perms.model.PermModelData;


/**
 * Specifies what the permission handling view minimal functions.
 * 
 * @author HD3D
 * 
 */
public interface IPermsView
{

    /**
     * Initializes all the inner widget content.
     */
    public void setup();

    /**
     * Performs enabling/disabling actions upon role selection.
     */
    public void onRoleSelection();

    /**
     * Shows the view's inner tree grid on this request.
     */
    public void showGrid();

    /**
     * Orders the view inner tree grid to expand or collapse the specified node. In case of an expansion, recusrively
     * expands all children.
     * 
     * @param node
     *            the node to expand or collapse
     * @param expanded
     *            true to expand, false to collapse
     */
    public void setNodeExpanded(PermModelData node, boolean expanded);

    /**
     * Performs enabling/disabling actions upon undo-able action.
     */
    public void onUndoableAction();

    /**
     * Performs enabling/disabling actions upon 'undo'.
     */
    public void onUndo();

    /**
     * Disables the 'redo' button.
     */
    public void onEmptyRedoStack();

    /**
     * Disables the 'undo' and 'save' button if requested.
     * 
     * @param saveAlso
     *            whether to disable the 'save' button also
     */
    public void onEmptyUndoStack(boolean saveAlso);

    /**
     * Shows a 'quit and/or save' dialog that allows the user to cancel or save all modifications on selected role's
     * properties.
     * 
     * @param delayedEvent
     *            the event that triggered this call
     */
    public void showQuitAndSaveDialog(AppEvent delayedEvent);

    /**
     * Refreshes the inner grid's view according to current store data.
     */
    public void refreshGrid();

    /**
     * Gets a string that stands for "all this node children"
     * 
     * @return an "all children" String
     */
    public String getAllChildrenString();

    /**
     * Gets a string that stands for "any version"
     * 
     * @return an "any version" String
     */
    public String getAnyVersionString();

}
