package fr.hd3d.admin.ui.client.users.controller;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.admin.ui.client.config.AdminConfig;
import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.users.model.AccountsModel;
import fr.hd3d.admin.ui.client.users.view.IAccountView;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;


/**
 * The main user accounts controller, capable of handling all the user accounts events and operations.
 * 
 * @author HD3D
 * 
 */
public class AccountsController extends Controller
{

    /** the main account model */
    private AccountsModel model;
    /** the main account view */
    private IAccountView view;

    /**
     * Instantiates a controller for the specified model and view.
     * 
     * @param model
     *            the user accounts model
     * @param view
     *            the user accounts view
     */
    public AccountsController(AccountsModel model, IAccountView view)
    {
        this.model = model;
        this.view = view;
        registerEvents();
    }

    public void handleEvent(AppEvent event)
    {
        forwardToChild(event);

        EventType type = event.getType();

        if (type == CommonEvents.PERMISSION_INITIALIZED)
        {
            onPermissionInitialized();
        }
        else if (type == AdminEvents.USER_SELECTED)
        {
            PersonModelData user = event.<PersonModelData> getData();
            onUserSelection(user);
        }
        else if (type == AdminEvents.REFRESH_USERS_LIST)
        {
            onUsersRefresh();
        }
        else if (type == AdminEvents.USER_CREATE_ACCOUNT_CLICKED)
        {
            onAccountCreationDemand();
        }
        else if (type == AdminEvents.USER_DELETE_ACCOUNT_CLICKED)
        {
            onAccountDeletionDemand();
        }
        else if (type == AdminEvents.USER_DELETE_ACCOUNT_CONFIRMED)
        {
            onAccountDeletion();
        }
        else if (type == AdminEvents.USER_EDIT_ACCOUNT_CLICKED)
        {
            onAccountEditionDemand();
        }
    }

    /**
     * Register all the events handled by this controller.
     */
    protected void registerEvents()
    {
        registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
        registerEventTypes(AdminEvents.USER_SELECTED);
        registerEventTypes(AdminEvents.REFRESH_USERS_LIST);
        registerEventTypes(AdminEvents.USER_CREATE_ACCOUNT_CLICKED);
        registerEventTypes(AdminEvents.USER_DELETE_ACCOUNT_CLICKED);
        registerEventTypes(AdminEvents.USER_DELETE_ACCOUNT_CONFIRMED);
        registerEventTypes(AdminEvents.USER_EDIT_ACCOUNT_CLICKED);
    }

    /**
     * Called when the permissions have been acquired by the main controller, this method initializes the view.
     */
    protected void onPermissionInitialized()
    {
        model.initModel();
        view.setup();
    }

    /**
     * Called upon person selection, this methods orders the model to select the person and assures the view takes the
     * selection into account.
     * 
     * @param user
     *            the selected person
     */
    protected void onUserSelection(PersonModelData user)
    {
        model.setSelected(user);
        String path = AdminConfig.USERACCOUNTS_PERM + ":" + user.getLogin();
        view.onUserSelection(PermissionUtil.hasCreateRightsOnPath(path), PermissionUtil.hasUpdateRightsOnPath(path),
                PermissionUtil.hasDeleteRightsOnPath(path));
    }

    /**
     * Orders user list full refreshment and user selection clearance in view and model.
     */
    protected void onUsersRefresh()
    {
        model.setSelected(null);
        model.refreshData();
        view.onUsersRefresh();
    }

    /**
     * Orders the view to display an account creation form dialog.
     */
    protected void onAccountCreationDemand()
    {
        view.onAccountCreationDemand();
    }

    /**
     * Orders the view to display an account deletion confirmation dialog.
     */
    protected void onAccountDeletionDemand()
    {
        view.onAccountDeletionDemand();
    }

    /**
     * Orders the model to delete selected user's account.
     */
    protected void onAccountDeletion()
    {
        model.deleteAccount();
    }

    /**
     * Orders the view to display a 'change password' form dialog.
     */
    protected void onAccountEditionDemand()
    {
        view.onAccountEditionDemand();
    }

}
