package fr.hd3d.admin.ui.client.rights.controller;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.controller.SubController;
import fr.hd3d.admin.ui.client.view.ISubView;


/**
 * The controller for the 'Roles and Rights Management' application tab.
 * 
 * @author HD3D
 * 
 */
public class RightsController extends SubController
{

    public RightsController(ISubView tab)
    {
        super(tab);
    }

    protected void registerEvents()
    {
        super.registerEvents();
    }

    protected void doHandleEvent(AppEvent event)
    {}

}
