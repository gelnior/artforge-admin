package fr.hd3d.admin.ui.test.rights;

import fr.hd3d.admin.ui.client.controller.SubController;
import fr.hd3d.admin.ui.client.rights.view.IResourceGroupView;
import fr.hd3d.admin.ui.client.rights.view.IRoleView;
import fr.hd3d.admin.ui.client.view.ISubView;


public class RightsViewMock implements ISubView
{

    private IRoleView r;
    private IResourceGroupView g;

    public boolean init = false;

    public RightsViewMock(RoleViewMock r, ResourceGroupViewMock g)
    {
        this.r = r;
        this.g = g;
    }

    public String getHeading()
    {
        return "heading";
    }

    public SubController init()
    {
        // no need for test
        return null;
    }

    public void initWidgets()
    {
        init = true;
        r.setup();
        g.setup();
    }

}
