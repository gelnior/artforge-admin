package fr.hd3d.admin.ui.client.perms.model;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreSorter;
import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.admin.ui.client.model.callback.ExpandNodeCallback;
import fr.hd3d.admin.ui.client.model.modeldata.GenericModelData;
import fr.hd3d.admin.ui.client.model.modeldata.SingleGenericReader;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.proxy.ServicesProxyCallback;


/**
 * A permission model that contains a permission model data tree store. It handles role selection and tree store
 * initialization upon role selection. Then it can order the tree store to be translated back to selected role's
 * attributes in order to save it.
 * 
 * @author HD3D
 * 
 */
public abstract class PermsModel
{

    /** the selected role */
    protected RoleModelData role;

    /** the tree store */
    protected TreeStore<PermModelData> tree;

    /** the reader to use in order to retrieve data from the server */
    private IReader<GenericModelData> reader;

    /* allows update at grid's first expansion */
    private boolean firstExpansion;

    /**
     * Creates a permission model.
     */
    @SuppressWarnings("unchecked")
    public PermsModel()
    {
        super();

        tree = new TreeStore<PermModelData>();
        tree.setStoreSorter(new PermSorter());

        reader = (IReader<GenericModelData>) SingleGenericReader.getReader();
    }

    /**
     * Tells whether the current model is standing for role templates or actual roles.
     * 
     * @return true if the handled roles are in fact role templates
     */
    public abstract boolean isTemplate();

    /**
     * Checks whether the root nodes are expanded for the first time.
     * 
     * @return true if the tree store has been freshly refreshed
     */
    public boolean isFirstExpansion()
    {
        return firstExpansion;
    }

    /**
     * Tells the model that the first expansion is over, orders the tree grid to be refreshed.
     */
    public void firstExpansionOver()
    {
        firstExpansion = false;
    }

    /**
     * Orders the initialization of the service URLs tree.
     */
    public void initServiceURLs()
    {
        PermsTreeStoreUtil.initServiceURLs();
    }

    /**
     * Sets selected role.
     * 
     * @param role
     *            the role to select
     */
    public void setRole(RoleModelData role)
    {
        this.role = role;
    }

    /**
     * Gets selected role's name.
     * 
     * @return selected role's name
     */
    public String getRoleName()
    {
        if (role == null)
        {
            return "";
        }
        return role.getName();
    }

    /**
     * Gets selected role model data.
     * 
     * @return selected role
     */
    public RoleModelData getRole()
    {
        return role;
    }

    /**
     * Refreshes the tree store. The method starts by clearing it and then adding all the role's permissions and bans.
     */
    public void refreshStore()
    {
        tree.removeAll();
        firstExpansion = true;
        if (role != null)
        {
            PermsTreeStoreUtil.insertInStore(tree, role.getPermissions(), role.getBans());

            /* debug */
            // GWT.log(role.getPermissions().toString() + '\n' + role.getBans().toString(), null);
        }
    }

    /**
     * Gets the model's permission model data tree store.
     * 
     * @return the model's tree store
     */
    public TreeStore<PermModelData> getStore()
    {
        return tree;
    }

    /**
     * Sends a 'GET' request to the server at the URL path specified inside the node in order to recover its children.
     * 
     * @param node
     *            the children requesting node
     * @param update
     *            whether the incoming data will be regarded as new input (false) or an update of already present
     *            objects (true)
     */
    public void requestData(PermModelData node, boolean update)
    {
        String path = PermsTreeStoreUtil.getServerPath(node);
        IRestRequestHandler handler = RestRequestHandlerSingleton.getInstance();
        handler.getRequest(path, new ServicesProxyCallback<GenericModelData>(reader, new ExpandNodeCallback(this, node,
                update)));
    }

    /**
     * Updates the list of a node's ID children with the label of server's actual data objects.
     * 
     * @param node
     *            the parent node
     * @param data
     *            the server data object list
     */
    public void updateData(PermModelData node, List<GenericModelData> data)
    {
        GenericModelData done;
        for (PermModelData child : tree.getChildren(node))
        {
            if (child.getType() == PermModelData.ID)
            {
                done = null;
                for (GenericModelData h : data)
                {
                    if (h.getId().toString().equals(child.getIdentifier()))
                    {
                        child.setLabel(h.getLabel()); // rename the child node
                        child = tree.getFirstChild(child);
                        if (child != null) // rename also the 'ALL' child node
                        {
                            child.setLabel(child.getLabel().replace(h.getId().toString(), h.getLabel()));
                        }
                        done = h;
                        break;
                    }
                }
                if (done != null)
                {
                    data.remove(done);
                }
            }
        }
        EventDispatcher.forwardEvent(refreshGridEvent());
    }

    /**
     * Computes the list of specified node's children and passes it to the tree store.
     * 
     * @param node
     *            the parent node
     * @param data
     *            the children list
     */
    public void appendData(PermModelData node, List<GenericModelData> data)
    {
        List<PermModelData> list = new ArrayList<PermModelData>();
        PermModelData child;
        String path = node.getPath() + ":";
        String id;
        for (GenericModelData h : data)
        {
            id = h.getId().toString();
            child = new PermModelData(id, PermModelData.ID, path + id);
            child.setLabel(h.getLabel());
            list.add(child);
        }

        PermsTreeStoreUtil.appendChosenChildren(tree, node, list, expansionFinishedEvent());
    }

    /**
     * Saves changes on the specified role. If it is null, save changes on selected role.
     * 
     * @param toSave
     *            the role to save
     */
    public void saveChanges(RoleModelData toSave)
    {
        if (toSave != null)
        {
            PermsTreeStoreUtil.translateStore(tree, toSave.getPermissions(), toSave.getBans());
            toSave.save();
        }
        else if (role != null)
        {
            PermsTreeStoreUtil.translateStore(tree, role.getPermissions(), role.getBans());
            role.save();

            /* debug */
            // GWT.log(role.getPermissions().toString() + '\n' + role.getBans().toString(), null);
        }
    }

    /**
     * Expands the tree store at the specified node's level.
     * 
     * @param node
     *            the node to expand
     */
    public void expandNode(PermModelData node)
    {
        PermsTreeStoreUtil.expandNode(getStore(), node, getNodeChidrenIDs(), expansionFinishedEvent());
    }

    /**
     * Collapses the tree store at the specified node's level.
     * 
     * @param node
     *            the node to collapse
     */
    public void collapseNode(PermModelData node)
    {
        PermsTreeStoreUtil.collapseNode(getStore(), node);
    }

    /**
     * A custom sorter that will always give advantage to an 'ALL' type permission model data.
     * 
     * @author HD3D
     * 
     */
    static class PermSorter extends StoreSorter<PermModelData>
    {
        public int compare(Store<PermModelData> store, PermModelData m1, PermModelData m2, String property)
        {
            if (store instanceof TreeStore)
            {
                TreeStore<PermModelData> tree = (TreeStore<PermModelData>) store;
                if (m1.getType() == PermModelData.ALL)
                {
                    if (tree.getSortState().getSortDir() == Style.SortDir.DESC)
                    {
                        return 10000;
                    }
                    else
                    {
                        return -10000;
                    }
                }
                if (m2.getType() == PermModelData.ALL)
                {
                    if (tree.getSortState().getSortDir() == Style.SortDir.DESC)
                    {
                        return 10000;
                    }
                    else
                    {
                        return -10000;
                    }
                }
            }
            return super.compare(store, m1, m2, property);
        }
    }

    /* event types */

    /**
     * The event type that denotes a full undo request.
     * 
     * @return a dedicated event type
     */
    public abstract EventType fullUndoEvent();

    /**
     * The event type that denotes a re-do request.
     * 
     * @return a dedicated event type
     */
    public abstract EventType redoEvent();

    /**
     * The event type that denotes a save request.
     * 
     * @return a dedicated event type
     */
    public abstract EventType saveEvent();

    /**
     * The event type that denotes a undo request.
     * 
     * @return a dedicated event type
     */
    public abstract EventType undoEvent();

    /**
     * Defines the event type that denotes a click on the 'create' right column.
     * 
     * @return a dedicated event type
     */
    public abstract EventType canCreateClickedEvent();

    /**
     * Defines the event type that denotes a click on the 'read' right column.
     * 
     * @return a dedicated event type
     */
    public abstract EventType canReadClickedEvent();

    /**
     * Defines the event type that denotes a click on the 'delete' right column.
     * 
     * @return a dedicated event type
     */
    public abstract EventType canDeleteClickedEvent();

    /**
     * Defines the event type that denotes a click on the 'ALL' right column.
     * 
     * @return a dedicated event type
     */
    public abstract EventType canAllClickedEvent();

    /**
     * Defines the event type that denotes a click on the 'Update' right column.
     * 
     * @return a dedicated event type
     */
    public abstract EventType canUpdateClickedEvent();

    /**
     * Defines the event type that denotes an 'expand node' order.
     * 
     * @return a dedicated event type
     */
    public abstract EventType expandNodeEvent();

    /**
     * Defines the event type that carries an 'node expansion finished' information.
     * 
     * @return a dedicated event type
     */
    public abstract EventType expansionFinishedEvent();

    /**
     * Defines the event type that denotes a 'collapse node' order.
     * 
     * @return a dedicated event type
     */
    public abstract EventType collapseNodeEvent();

    /**
     * Defines the event type that denotes an order send a request of a node's children to the server.
     * 
     * @return a dedicated event type
     */
    public abstract EventType getNodeChidrenIDs();

    /**
     * Defines an event that orders the refreshment of the grid's view.
     * 
     * @return a dedicated event type
     */
    public abstract EventType refreshGridEvent();

}
