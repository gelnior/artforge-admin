package fr.hd3d.admin.ui.test.modeldata;

import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.Hd3dPagingJsonReaderMock;


public class RolePagingReaderMock extends Hd3dPagingJsonReaderMock<RoleModelData>
{

    public RolePagingReaderMock()
    {
        super(RoleModelData.getModelType());
    }

    public RoleModelData newModelInstance()
    {
        return new RoleModelData();
    }

}
