package fr.hd3d.admin.ui.client.view;

import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;

import fr.hd3d.admin.ui.client.model.AbstractRoleModel;
import fr.hd3d.admin.ui.client.rights.view.SubGridPanel;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;


/**
 * This panel is a factoring of the common features of a role grid containing panel. This one can propose and handle
 * creation, edition and deletion demands, as well as adding and removal if specified so.
 * 
 * @author HD3D
 */
public abstract class AbstractRolePanel extends SubGridPanel<RoleModelData, AbstractRoleModel> implements
        IAbstractRoleView
{

    protected Button createButton;
    protected Button deleteButton;
    protected Button editButton;

    // optional buttons
    /** whether an add button is required or not */
    protected boolean hasAdd;
    /** whether a remove button is required or not */
    protected boolean hasRemove;
    protected Button addButton;
    protected Button removeButton;

    // dialogs
    /** a dialog that handles role creation request */
    protected Dialog createLine = LabelDialog.getCreationDialog(AdminMainView.MESSAGES.CreateRoleWindow(), model
            .creationConfirmEvent());
    /** a dialog that handles role edition request */
    protected LabelDialog editLine = LabelDialog.getEditionDialog(AdminMainView.MESSAGES.EditRoleWindow(), model
            .editionConfirmEvent(), null);

    /**
     * The adequate constructor for an abstract role panel, where it is specified as parameters whether to build an add
     * and/or remove button.
     * 
     * @param model
     *            the abstract role model
     * @param hasAdd
     *            whether to build an add button
     * @param hasRemove
     *            whether to build a remove button
     */
    public AbstractRolePanel(AbstractRoleModel model, boolean hasAdd, boolean hasRemove)
    {
        super(model);
        this.hasAdd = hasAdd;
        this.hasRemove = hasRemove;
    }

    protected void setActionToolBar()
    {
        super.setActionToolBar();

        actionBar.add(new SeparatorToolItem());

        createButton = setupSubGridButton(null, AdminMainView.MESSAGES.CreateRole(), "create-icon", model
                .creationClickEvent());
        if (!PermissionUtil.hasCreateRights(model.getRoleClassName()))
        {
            createButton.disable();
        }
        actionBar.add(createButton);

        editButton = setupSubGridButton(null, AdminMainView.MESSAGES.EditRole(), "edit-icon", model.editionClickEvent());
        editButton.disable();
        actionBar.add(editButton);

        deleteButton = setupSubGridButton(null, AdminMainView.MESSAGES.DeleteRole(), "delete-icon", model
                .deleteClickEvent());
        deleteButton.disable();
        actionBar.add(deleteButton);

        actionBar.add(new SeparatorToolItem());

        if (hasAdd)
        {
            addButton = setupSubGridButton(null, AdminMainView.MESSAGES.AddRole(), "add-icon", model.addingClickEvent());
            addButton.disable();
            actionBar.add(addButton);
        }

        if (hasRemove)
        {
            removeButton = setupSubGridButton(null, AdminMainView.MESSAGES.RemoveRole(), "remove-icon", model
                    .removeClickEvent());
            removeButton.disable();
            actionBar.add(removeButton);
        }
    }

    protected String nameField()
    {
        return RoleModelData.NAME_FIELD;
    }

    public void onItemSelection(boolean canUpdate, boolean canDelete)
    {
        if (canUpdate)
        {
            editButton.enable();
        }
        else
        {
            editButton.disable();
        }
        if (canDelete)
        {
            deleteButton.enable();
        }
        else
        {
            deleteButton.disable();
        }
    }

    public void onFullDeselection()
    {
        editButton.disable();
        deleteButton.disable();
        if (hasRemove)
        {
            removeButton.disable();
        }
    }

    public void onCreateDemand()
    {
        createLine.setPosition(getAbsoluteLeft() + 30, auxGrid.getAbsoluteTop() + 60);
        createLine.show();
    }

    public void onEditDemand()
    {
        editLine.setDefaultText((String) model.selected().get(nameField()));
        editLine.setPosition(getAbsoluteLeft() + 30, auxGrid.getAbsoluteTop() + 60);
        editLine.show();
    }

}
