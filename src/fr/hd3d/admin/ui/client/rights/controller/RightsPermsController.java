package fr.hd3d.admin.ui.client.rights.controller;

import java.util.LinkedList;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.perms.controller.PermsController;
import fr.hd3d.admin.ui.client.perms.view.IPermsView;
import fr.hd3d.admin.ui.client.rights.model.RightsPermsModel;


/**
 * A controller capable of handling all the events related to the permission and bans panel.
 * 
 * @author HD3D
 * 
 */
public class RightsPermsController extends PermsController
{

    public RightsPermsController(RightsPermsModel model, IPermsView view)
    {
        super(model, view);
    }

    protected List<EventType> defineRoleDeselectionEvents()
    {
        List<EventType> events = new LinkedList<EventType>();
        events.add(AdminEvents.ALL_ROLES_CLICKED);
        events.add(AdminEvents.RESOURCEGROUP_SELECTED);
        events.add(AdminEvents.DELETE_ROLE_CLICKED);
        events.add(AdminEvents.REMOVE_ROLE_CLICKED);
        return events;
    }

    protected List<EventType> defineRoleModificationEvents()
    {
        List<EventType> events = new LinkedList<EventType>();
        events.add(AdminEvents.SERVER_ROLES_MODIFICATION);
        return events;
    }

    protected List<EventType> defineRoleSelectionEvents()
    {
        List<EventType> events = new LinkedList<EventType>();
        events.add(AdminEvents.ROLE_SELECTED);
        return events;
    }

}
