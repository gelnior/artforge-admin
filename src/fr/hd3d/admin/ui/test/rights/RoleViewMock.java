package fr.hd3d.admin.ui.test.rights;

import fr.hd3d.admin.ui.client.rights.model.RoleModel;
import fr.hd3d.admin.ui.client.rights.view.IRoleView;
import fr.hd3d.admin.ui.test.AbstractRoleViewMock;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;


public class RoleViewMock extends AbstractRoleViewMock implements IRoleView
{

    public RoleViewMock(RoleModel model)
    {
        super(model);
    }

    public String groupName;
    public int relToolOpened = 0;

    public void onGroupDeselection()
    {
        onFullDeselection();
        groupName = null;
    }

    public void onGroupSelection(String groupName, boolean canUpdate)
    {
        onFullDeselection();
        this.groupName = groupName;
    }

    public void openGroupRelationTool(ResourceGroupModelData group)
    {
        relToolOpened++;
    }

}
