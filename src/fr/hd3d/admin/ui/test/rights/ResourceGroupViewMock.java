package fr.hd3d.admin.ui.test.rights;

import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.rights.model.ResourceGroupModel;
import fr.hd3d.admin.ui.client.rights.view.IResourceGroupView;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;


public class ResourceGroupViewMock extends SubGridViewMock<ResourceGroupModelData> implements IResourceGroupView
{

    public ResourceGroupViewMock(ResourceGroupModel model)
    {
        super(model);
    }

    public boolean personBox = true;
    public boolean projectBox = true;

    public void resetComboBoxes(boolean person, boolean project)
    {
        personBox = !person;
        projectBox = !project;
    }

    public EventType allEventType()
    {
        return AdminEvents.ALL_GROUPS_CLICKED;
    }

    public void onFullDeselection()
    {}

    public EventType selectionEventType()
    {
        return AdminEvents.RESOURCEGROUP_SELECTED;
    }

    public void onItemSelection(boolean canUpdate, boolean canDelete)
    {}

}
