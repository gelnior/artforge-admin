package fr.hd3d.admin.ui.client.rights.model;

import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.perms.model.PermsModel;


/**
 * The permissions and bans management model for the 'Roles and Rights Management' application.
 * 
 * @author HD3D
 * 
 */
public class RightsPermsModel extends PermsModel
{

    public boolean isTemplate()
    {
        return false;
    }

    /* event types */

    public EventType fullUndoEvent()
    {
        return AdminEvents.PERM_LIST_FULL_UNDO;
    }

    public EventType redoEvent()
    {
        return AdminEvents.PERM_LIST_REDO;
    }

    public EventType saveEvent()
    {
        return AdminEvents.PERM_LIST_SAVE;
    }

    public EventType undoEvent()
    {
        return AdminEvents.PERM_LIST_UNDO;
    }

    public EventType canCreateClickedEvent()
    {
        return AdminEvents.CAN_CREATE_CLICKED;
    }

    public EventType canReadClickedEvent()
    {
        return AdminEvents.CAN_READ_CLICKED;
    }

    public EventType canDeleteClickedEvent()
    {
        return AdminEvents.CAN_DELETE_CLICKED;
    }

    public EventType canAllClickedEvent()
    {
        return AdminEvents.CAN_ALL_CLICKED;
    }

    public EventType canUpdateClickedEvent()
    {
        return AdminEvents.CAN_UPDATE_CLICKED;
    }

    public EventType expandNodeEvent()
    {
        return AdminEvents.EXPAND_NODE;
    }

    public EventType collapseNodeEvent()
    {
        return AdminEvents.REDUCE_NODE;
    }

    public EventType getNodeChidrenIDs()
    {
        return AdminEvents.GET_NODE_CHILDREN;
    }

    public EventType expansionFinishedEvent()
    {
        return AdminEvents.EXPANSION_FINISHED;
    }

    public EventType refreshGridEvent()
    {
        return AdminEvents.REFRESH_PERM_GRID;
    }

}
