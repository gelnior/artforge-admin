package fr.hd3d.admin.ui.client.rights.view;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.admin.ui.client.rights.view.RemoveButtonListener;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.common.ui.client.modeldata.NameModelData;


class RemoveButtonRenderer implements GridCellRenderer<NameModelData>
{
    EventType event;

    public RemoveButtonRenderer(EventType event)
    {
        this.event = event;
    }

    public Object render(NameModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<NameModelData> store, Grid<NameModelData> grid)
    {
        RemoveButtonListener listener = new RemoveButtonListener(store.getAt(rowIndex), event);
        Button remove = setupButton(null, AdminMainView.MESSAGES.RemoveRight(), "delete-icon", listener);
        remove.setWidth(20);

        return remove;
    }

    /**
     * adds a button with proper listener, icon, text...
     */
    protected Button setupButton(String text, String toolTip, String iconCSS, SelectionListener<ButtonEvent> listener)
    {
        Button result = new Button();
        if (text != null)
        {
            result.setText(text);
        }
        if (toolTip != null)
        {
            result.setToolTip(toolTip);
        }
        if (iconCSS != null)
        {
            result.setIconStyle(iconCSS);
        }
        if (listener != null)
        {
            result.addSelectionListener(listener);
        }
        return result;
    }
}
