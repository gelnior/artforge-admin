package fr.hd3d.admin.ui.client.rights.model;

import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.model.modeldata.SingleResourceGroupReader;
import fr.hd3d.common.ui.client.modeldata.reader.IPagingReader;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;


/**
 * The <code>SubGridModel</code> specification for handling resource group model data and its constraints.
 * 
 * @author HD3D
 * 
 */
public class ResourceGroupModel extends SubGridModel<ResourceGroupModelData>
{

    protected IPagingReader<ResourceGroupModelData> getPagingReader()
    {
        return SingleResourceGroupReader.getPagingReader();
    }

    public void setPath(String path)
    {
        super.setPath(path);
    }

    public void initProxyData()
    {
        super.initProxyData();
    }

    protected IReader<ResourceGroupModelData> getSimpleReader()
    {
        return SingleResourceGroupReader.getReader();
    }

    public EventType allEventType()
    {
        return AdminEvents.ALL_GROUPS_CLICKED;
    }

    public EventType selectionEventType()
    {
        return AdminEvents.RESOURCEGROUP_SELECTED;
    }

}
