package fr.hd3d.admin.ui.test.users;

import fr.hd3d.admin.ui.client.controller.SubController;
import fr.hd3d.admin.ui.client.users.view.IAccountView;
import fr.hd3d.admin.ui.client.view.ISubView;


public class UsersViewMock implements ISubView
{

    private IAccountView a;

    public boolean init = false;

    public UsersViewMock(AccountViewMock a)
    {
        this.a = a;
    }

    public String getHeading()
    {
        return "heading";
    }

    public SubController init()
    {
        // no need for test
        return null;
    }

    public void initWidgets()
    {
        a.setup();

        init = true;
    }

}
