package fr.hd3d.admin.ui.client.mountpoint.model;

import fr.hd3d.common.ui.client.modeldata.asset.MountPointModelData;
import fr.hd3d.common.ui.client.modeldata.reader.MountPointReader;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


public class MountPointModel
{

    private ServiceStore<MountPointModelData> mountPointStore = new ServiceStore<MountPointModelData>(
            new MountPointReader());

    public ServiceStore<MountPointModelData> getMountPointStore()
    {
        return mountPointStore;
    }

    public MountPointModel()
    {

    }

    public void reload()
    {
        mountPointStore.reload();
    }

}
