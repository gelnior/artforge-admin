package fr.hd3d.admin.ui.client.perms.view;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;

import fr.hd3d.admin.ui.client.perms.model.PermModelData;
import fr.hd3d.admin.ui.client.perms.model.PermsModel;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.admin.ui.client.view.QuitAndSaveDialog;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;


/**
 * The permission tree grid containing panel.
 * 
 * @author HD3D
 * 
 */
public class PermsPanel extends ContentPanel implements IPermsView
{

    /** the permission model data tree grid */
    protected PermGrid rightsGrid;
    /** the permission model */
    protected PermsModel model;

    protected ToolBar actions = new ToolBar();
    protected Button save;
    protected Button undo;
    protected Button redo;

    /** a quit or save dialog shown when unsaved actions might be lost */
    protected QuitAndSaveDialog roleQNS;

    /**
     * Creates a panel for the permission model's tree store display.
     * 
     * @param model
     *            the permission model
     */
    public PermsPanel(PermsModel model)
    {
        this.model = model;
    }

    /**
     * Initializes the widgets and more specifically the permission tree grid.
     */
    public void setup()
    {
        setStyle();

        setActionToolBars();
        setTopComponent(actions);

        setupRightsGrid();

        setupQNS();

        // FIXME initialize manual filter
        ManualFilterDialog.getInstance().show();
        ManualFilterDialog.getInstance().hide();
    }

    /**
     * Sets borders, headers, layout, etc.
     */
    protected void setStyle()
    {
        setHeaderVisible(true);
        resetHeading();
        setBodyBorder(true);
        setBorders(false);
        setLayout(new FitLayout());
    }

    /**
     * Sets the quit and save dialog.
     */
    protected void setupQNS()
    {
        roleQNS = new RoleQuitNSave();
    }

    /**
     * Builds up the action tool bar.
     */
    protected void setActionToolBars()
    {
        save = setupButton(null, AdminMainView.MESSAGES.SaveList(), "save-icon", model.saveEvent());
        save.disable();
        actions.add(save);

        undo = setupButton(null, AdminMainView.MESSAGES.UndoList(), "undo-icon", model.undoEvent());
        undo.disable();
        actions.add(undo);

        redo = setupButton(null, AdminMainView.MESSAGES.RedoList(), "redo-icon", model.redoEvent());
        redo.disable();
        actions.add(redo);

        /* undo comment to make caption appear */
        // actions.add(new FillToolItem());
        // actions.add(new LabelToolItem(AdminMainView.CONSTANTS.Key()));
        // actions.add(new SeparatorToolItem());
        //
        // // permitted label
        // LabelToolItem lab1 = new LabelToolItem(AdminMainView.CONSTANTS.Permitted());
        // lab1.addStyleName("permitted-check");
        // actions.add(lab1);
        //
        // // deduced permission label
        // LabelToolItem lab2 = new LabelToolItem(AdminMainView.CONSTANTS.Permitted());
        // lab2.addStyleName("deduced-perm-check");
        // actions.add(lab2);
        //
        // // banned label
        // LabelToolItem lab3 = new LabelToolItem(AdminMainView.CONSTANTS.Banned());
        // lab3.addStyleName("banned-check");
        // actions.add(lab3);
        //
        // // deduced ban label
        // LabelToolItem lab4 = new LabelToolItem(AdminMainView.CONSTANTS.Banned());
        // lab4.addStyleName("deduced-ban-check");
        // actions.add(lab4);
        actions.setStyleAttribute("padding", "5px");
    }

    public void onRoleSelection()
    {
        if (model.getRole() == null)
        {
            rightsGrid.hide();
        }
        else
        {
            showGrid();
            for (PermModelData root : model.getStore().getRootItems())
            {
                rightsGrid.setExpanded(root, true, true);
            }
        }
        resetHeading();
    }

    /**
     * Sets proper heading depending on the selected role.
     */
    protected void resetHeading()
    {
        setHeading(AdminMainView.CONSTANTS.RoleProp() + " - " + model.getRoleName());
    }

    public void showGrid()
    {
        if (model.getRole() != null)
        {
            rightsGrid.show();
            rightsGrid.getView().refresh(true);
        }
        else
        {
            rightsGrid.hide();
        }
    }

    public void setNodeExpanded(PermModelData node, boolean expanded)
    {
        rightsGrid.setExpanded(node, expanded, true);
    }

    /**
     * Builds up the permission tree grid.
     */
    protected void setupRightsGrid()
    {
        rightsGrid = new PermGrid(model);
        rightsGrid.hide();
        add(rightsGrid, new FitData());
    }

    /**
     * Adds a button with proper listener, icon, text...
     * 
     * @return a button with all the specified characteristics
     */
    protected Button setupButton(String text, String toolTip, String iconCSS, EventType eventType)
    {
        Button result = new Button();
        if (text != null)
        {
            result.setText(text);
        }
        if (toolTip != null)
        {
            result.setToolTip(toolTip);
        }
        if (iconCSS != null)
        {
            result.setIconStyle(iconCSS);
        }
        if (eventType != null)
        {
            result.addSelectionListener(new ButtonClickListener(eventType));
        }
        return result;
    }

    public void refreshGrid()
    {
        rightsGrid.getView().refresh(false);
    }

    public void onUndoableAction()
    {
        undo.enable();
        save.enable();
    }

    public void onUndo()
    {
        redo.enable();
    }

    public void onEmptyRedoStack()
    {
        redo.disable();
    }

    public void onEmptyUndoStack(boolean saveAlso)
    {
        undo.disable();
        if (saveAlso)
        {
            save.disable();
        }
    }

    public void showQuitAndSaveDialog(AppEvent delayedEvent)
    {
        roleQNS.setDelayedEvent(delayedEvent);
        roleQNS.setItemToSave(model.getRole());
        roleQNS.setPosition(getAbsoluteLeft() + 50, getAbsoluteTop() + 150);
        roleQNS.show();
    }

    /**
     * A specifically designed 'quit or save' dialog, shown when unsaved changes might be lost.
     * 
     * @author HD3D
     * 
     */
    class RoleQuitNSave extends QuitAndSaveDialog
    {
        public RoleQuitNSave()
        {
            super(AdminMainView.MESSAGES.RolePropertiesChange(), null, model.saveEvent(), model.fullUndoEvent(), null);
            autoSave = false;
        }
    }

    public String getAllChildrenString()
    {
        return AdminMainView.CONSTANTS.AllButton();
    }

    public String getAnyVersionString()
    {
        return AdminMainView.CONSTANTS.PermTypeAnyVersion();
    }

}
