package fr.hd3d.admin.ui.client.view;

import fr.hd3d.admin.ui.client.rights.view.ISubGridView;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;


/**
 * This specification of the <code>ISubGridView</code> adds graphic creation and edition request handling.
 * 
 * @author HD3D
 * 
 */
public interface IAbstractRoleView extends ISubGridView<RoleModelData>
{

    /**
     * Displays the appropriate dialog upon 'create role' request.
     */
    public void onCreateDemand();

    /**
     * Displays the appropriate dialog upon 'edit role name' request.
     */
    public void onEditDemand();

}
