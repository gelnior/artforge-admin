package fr.hd3d.admin.ui.client.rights.controller;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.controller.AbstractRoleController;
import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.rights.model.RoleModel;
import fr.hd3d.admin.ui.client.rights.view.IRoleView;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;


/**
 * The role controller specification needed to handle resource group selection.
 * 
 * @author HD3D
 * 
 */
public class RoleController extends AbstractRoleController<RoleModel, IRoleView>
{

    public RoleController(RoleModel model, IRoleView view)
    {
        super(model, view);
    }

    protected void doHandleEvent(AppEvent event)
    {
        super.doHandleEvent(event);
        EventType type = event.getType();

        if (type == AdminEvents.RESOURCEGROUP_SELECTED)
        {
            fullyLoaded = false;
            clearConstraints();
            model.setSelectedGroup(event.<ResourceGroupModelData> getData());
            setupConstraints();
            deselectAll();
            refreshStore();
        }
        else if (type == AdminEvents.SERVER_SELECTED_GROUP_MODIFICATION)
        {
            model.getSelectedGroup().refresh(AdminEvents.SERVER_ROLES_MODIFICATION);
        }
    }

    protected String path()
    {
        return ServicesPath.getPath(RoleModelData.SIMPLE_CLASS_NAME);
    }

    protected void onConstraintsClearance()
    {
        model.setSelectedGroup(null);
        view.onGroupDeselection();
    }

    protected void setupConstraints()
    {
        ResourceGroupModelData selectedGroup = model.getSelectedGroup();
        if (selectedGroup != null)
        {
            List<Long> roleIds = selectedGroup.getRoleIds();
            if (roleIds != null && !roleIds.isEmpty())
            {
                Constraint constraint = new Constraint(EConstraintOperator.in, Hd3dModelData.ID_FIELD, roleIds, null);
                constraints.add(constraint);
            }
            else
            {
                view.hideGrid();
            }
            view.onGroupSelection(selectedGroup.getName(), selectedGroup.getUserCanUpdate());
        }
    }

    protected void registerEvents()
    {
        super.registerEvents();
        registerEventTypes(AdminEvents.SERVER_SELECTED_GROUP_MODIFICATION);
        registerEventTypes(AdminEvents.RESOURCEGROUP_SELECTED);
    }

    protected void onAddingDemand()
    {
        // selected group is not null a priori
        view.openGroupRelationTool(model.getSelectedGroup());
    }

    protected void onRemovalDemand()
    {
        model.removeLine();
    }

}
