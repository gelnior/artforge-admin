package fr.hd3d.admin.ui.client.perms.controller;

import java.util.LinkedList;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.admin.ui.client.config.AdminConfig;
import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.perms.model.PermModelData;
import fr.hd3d.admin.ui.client.perms.model.PermsModel;
import fr.hd3d.admin.ui.client.perms.model.ShieldedField;
import fr.hd3d.admin.ui.client.perms.view.IPermsView;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.userrights.UserRightsResolver;


/**
 * The permission controller handles all the permission related events, such as role selection or change in a role's
 * rights, permission tree expansion or collapse plus undo, re-do and save requests.
 * 
 * @author HD3D
 * 
 */
public abstract class PermsController extends Controller
{

    /** the permission model */
    protected PermsModel model;
    /** the permission view */
    protected IPermsView view;

    /** the command manager that will order all changes in a role's permissions */
    protected final PermsCommandManager manager;

    /** whether there are changes to be saved */
    protected boolean unsavedChanges = false;
    /** whether the service URLs tree has been initialized */
    protected boolean services_init = false;
    /** whether the user can update currently selected role */
    protected boolean userCanUpdate = false;

    private List<EventType> roleSelectionEvents;
    private List<EventType> roleDeselectionEvents;
    private List<EventType> roleModificationEvents;

    /**
     * Creates a controller for handling permission related events.
     * 
     * @param model
     *            the permission model
     * @param view
     *            the permission view
     */
    public PermsController(PermsModel model, IPermsView view)
    {
        this.model = model;
        this.view = view;

        manager = new PermsCommandManager(model);

        buildUpRoleEvents();
        registerEvents();
    }

    public void handleEvent(AppEvent event)
    {
        this.forwardToChild(event);
        EventType type = event.getType();

        if (type == CommonEvents.CONFIG_INITIALIZED)
        {
            onConfigInitialized(event);
        }
        else if (type == AdminEvents.SERVICE_URLS_INITIALIZED)
        {
            onServiceURLsInitialized();
        }
        else if (type == CommonEvents.PERMISSION_INITIALIZED)
        {
            this.onPermissionInitialized(event);
        }
        else if (services_init && roleSelectionEvents.contains(type))
        {
            if (unsavedChanges)
            {
                onPrematuredSelectionChange(event);
            }
            else
            {
                RoleModelData selectedRole = event.<RoleModelData> getData();
                model.setRole(selectedRole);
                onRoleSelection();
            }
        }
        else if (services_init && roleDeselectionEvents.contains(type))
        {
            if (unsavedChanges)
            {
                onPrematuredSelectionChange(event);
            }
            else
            {
                model.setRole(null);
                onRoleSelection();
            }
        }
        else if (services_init && roleModificationEvents.contains(type))
        {
            if (unsavedChanges)
            {
                onPrematuredSelectionChange(event);
            }
            else
            {
                onRoleSelection();
            }
        }
        else if (type == model.saveEvent())
        {
            RoleModelData roleToSave = event.<RoleModelData> getData();
            onSave(roleToSave);
        }
        else if (type == model.fullUndoEvent())
        {
            onFullUndo();
        }
        else if (type == model.undoEvent())
        {
            onUndo();
        }
        else if (type == model.redoEvent())
        {
            onRedo();
        }
        else if (type == model.canCreateClickedEvent())
        {
            PermModelData perm = event.<PermModelData> getData();
            if (userCanUpdate && UserRightsResolver.userHasRight(perm.getPath() + ":" + AdminConfig.CREATE))
            {
                onCanCreate(perm);
            }
            else
            {
                Info.display(AdminMainView.CONSTANTS.AuthzNeeded(), AdminMainView.MESSAGES.NoRightToClickOnPerm(),
                        AdminConfig.CREATE, perm.getPath());
            }
        }
        else if (type == model.canAllClickedEvent())
        {
            PermModelData perm = event.<PermModelData> getData();
            if (userCanUpdate && UserRightsResolver.userHasRight(perm.getPath() + ":*"))
            {
                onCanAll(perm);
            }
            else
            {
                Info.display(AdminMainView.CONSTANTS.AuthzNeeded(), AdminMainView.MESSAGES.NoRightToClickOnPerm(),
                        AdminMainView.CONSTANTS.AllButton(), perm.getPath());
            }
        }
        else if (type == model.canReadClickedEvent())
        {
            PermModelData perm = event.<PermModelData> getData();
            if (userCanUpdate && UserRightsResolver.userHasRight(perm.getPath() + ":" + AdminConfig.READ))
            {
                onCanRead(perm);
            }
            else
            {
                Info.display(AdminMainView.CONSTANTS.AuthzNeeded(), AdminMainView.MESSAGES.NoRightToClickOnPerm(),
                        AdminConfig.READ, perm.getPath());
            }
        }
        else if (type == model.canUpdateClickedEvent())
        {
            PermModelData perm = event.<PermModelData> getData();
            if (userCanUpdate && UserRightsResolver.userHasRight(perm.getPath() + ":" + AdminConfig.UPDATE))
            {
                onCanUpdate(perm);
            }
            else
            {
                Info.display(AdminMainView.CONSTANTS.AuthzNeeded(), AdminMainView.MESSAGES.NoRightToClickOnPerm(),
                        AdminConfig.UPDATE, perm.getPath());
            }
        }
        else if (type == model.canDeleteClickedEvent())
        {
            PermModelData perm = event.<PermModelData> getData();
            if (userCanUpdate && UserRightsResolver.userHasRight(perm.getPath() + ":" + AdminConfig.DELETE))
            {
                onCanDelete(perm);
            }
            else
            {
                Info.display(AdminMainView.CONSTANTS.AuthzNeeded(), AdminMainView.MESSAGES.NoRightToClickOnPerm(),
                        AdminConfig.DELETE, perm.getPath());
            }
        }
        else if (type == model.expandNodeEvent())
        {
            PermModelData perm = event.<PermModelData> getData();
            onExpandNode(perm);
        }
        else if (type == model.expansionFinishedEvent())
        {
            PermModelData perm = event.<PermModelData> getData();
            onNodeExpansionFinished(perm);
        }
        else if (type == model.collapseNodeEvent())
        {
            PermModelData perm = event.<PermModelData> getData();
            onCollapseNode(perm);
        }
        else if (type == model.getNodeChidrenIDs())
        {
            PermModelData perm = event.<PermModelData> getData();
            onNodeChildrenRequest(perm);
        }
        else if (type == model.refreshGridEvent())
        {
            refreshPermGrid();
        }
    }

    /**
     * Orders the model to get in touch with the service URL tree.
     * 
     * @param event
     *            the incoming event
     */
    protected void onConfigInitialized(AppEvent event)
    {
        model.initServiceURLs();
    }

    /**
     * Initializes the view upon permission initialization.
     * 
     * @param event
     *            the incoming event
     */
    protected void onPermissionInitialized(AppEvent event)
    {
        model.refreshStore();
        view.setup();
    }

    /**
     * Interrupts the current event and orders the view to display a 'quit or save' dialog.
     * 
     * @param delayedEvent
     *            the interrupted event
     */
    protected void onPrematuredSelectionChange(AppEvent delayedEvent)
    {
        view.showQuitAndSaveDialog(delayedEvent);
    }

    /**
     * Performs all the display changes upon role selection.
     */
    protected void onRoleSelection()
    {
        clearCommandManager();

        if (model.getRole() != null)
        {
            userCanUpdate = model.getRole().getUserCanUpdate();
        }
        model.refreshStore();
        view.onRoleSelection();
    }

    /**
     * Orders the model to save the specified role. If it is null, orders the model to save the selected role.
     * 
     * @param role
     *            the role to save
     */
    protected void onSave(RoleModelData role)
    {
        unsavedChanges = false;
        clearCommandManager();
        model.saveChanges(role);
    }

    /**
     * Undoes all the changes that have been done to the selected role.
     */
    protected void onFullUndo()
    {
        unsavedChanges = false;
        while (!manager.getUndoCommandStack().isEmpty())
        {
            manager.undo();
        }
        refreshPermGrid();
        clearCommandManager();
    }

    /**
     * Undoes last permission change on the selected role.
     */
    protected void onUndo()
    {
        manager.undo();
        refreshPermGrid();
        if (manager.getUndoCommandStack().isEmpty())
        {
            view.onEmptyUndoStack(!unsavedChanges);
            unsavedChanges = false; // FIXME slight bug here
        }
        view.onUndo();
        view.showGrid();
    }

    /**
     * Re-does last undone permission change on the selected role.
     */
    protected void onRedo()
    {
        manager.redo();
        view.refreshGrid();
        if (manager.getRedoCommandStack().isEmpty())
        {
            view.onEmptyRedoStack();
        }
        afterUndoableAction();
    }

    /**
     * Handles a permission change request on the 'delete' column, at the specified node level.
     * 
     * @param perm
     *            the node on which the right change has been requested
     */
    private void onCanDelete(PermModelData perm)
    {
        ShieldedField field = perm.getCanDelete();
        if (!field.hasBanShield() && field.isEnabled())
        {
            beforeUndoableAction();
            manager.executeCanDeleteClick(perm);
            refreshPermGrid();
            afterUndoableAction();
        }
    }

    /**
     * Handles a permission change request on the 'update' column, at the specified node level.
     * 
     * @param perm
     *            the node on which the right change has been requested
     */
    private void onCanUpdate(PermModelData perm)
    {
        ShieldedField field = perm.getCanUpdate();
        if (!field.hasBanShield() && field.isEnabled())
        {
            beforeUndoableAction();
            manager.executeCanUpdateClick(perm);
            refreshPermGrid();
            afterUndoableAction();
        }
    }

    /**
     * Handles a permission change request on the 'read' column, at the specified node level.
     * 
     * @param perm
     *            the node on which the right change has been requested
     */
    private void onCanRead(PermModelData perm)
    {
        ShieldedField field = perm.getCanRead();
        if (!field.hasBanShield() && field.isEnabled())
        {
            beforeUndoableAction();
            manager.executeCanReadClick(perm);
            refreshPermGrid();
            afterUndoableAction();
        }
    }

    /**
     * Handles a permission change request on the 'ALL' column, at the specified node level.
     * 
     * @param perm
     *            the node on which the right change has been requested
     */
    private void onCanAll(PermModelData perm)
    {
        ShieldedField field = perm.getCanAll();
        if (!field.hasBanShield() && field.isEnabled())
        {
            beforeUndoableAction();
            manager.executeCanAllClick(perm);
            refreshPermGrid();
            afterUndoableAction();
        }
    }

    /**
     * Handles a permission change request on the 'create' column, at the specified node level.
     * 
     * @param perm
     *            the node on which the right change has been requested
     */
    private void onCanCreate(PermModelData perm)
    {
        ShieldedField field = perm.getCanCreate();
        if (!field.hasBanShield() && field.isEnabled())
        {
            beforeUndoableAction();
            manager.executeCanCreateClick(perm);
            refreshPermGrid();
            afterUndoableAction();
        }
    }

    /**
     * Orders the tree grid to expand the specified node. Also clears all undo-able commands, disabling the possibility
     * to undo the last permission changes on selected role.
     * 
     * @param perm
     *            the node to expand
     */
    private void onExpandNode(PermModelData perm)
    {
        clearCommandManager(); // FIXME way to radical

        view.setNodeExpanded(perm, false);
        model.expandNode(perm);
    }

    /**
     * Refreshes the grid content after having fully the specified node fully expanded.
     * 
     * @param perm
     *            the expanded node
     */
    private void onNodeExpansionFinished(PermModelData perm)
    {
        view.setNodeExpanded(perm, true);
        refreshPermGrid();
    }

    /**
     * Refreshes the grid content after a node expansion.
     * 
     * @param perm
     *            the expanded node
     */
    private void refreshPermGrid()
    {
        view.refreshGrid();
    }

    /**
     * Orders the tree grid to collapse the specified node. Also clears all undo-able commands, disabling the
     * possibility to undo the last permission changes on selected role.
     * 
     * @param perm
     *            the node to collapse
     */
    private void onCollapseNode(PermModelData perm)
    {
        clearCommandManager(); // FIXME way to radical

        model.collapseNode(perm);
        refreshPermGrid();
    }

    /**
     * After a node has been expanded, the following method orders the model to send a request to the server in order to
     * retrieve all the specified node's children IDs.
     * 
     * @param perm
     *            the expanded node
     */
    private void onNodeChildrenRequest(PermModelData perm)
    {
        model.requestData(perm, false);
        refreshPermGrid();
    }

    /**
     * Registers all the application events this controller can handle.
     */
    protected void registerEvents()
    {
        for (EventType type : roleSelectionEvents)
        {
            registerEventTypes(type);
        }
        for (EventType type : roleDeselectionEvents)
        {
            registerEventTypes(type);
        }
        for (EventType type : roleModificationEvents)
        {
            registerEventTypes(type);
        }
        registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
        registerEventTypes(CommonEvents.CONFIG_INITIALIZED);
        registerEventTypes(AdminEvents.SERVICE_URLS_INITIALIZED);
        registerEventTypes(model.canAllClickedEvent());
        registerEventTypes(model.canCreateClickedEvent());
        registerEventTypes(model.canReadClickedEvent());
        registerEventTypes(model.canUpdateClickedEvent());
        registerEventTypes(model.canDeleteClickedEvent());
        registerEventTypes(model.collapseNodeEvent());
        registerEventTypes(model.expandNodeEvent());
        registerEventTypes(model.getNodeChidrenIDs());
        registerEventTypes(model.saveEvent());
        registerEventTypes(model.undoEvent());
        registerEventTypes(model.redoEvent());
        registerEventTypes(model.fullUndoEvent());
        registerEventTypes(model.expansionFinishedEvent());
        registerEventTypes(model.refreshGridEvent());
    }

    /**
     * Initializes all the role related events.
     */
    protected void buildUpRoleEvents()
    {
        roleSelectionEvents = defineRoleSelectionEvents();
        if (roleSelectionEvents == null)
        {
            roleSelectionEvents = new LinkedList<EventType>();
        }
        roleDeselectionEvents = defineRoleDeselectionEvents();
        if (roleDeselectionEvents == null)
        {
            roleDeselectionEvents = new LinkedList<EventType>();
        }
        roleModificationEvents = defineRoleModificationEvents();
        if (roleModificationEvents == null)
        {
            roleModificationEvents = new LinkedList<EventType>();
        }
    }

    /**
     * Defines the events that imply a role selection.
     * 
     * @return the list of the relevant event types
     */
    protected abstract List<EventType> defineRoleSelectionEvents();

    /**
     * Defines the events that imply a role selection clearance (deletion, etc.).
     * 
     * @return the list of the relevant event types
     */
    protected abstract List<EventType> defineRoleDeselectionEvents();

    /**
     * Defines the events that imply a role modification on the server.
     * 
     * @return the list of the relevant event types
     */
    protected abstract List<EventType> defineRoleModificationEvents();

    private void onServiceURLsInitialized()
    {
        services_init = true;

        // testInit();
    }

    private void beforeUndoableAction()
    {
        if (!manager.getRedoCommandStack().isEmpty())
        {
            manager.getRedoCommandStack().clear();
            view.onEmptyRedoStack();
        }
    }

    private void afterUndoableAction()
    {
        view.onUndoableAction();
        view.showGrid();
        unsavedChanges = true;
    }

    private void clearCommandManager()
    {
        manager.clearStacks();
        view.onEmptyRedoStack();
        view.onEmptyUndoStack(!unsavedChanges);
    }

    /* debug */
    // private void testInit()
    // {
    // RoleModelData role = new RoleModelData();
    // role.setName("Role_test");
    // AppEvent select = new AppEvent(AdminEvents.ROLE_SELECTED);
    // role.getPermissions().add("v1:projects:14:update,delete");
    // role.getPermissions().add("v1:projects:12:*:create");
    // role.getPermissions().add("v1:projects:*:read");
    // role.getPermissions().add("*:persons:2451:*");
    // role.getPermissions().add("*:persons:*:read");
    // role.getBans().add("v1:projects:12:categories:create");
    // role.getBans().add("*:persons:2451:*:create,update");
    // select.setData(role);
    // EventDispatcher.forwardEvent(select);
    // }
}
