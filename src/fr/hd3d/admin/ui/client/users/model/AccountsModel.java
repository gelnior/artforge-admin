package fr.hd3d.admin.ui.client.users.model;

import fr.hd3d.admin.ui.client.config.AdminConfig;
import fr.hd3d.admin.ui.client.factoring.simplegrid.Hd3dSimpleGridModel;
import fr.hd3d.admin.ui.client.model.callback.AccountEventCallBack;
import fr.hd3d.admin.ui.client.model.modeldata.SinglePersonReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;


/**
 * The model that handles all user account global operations and persons data loading.
 * 
 * @author HD3D
 * 
 */
public class AccountsModel extends Hd3dSimpleGridModel<PersonModelData>
{

    /** the selected person */
    private PersonModelData selected;

    public AccountsModel()
    {
        super();
    }

    /**
     * Initializes the proxy and loads data in store.
     */
    public void initModel()
    {
        setPath(ServicesPath.getPath(PersonModelData.SIMPLE_CLASS_NAME));

        initWithPaging(SinglePersonReader.getReader());
    }

    /**
     * Sets the selected person model data.
     * 
     * @param person
     *            the person to select
     */
    public void setSelected(PersonModelData person)
    {
        selected = person;
    }

    /**
     * Gets the selected person model data.
     * 
     * @return the selected person
     */
    public PersonModelData getSelected()
    {
        return selected;
    }

    /**
     * Sends a delete request to the request so that it deletes the account of the selected person.
     */
    public void deleteAccount()
    {
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.deleteRequest(getAccountPath(), new AccountEventCallBack());
    }

    /**
     * Calculates the path to the selected user's account resource from the selected person's login.
     * 
     * @return the path of the selected user's account
     */
    private String getAccountPath()
    {
        if (selected != null)
        {
            return AdminConfig.USERACCOUNTS_PATH + selected.getLogin();
        }
        return null;
    }

}
