package fr.hd3d.admin.ui.test.rights;

import fr.hd3d.admin.ui.client.rights.model.SubGridModel;
import fr.hd3d.admin.ui.client.rights.view.ISubGridView;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public abstract class SubGridViewMock<D extends Hd3dModelData> implements ISubGridView<D>
{

    public boolean gridVisible = true;
    public boolean allEnabled = true;
    public boolean init = false;

    protected SubGridModel<D> model;

    public SubGridViewMock(SubGridModel<D> model)
    {
        this.model = model;
    }

    public void hideGrid()
    {
        gridVisible = false;
    }

    public void setup()
    {
        init = true;
    }

    public void showGrid()
    {
        gridVisible = true;
    }

    public void deselectAll(boolean fullLoaded)
    {
        if (fullLoaded)
        {
            allEnabled = false;
        }
        else
        {
            allEnabled = true;
        }
    }

}
