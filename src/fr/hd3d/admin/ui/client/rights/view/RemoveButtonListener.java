package fr.hd3d.admin.ui.client.rights.view;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.NameModelData;


class RemoveButtonListener extends SelectionListener<ButtonEvent>
{
    private NameModelData right;
    private EventType event;

    public RemoveButtonListener(NameModelData right, EventType event)
    {
        this.right = right;
        this.event = event;
    }

    public void componentSelected(ButtonEvent ce)
    {
        AppEvent remove = new AppEvent(event);
        remove.setData(right);
        EventDispatcher.forwardEvent(remove);
    }

    private EventType removeEvent()
    {
        // if (bans)
        // {
        // return AdminEvents.REMOVE_BAN;
        // }
        return AdminEvents.REMOVE_PERMISSION;
    }
}
