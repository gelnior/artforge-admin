package fr.hd3d.admin.ui.client.factoring.simplegrid;

import java.util.ArrayList;
import java.util.LinkedList;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dListJsonReader;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dPagingJsonReader;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;


/**
 * A simple grid for the display of non-modifiable informations on a <code>Hd3dModelData</code> store. It supports
 * dynamic change of column config.
 * 
 * @author HD3D
 * 
 * @param <D>
 *            the model data type
 */
public class Hd3dSimpleGrid<D extends Hd3dModelData> extends Grid<D>
{

    protected Hd3dSimpleGridModel<D> model;

    protected LinkedList<ColumnConfig> columns = new LinkedList<ColumnConfig>();
    protected boolean showId = true;
    protected String autoExpandColumn = "";

    private boolean colHaveChanged = false;
    private boolean idShown = false;

    /**
     * Default constructor with no store nor columns
     */
    public Hd3dSimpleGrid()
    {
        super(null, new ColumnModel(new ArrayList<ColumnConfig>()));
    }

    /**
     * Initializes all the grid components, which must be done after all columns have been added.
     * 
     * @param model
     *            the fully initialized model from which the data is loaded
     */
    public void setup(Hd3dSimpleGridModel<D> model)
    {
        this.model = model;

        resetColumns();
    }

    /**
     * Initializes all the grid components. The model will automatically be set to a default
     * <code>Hd3dSimpleGridModel</code>, which will be initialized from the given reader and resource path.
     * 
     * @param reader
     *            a paging or list reader
     * @param path
     *            the path to the resource
     */
    public void autoSetup(String path, IReader<D> reader)
    {
        model = new Hd3dSimpleGridModel<D>();
        model.setPath(path);
        if (reader instanceof Hd3dPagingJsonReader<?>)
        {
            model.initWithPaging((Hd3dPagingJsonReader<D>) reader);
        }
        else
        {
            model.initWithoutPaging((Hd3dListJsonReader<D>) reader);
        }

        resetColumns();
    }

    /**
     * Sets the grid's columns, adding the Id column if asked to.
     * 
     * @return the column model
     */
    protected ColumnModel setupColumns()
    {
        if (showId)
        {
            if (!idShown)
            {
                addColumn(Hd3dModelData.ID_FIELD, "Id", 60, false);
                ColumnConfig id = columns.removeLast();
                columns.addFirst(id);
                idShown = true;
            }
        }
        else
        {
            if (idShown)
            {
                removeColumn(Hd3dModelData.ID_FIELD);
                idShown = false;
            }
        }
        colHaveChanged = false;
        return new ColumnModel(columns);
    }

    /**
     * Resets the columns displayed after a column configuration change.
     */
    public void resetColumns()
    {
        if (colHaveChanged)
        {
            ListStore<D> lStore = loadStore();
            ColumnModel cMod = setupColumns();
            reconfigure(lStore, cMod);
            setStyle();
        }
        else
        {
            reconfigure(getStore(), getColumnModel());
            setStyle();
        }
    }

    /**
     * Sets whether the id column is shown or not
     * 
     * @param showId
     *            whether the id column must be shown or not
     */
    public void setShowId(boolean showId)
    {
        this.showId = showId;
    }

    /**
     * Adds a data containing column to the grid. The name of the field to be displayed must be the exact name of the
     * corresponding Hd3dModelData field name. Columns will appear in accordance with the order in which they have been
     * set.
     * 
     * @param name
     *            the model property to be displayed in the column
     * @param header
     *            the header of the column
     * @param width
     *            the width of the column
     * @param resizable
     *            whether the column can be resized or not
     */
    public void addColumn(String name, String header, int width, boolean resizable)
    {
        ColumnConfig column = new ColumnConfig();
        column.setId(name);
        column.setHeader(header);
        column.setWidth(width);
        column.setResizable(resizable);
        autoExpandColumn = name;
        columns.addLast(column);
        colHaveChanged = true;
    }

    /**
     * Removes the column linked with the specified field name. In order to have the grid take this change into account,
     * you must ensure that {@link #resetColumns()} is called afterwards.
     * 
     * @param name
     */
    public void removeColumn(String name)
    {
        if (name != null)
        {
            int toRemove = -1;
            int index = 0;
            for (ColumnConfig col : columns)
            {
                if (name.equals(col.getId()))
                {
                    toRemove = index;
                    break;
                }
                index++;
            }
            if (toRemove > 0)
            {
                columns.remove(toRemove);
                colHaveChanged = true;
            }
        }
    }

    /**
     * Set all the style parameters such as borders, load mask etc.
     */
    protected void setStyle()
    {
        setAutoExpandColumn(autoExpandColumn);
        setAutoExpandMax(1000);
        getView().setForceFit(true);
        setLoadMask(true);
        setBorders(true);
    }

    /**
     * The model's store access.
     * 
     * @return the model store
     */
    protected ListStore<D> loadStore()
    {
        return model.getStore();
    }

    /**
     * Adds the specified renderer to the -existing- specified column. In order to have the grid take this change into
     * account, you must ensure that {@link #resetColumns()} or {@link #setup(Hd3dSimpleGridModel)} is called
     * afterwards.
     * 
     * @param columnName
     *            the column name
     * @param renderer
     *            the renderer to add
     */
    public void addRenderer(String columnName, GridCellRenderer<D> renderer)
    {
        if (columnName != null)
        {
            for (ColumnConfig col : columns)
            {
                if (columnName.equals(col.getId()))
                {
                    col.setRenderer(renderer);
                    colHaveChanged = true;
                    return;
                }
            }
        }
    }

    /**
     * Refreshes the model data and by doing so the grid content.
     */
    public void refreshData()
    {
        model.refreshData();
    }

}
