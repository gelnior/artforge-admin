package fr.hd3d.admin.ui.client.rights.view;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * A wrapping view for displaying a simple non-editable grid provided with a selection mechanism, refreshment and
 * supporting constraints on the data to be loaded.
 * 
 * @author HD3D
 * 
 * @param <D>
 *            the inner grid's model data
 */
public interface ISubGridView<D extends Hd3dModelData>
{

    /**
     * Initializes all inner components.
     */
    public void setup();

    /**
     * Hides the inner grid if it was shown.
     */
    public void hideGrid();

    /**
     * Shows the inner grid if it was hidden.
     */
    public void showGrid();

    /**
     * Performs the enabling/disabling actions upon item selection clearance.
     */
    public void onFullDeselection();

    /**
     * Performs the enabling/disabling actions upon full reload of the model store.
     * 
     * @param fullLoaded
     *            whether the data is already fully loaded or not
     */
    public void deselectAll(boolean fullLoaded);

    /**
     * Performs the enabling/disabling actions upon single item selection.
     * 
     * @param canUpdate
     *            whether the user can update selected item
     * @param canDelete
     *            whether the user can delete selected item
     */
    public void onItemSelection(boolean canUpdate, boolean canDelete);

}
