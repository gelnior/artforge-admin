package fr.hd3d.admin.ui.client.model.modeldata;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetAbstractModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;


/**
 * This class is a slight improvement of the generic HD3D model data. The model type it nests contains some more fields
 * that are likely to welcome a data object display-able name.
 * 
 * @author HD3D
 * 
 */
public class GenericModelData extends Hd3dModelData
{
    public static String LABEL_FIELD = "label";

    private static final long serialVersionUID = 30721815036071464L;

    public static ModelType getModelType()
    {
        ModelType type = Hd3dModelData.getModelType();

        // adds all the fields that can stand for a label
        type.addField(RecordModelData.NAME_FIELD);
        type.addField(AssetAbstractModelData.KEY_FIELD);
        type.addField(ConstituentModelData.CONSTITUENT_LABEL);
        type.addField(PersonModelData.PERSON_LOGIN_FIELD);
        type.addField(ConstituentModelData.CONSTITUENT_HOOK);

        return type;
    }

    /**
     * Gets the model data nested label, which can be a name, a key, a hook etc., as long as it represents a better
     * identifier than the object ID itself. If two fields or more are set, an order of priority between the potential
     * label fields is defined. If no field has been set, it just returns the object's id.
     * 
     * @return the most relevant data object identifier: name, key, hook etc.
     */
    public String getLabel()
    {
        String label;
        label = get(AssetAbstractModelData.KEY_FIELD);
        if (label != null)
        {
            return label;
        }
        label = get(ConstituentModelData.CONSTITUENT_LABEL);
        if (label != null)
        {
            return label;
        }
        label = get(PersonModelData.PERSON_LOGIN_FIELD);
        if (label != null)
        {
            return label;
        }
        label = get(RecordModelData.NAME_FIELD);
        if (label != null)
        {
            return label;
        }
        label = get(ConstituentModelData.CONSTITUENT_HOOK);
        if (label != null)
        {
            return label;
        }
        return String.valueOf(getId().longValue());
    }

}
