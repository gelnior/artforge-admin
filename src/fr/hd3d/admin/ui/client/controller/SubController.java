package fr.hd3d.admin.ui.client.controller;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.admin.ui.client.view.ISubView;
import fr.hd3d.common.ui.client.event.CommonEvents;


/**
 * The dedicated controller for an application tab.
 * 
 * @author HD3D
 * 
 */
public abstract class SubController extends Controller
{

    /** The view depending on this controller */
    protected final ISubView view;

    /**
     * Creates a controller for the specified view and register events.
     * 
     * @param tab
     *            the view
     */
    public SubController(ISubView tab)
    {
        this.view = tab;
        registerEvents();
    }

    public void handleEvent(AppEvent event)
    {
        forwardToChild(event);

        EventType type = event.getType();
        if (type == CommonEvents.PERMISSION_INITIALIZED)
        {
            onPermissionsInitialized();
        }
        doHandleEvent(event);
    }

    /**
     * Meant to be overwritten by children, this method is called after the events have been forwarded to children. As
     * can {@link #handleEvent(AppEvent)}, it can only handle registered events.
     * 
     * @param event
     *            the event to handle
     */
    protected abstract void doHandleEvent(AppEvent event);

    /**
     * Register all the events handled by this controller.
     */
    protected void registerEvents()
    {
        registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
    }

    /**
     * Called when the permissions have been acquired by the main controller, this method initializes the view.
     */
    protected void onPermissionsInitialized()
    {
        view.initWidgets();
    }

}
