package fr.hd3d.admin.ui.client.perms.controller;

import java.util.LinkedList;
import java.util.List;

import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.admin.ui.client.perms.model.PermModelData;
import fr.hd3d.admin.ui.client.perms.model.PermsModel;
import fr.hd3d.admin.ui.client.perms.model.PermsTreeStoreUtil;
import fr.hd3d.admin.ui.client.perms.model.ShieldedField;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.common.ui.client.undo.CommandManager;
import fr.hd3d.common.ui.client.undo.UndoableCommand;


/**
 * The permission related command manager. It can mainly execute the five commands corresponding to an action on the
 * five 'create', 'read', 'update', 'delete' and 'ALL' columns. The commands here will recursively update the tree store
 * nodes so that it always remains self-consistent.
 * 
 * @author HD3D
 * 
 */
public final class PermsCommandManager extends CommandManager
{

    PermsModel model;
    TreeStore<PermModelData> store;

    /**
     * Creates a command manager for the permission model.
     * 
     * @param model
     *            a permission model
     */
    public PermsCommandManager(PermsModel model)
    {
        super();
        this.model = model;
        this.store = model.getStore();
    }

    /**
     * Executes a command to take the create column click at the node's row into account.
     * 
     * @param node
     *            the node for which the 'create' right has been clicked on
     */
    public void executeCanCreateClick(PermModelData node)
    {
        executeCommand(new CanCreateClick(node));
    }

    /**
     * Executes a command to take the read column click at the node's row into account.
     * 
     * @param node
     *            the node for which the 'read' right has been clicked on
     */
    public void executeCanReadClick(PermModelData node)
    {
        executeCommand(new CanReadClick(node));
    }

    /**
     * Executes a command to take the update column click at the node's row into account.
     * 
     * @param node
     *            the node for which the 'update' right has been clicked on
     */
    public void executeCanUpdateClick(PermModelData node)
    {
        executeCommand(new CanUpdateClick(node));
    }

    /**
     * Executes a command to take the ALL column click at the node's row into account.
     * 
     * @param node
     *            the node for which the 'ALL' right has been clicked on
     */
    public void executeCanAllClick(PermModelData node)
    {
        executeCommand(new CanAllClick(node));
    }

    /**
     * Executes a command to take the delete column click at the node's row into account.
     * 
     * @param node
     *            the node for which the 'delete' right has been clicked on
     */
    public void executeCanDeleteClick(PermModelData node)
    {
        executeCommand(new CanDeleteClick(node));
    }

    /**
     * A command template capable of facing any change on the permission tree, including passing on the changes to child
     * nodes.
     * 
     * @author HD3D
     * 
     */
    abstract class RightClick extends UndoableCommand
    {
        protected PermModelData data;
        protected ShieldedField field;
        protected int oldValue;

        protected boolean first;
        protected boolean relay;
        protected int mainValue;
        protected boolean passOn;
        protected List<RightClick> nexts;

        // orders
        protected boolean increaseBans = false;
        protected boolean decreaseBans = false;
        protected boolean increasePerms = false;
        protected boolean decreasePerms = false;

        public RightClick(PermModelData perm, int mainValue, boolean passOn)
        {
            data = perm;
            this.first = mainValue == 0;
            this.mainValue = mainValue;

            if (passOn || perm.getType() == PermModelData.ALL)
            {
                this.passOn = true;
            }
            field = get();
        }

        public void execute()
        {
            oldValue = field.getValue();
            if (!first)
            {
                applyOrders();
            }
            int value = proceedValue(oldValue);
            field.setValue(value);

            // /* debug */
            // GWT.log(data.getPath() + " " + this.getClass().getName().split("Can")[1] + " << val " + oldValue + " -> "
            // + value + " passOn = " + passOn + " mValue = " + mainValue + " actual = " + field.getValue()
            // + " perm bans: " + increaseBans + increasePerms + decreaseBans + decreasePerms + " shields "
            // + field.hasBanShield() + field.hasPermShield(), null);

            // pass on child commands
            if (passOn)
            {
                nexts = new LinkedList<RightClick>();
                initChildCommands();
                for (RightClick cmd : nexts)
                {
                    cmd.execute();
                }
            }
        }

        public void undo()
        {
            mainValue = 0;
            if (relay)
            {
                resetRelay();
            }
            else if (!first)
            {
                cancelOrders();
            }
            if (first || relay)
            {
                field.setValue(oldValue);
            }

            // /* debug */
            // GWT.log(data.getPath() + " " + this.getClass().getName().split("Can")[1] + " -> val " + oldValue
            // + " passOn = " + passOn + " mValue = " + mainValue + " actual = " + field.getValue() + " perms: "
            // + increaseBans + increasePerms + decreaseBans + decreasePerms + " shields " + field.hasBanShield()
            // + field.hasPermShield(), null);

            // pass on child commands
            if (passOn)
            {
                for (RightClick cmd : nexts)
                {
                    cmd.undo();
                }
                nexts.clear();
            }
        }

        protected void initChildCommands()
        {
            List<PermModelData> children = PermsTreeStoreUtil.getNextNodes(store, data);
            for (PermModelData child : children)
            {
                addChildCommand(child); // add child commands to 'next'
            }
            for (RightClick cmd : nexts)
            {
                cmd.increaseBans = increaseBans;
                cmd.decreaseBans = decreaseBans;
                cmd.increasePerms = increasePerms;
                cmd.decreasePerms = decreasePerms;
            }
        }

        protected int proceedValue(int value)
        {
            if (mainValue > PermModelData.NONE)
            {
                if (value == mainValue)
                {
                    value = PermModelData.DEFAULT;
                    setRelay();
                }
                if (value > PermModelData.BANNED)
                {
                    value = PermModelData.DEFAULT;
                }
            }
            else if (mainValue == 0)
            {
                // set value to next value in priority order
                if (value == PermModelData.DEDUCED_PERMISSION)
                {
                    value = PermModelData.BANNED; // set ban if value is deduced
                    increaseBans = true;
                }
                else if (value == PermModelData.DEFAULT)
                {
                    value = PermModelData.PERMITTED;
                    increasePerms = true;
                }
                else if (value == PermModelData.PERMITTED)
                {
                    value = PermModelData.BANNED;
                    increaseBans = true;
                    decreasePerms = true;
                }
                else if (value == PermModelData.BANNED)
                {
                    value = PermModelData.DEFAULT;
                    decreaseBans = true;
                }
                else
                {
                    passOn = false; // do nothing
                }
                mainValue = value;
            }
            return value;
        }

        protected void applyOrders()
        {
            if (decreaseBans)
            {
                field.decreaseBanShield();
            }
            if (increaseBans)
            {
                field.increaseBanShield();
            }
            if (decreasePerms)
            {
                field.decreasePermShield();
            }
            if (increasePerms)
            {
                field.increasePermShield();
            }
        }

        protected void cancelOrders()
        {
            if (decreaseBans)
            {
                field.increaseBanShield();
            }
            if (increaseBans || relay)
            {
                field.decreaseBanShield();
            }
            if (decreasePerms)
            {
                field.increasePermShield();
            }
            if (increasePerms)
            {
                field.decreasePermShield();
            }
        }

        protected ShieldedField get()
        {
            return get(data);
        }

        protected abstract void addChildCommand(PermModelData child);

        protected abstract ShieldedField get(PermModelData perm);

        protected void setRelay()
        {
            relay = true;
            if (data.getType() == PermModelData.ALL)
            {
                if (mainValue == PermModelData.BANNED)
                {
                    increaseBans = false;
                    decreasePerms = true; // only passes the order to decrease the permission shield
                }
                else
                {
                    passOn = false; // no need to pass on after a relay
                }
            }
        }

        protected void resetRelay()
        {
            cancelOrders();
        }
    }

    /**
     * This command handles a permission status change on the 'create' column.
     * 
     * @author HD3D
     * 
     */
    class CanCreateClick extends RightClick
    {
        public CanCreateClick(PermModelData perm, int mainValue, boolean passOn)
        {
            super(perm, mainValue, passOn);
        }

        public String getName()
        {
            return AdminMainView.CONSTANTS.CanCreate();
        }

        public CanCreateClick(PermModelData perm)
        {
            super(perm, 0, false);
        }

        protected ShieldedField get(PermModelData perm)
        {
            return perm.getCanCreate();
        }

        protected void set(int value)
        {
            data.setCanCreate(value);
        }

        protected void addChildCommand(PermModelData child)
        {
            nexts.add(new CanCreateClick(child, mainValue, false));
        }
    }

    /**
     * This command handles a permission status change on the 'read' column.
     * 
     * @author HD3D
     * 
     */
    class CanReadClick extends RightClick
    {
        public CanReadClick(PermModelData perm, int mainValue, boolean passOn)
        {
            super(perm, mainValue, passOn);
        }

        public String getName()
        {
            return AdminMainView.CONSTANTS.CanRead();
        }

        public CanReadClick(PermModelData perm)
        {
            super(perm, 0, false);
        }

        protected ShieldedField get(PermModelData perm)
        {
            return perm.getCanRead();
        }

        protected void set(int value)
        {
            data.setCanRead(value);
        }

        protected void addChildCommand(PermModelData child)
        {
            nexts.add(new CanReadClick(child, mainValue, false));
        }
    }

    /**
     * This command handles a permission status change on the 'update' column.
     * 
     * @author HD3D
     * 
     */
    class CanUpdateClick extends RightClick
    {
        public CanUpdateClick(PermModelData perm, int mainValue, boolean passOn)
        {
            super(perm, mainValue, passOn);
        }

        public String getName()
        {
            return AdminMainView.CONSTANTS.CanUpdate();
        }

        public CanUpdateClick(PermModelData perm)
        {
            super(perm, 0, false);
        }

        protected ShieldedField get(PermModelData perm)
        {
            return perm.getCanUpdate();
        }

        protected void set(int value)
        {
            data.setCanUpdate(value);
        }

        protected void addChildCommand(PermModelData child)
        {
            nexts.add(new CanUpdateClick(child, mainValue, false));
        }
    }

    /**
     * This command handles a permission status change on the 'delete' column.
     * 
     * @author HD3D
     * 
     */
    class CanDeleteClick extends RightClick
    {
        public CanDeleteClick(PermModelData perm, int mainValue, boolean passOn)
        {
            super(perm, mainValue, passOn);
        }

        public String getName()
        {
            return AdminMainView.CONSTANTS.CanDelete();
        }

        public CanDeleteClick(PermModelData perm)
        {
            super(perm, 0, false);
        }

        protected ShieldedField get(PermModelData perm)
        {
            return perm.getCanDelete();
        }

        protected void set(int value)
        {
            data.setCanDelete(value);
        }

        protected void addChildCommand(PermModelData child)
        {
            nexts.add(new CanDeleteClick(child, mainValue, false));
        }
    }

    /**
     * This command handles a permission status change on the 'ALL' column.
     * 
     * @author HD3D
     * 
     */
    class CanAllClick extends RightClick
    {
        public CanAllClick(PermModelData perm, int mainValue, boolean passOn)
        {
            super(perm, mainValue, passOn);
        }

        public String getName()
        {
            return AdminMainView.CONSTANTS.AllButton();
        }

        public CanAllClick(PermModelData perm)
        {
            super(perm, 0, true);
        }

        protected ShieldedField get(PermModelData perm)
        {
            return perm.getCanAll();
        }

        protected void set(int value)
        {
            data.setCanAll(value);
        }

        protected void initChildCommands()
        {
            if (first || relay)
            {
                nexts.add(new CanCreateClick(data, mainValue, true));
                nexts.add(new CanReadClick(data, mainValue, true));
                nexts.add(new CanUpdateClick(data, mainValue, true));
                nexts.add(new CanDeleteClick(data, mainValue, true));
            }
            super.initChildCommands();
        }

        protected void addChildCommand(PermModelData child)
        {
            nexts.add(new CanAllClick(child, mainValue, false));
        }

        protected void setRelay()
        {
            super.setRelay();
            passOn = true;
            if (increasePerms)
            {
                decreasePerms = true;
                increasePerms = false;
            }
            else if (decreasePerms)
            {
                decreasePerms = false;
            }
            if (increaseBans)
            {
                decreaseBans = true;
                increaseBans = false;
            }
        }

        protected void resetRelay()
        {
            if (decreasePerms)
            {
                field.decreasePermShield();
            }
            else
            {
                field.increasePermShield();
            }
            if (decreaseBans)
            {
                field.decreaseBanShield();
            }
        }
    }

}
