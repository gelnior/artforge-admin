package fr.hd3d.admin.ui.client.users.view;

/**
 * This view is the main component of the 'User Accounts Management' application. It will react to all the user global
 * operations and propose buttons for creation, password modification and deletion.
 * 
 * @author HD3D
 * 
 */
public interface IAccountView
{

    /**
     * Initializes all the inner widget components.
     */
    public void setup();

    /**
     * Performs all the enabling/disabling actions upon user selection depending on its account status.
     * 
     * @param canCreate
     *            whether the user can create an account for selected user
     * @param canUpdate
     *            whether the user can change the account password
     * @param canDelete
     *            whether the user can delete selected account
     */
    public void onUserSelection(boolean canCreate, boolean canUpdate, boolean canDelete);

    /**
     * Disables all action buttons.
     */
    public void onUsersRefresh();

    /**
     * Displays the appropriate dialog upon 'create account' request.
     */
    public void onAccountCreationDemand();

    /**
     * Displays the appropriate dialog upon 'edit account' request.
     */
    public void onAccountEditionDemand();

    /**
     * Displays a confirmation dialog for the user to confirm the deletion.
     */
    public void onAccountDeletionDemand();

}
