package fr.hd3d.admin.ui.client.users.view;

import fr.hd3d.admin.ui.client.controller.SubController;
import fr.hd3d.admin.ui.client.users.controller.AccountsController;
import fr.hd3d.admin.ui.client.users.controller.UsersController;
import fr.hd3d.admin.ui.client.users.model.AccountsModel;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.admin.ui.client.view.SubView;


/**
 * The 'User Accounts Management' user interface.
 * 
 * @author HD3D
 * 
 */
public class UsersTab extends SubView
{

    // user accounts
    /** the user accounts main model **/
    private AccountsModel accModel = new AccountsModel();
    /** the user accounts grid panel */
    private AccountsPanel accPanel = new AccountsPanel(accModel);
    /** the user accounts event controller */
    private AccountsController accController = new AccountsController(accModel, accPanel);

    public SubController init()
    {
        UsersController controller = new UsersController(this);
        controller.addChild(accController);
        return controller;
    }

    public void initWidgets()
    {
        super.initWidgets();

        this.addCenter(accPanel, 0, 0, 0, 0);
    }

    protected void setStyle()
    {
        setHeading(AdminMainView.CONSTANTS.UsersTabHeader());
        super.setStyle();
    }

}
