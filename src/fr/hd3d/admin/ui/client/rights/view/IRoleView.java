package fr.hd3d.admin.ui.client.rights.view;

import fr.hd3d.admin.ui.client.view.IAbstractRoleView;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;


/**
 * A specification of the role view that can adapt to resource group selection.
 * 
 * @author HD3D
 * 
 */
public interface IRoleView extends IAbstractRoleView
{

    /**
     * Performs enabling/disabling operations upon resource group selection.
     * 
     * @param groupName
     *            the selected group's name
     * @param canUpdate
     *            whether the user can update selected group
     */
    public void onGroupSelection(String groupName, boolean canUpdate);

    /**
     * Performs enabling/disabling operations upon resource group selection clearance.
     */
    public void onGroupDeselection();

    /**
     * Opens the relation tool for the selected resource group.
     * 
     * @param group
     *            the selected group's name
     */
    public void openGroupRelationTool(ResourceGroupModelData group);

}
