package fr.hd3d.admin.ui.client.templates.view;

/**
 * The main view for handling security templates.
 * 
 * @author HD3D
 * 
 */
public interface ISecurityTemplateView
{

    /**
     * Initializes all the panel's widgets and adds sub components.
     */
    public void setup();

    /**
     * Select the newly created security template and adds it to the combo box store.
     */
    public void selectNewTemplate();

    /**
     * Re-adds the selected security template to the combo box store (mainly after template edition).
     */
    public void reselectTemplate();

    /**
     * Removes the selected security template from the combo box store (mainly before template edition).
     */
    public void deselectTemplate();

    /**
     * Allows and enables interface components after template selection.
     * 
     * @param canUpdate
     *            whether the user can update the security template
     * @param canDelete
     *            whether the user can delete the security template
     */
    public void onTemplateSelection(boolean canUpdate, boolean canDelete);

    /**
     * Removes the selected security template from the combo box store and nullifies the combo box selection (mainly
     * before template deletion).
     */
    public void onTemplateDeletion();

    /**
     * Forbids and disables interface components after template selection clearance.
     */
    public void onTemplateDeselection();

    /**
     * Allows and enables interface components after project selection.
     * 
     * @param canApply
     *            whether the user can apply selected template to selected project
     */
    public void onProjectSelection(boolean canApply);

    /**
     * Shows a security template creation dialog upon creation request.
     */
    public void onCreateDemand();

    /**
     * Shows a security template edition dialog upon edition request.
     */
    public void onEditDemand();

    /**
     * Prompts a deletion confirmation dialog upon deletion request.
     */
    public void onDeleteDemand();

}
