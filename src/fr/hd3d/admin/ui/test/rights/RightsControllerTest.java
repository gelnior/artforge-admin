package fr.hd3d.admin.ui.test.rights;

import org.junit.Test;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.model.modeldata.SingleResourceGroupReader;
import fr.hd3d.admin.ui.client.model.modeldata.SingleRoleReader;
import fr.hd3d.admin.ui.client.rights.controller.ResourceGroupController;
import fr.hd3d.admin.ui.client.rights.controller.RightsController;
import fr.hd3d.admin.ui.client.rights.controller.RoleController;
import fr.hd3d.admin.ui.client.rights.model.ResourceGroupModel;
import fr.hd3d.admin.ui.client.rights.model.RoleModel;
import fr.hd3d.admin.ui.test.modeldata.ResourceGroupPagingReaderMock;
import fr.hd3d.admin.ui.test.modeldata.RolePagingReaderMock;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dModelDataReaderSingleton;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.modeldata.reader.ResourceGroupReaderMock;
import fr.hd3d.common.ui.test.util.modeldata.reader.RoleReaderMock;
import fr.hd3d.common.ui.test.widget.sheeteditor.mock.Hd3dModelDataReaderMock;


public class RightsControllerTest extends UITestCase
{

    int random = (int) (1000 * Math.random());

    RoleModel rM;
    RoleViewMock rV;
    int rCount = 0;

    ResourceGroupModel gM;
    ResourceGroupViewMock gV;
    int gCount = 0;

    RightsViewMock mockView;

    public void setUp()
    {
        super.setUp();

        // set super user
        PermissionUtil.setDebugOn();

        // set mock readers
        SingleRoleReader.setSimpleInstance(new RoleReaderMock());
        SingleRoleReader.setInstance(new RolePagingReaderMock());
        SingleResourceGroupReader.setSimpleInstance(new ResourceGroupReaderMock());
        SingleResourceGroupReader.setInstance(new ResourceGroupPagingReaderMock());
        Hd3dModelDataReaderSingleton.setInstanceAsMock(new Hd3dModelDataReaderMock());

        // roles init
        rM = new RoleModel();
        rV = new RoleViewMock(rM);
        RoleController roController = new RoleController(rM, rV);

        // resource groups init
        gM = new ResourceGroupModel();
        gV = new ResourceGroupViewMock(gM);
        ResourceGroupController rgController = new ResourceGroupController(gM, gV);

        mockView = new RightsViewMock(rV, gV);
        RightsController controller = new RightsController(mockView);
        controller.addChild(roController);
        controller.addChild(rgController);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    private void init()
    {
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);

        // save total count
        rCount = rM.getStore().getCount();
        gCount = gM.getStore().getCount();
    }

    private RoleModelData selectRole()
    {
        int i = 0;
        RoleModelData r = rM.getStore().getAt(i++);
        while (r.getPermissions() == null || r.getPermissions().isEmpty())
        {
            r = rM.getStore().getAt(i++);
        }
        AppEvent sel = new AppEvent(AdminEvents.ROLE_SELECTED);
        sel.setData(r);
        EventDispatcher.forwardEvent(sel);

        return r;
    }

    @Test
    public void testInit()
    {
        init();

        assertTrue(mockView.init);
        assertTrue(rV.init);
        assertTrue(gV.init);
        assertTrue(rCount != 0);
        assertTrue(gCount != 0);
    }
    //
    // @Test
    // public void testSelectGroup()
    // {
    // init();
    //
    // ResourceGroupModelData g = gM.getStore().getAt(0);
    // AppEvent sel = new AppEvent(AdminEvents.RESOURCEGROUP_SELECTED);
    // sel.setData(g);
    // EventDispatcher.forwardEvent(sel);
    //
    // assertEquals(g.getName(), rV.groupName);
    // assertEquals(g.getName(), gM.selected().getName());
    // if (g.getRoleIds() != null)
    // {
    // assertEquals(g.getRoleIds().size(), rM.getStore().getCount());
    // }
    //
    // EventDispatcher.forwardEvent(AdminEvents.ALL_GROUPS_CLICKED);
    //
    // assertEquals(gM.getStore().getCount(), gCount);
    // assertEquals(rM.getStore().getCount(), rCount);
    // assertNull(rV.groupName);
    // assertNull(gM.selected());
    // }
    //
    // @Test
    // public void testSelectRole()
    // {
    // init();
    //
    // RoleModelData r = selectRole();
    //
    // assertEquals(r.getName(), rM.selected().getName());
    // assertTrue(rV.actionsEnabled);
    //
    // EventDispatcher.forwardEvent(AdminEvents.ALL_ROLES_CLICKED);
    //
    // assertNull(rM.selected());
    // assertTrue(!rV.actionsEnabled);
    // }
    //
    // @Test
    // public void testRoleDemands()
    // {
    // init();
    //
    // selectRole();
    // int cCount = rV.createCount;
    // int eCount = rV.editCount;
    //
    // EventDispatcher.forwardEvent(AdminEvents.CREATE_ROLE_CLICKED);
    //
    // assertEquals(rV.createCount, ++cCount);
    //
    // EventDispatcher.forwardEvent(AdminEvents.EDIT_ROLE_CLICKED);
    //
    // assertEquals(rV.editCount, ++eCount);
    // }
    //
    // @Test
    // public void testCreatenDeleteRole()
    // {
    // init();
    // String rName = "new role " + random;
    //
    // AppEvent cre = new AppEvent(AdminEvents.CREATE_ROLE_CONFIRMED);
    // cre.setData(rName);
    // EventDispatcher.forwardEvent(cre);
    //
    // assertEquals(rM.selected().getName(), rName);
    //
    // RoleModelData r = rM.selected();
    //
    // assertNotNull(r);
    // assertEquals(r.getName(), rName);
    // assertTrue(rM.getStore().contains(r));
    // assertNotNull(r.getId());
    //
    // AppEvent sel = new AppEvent(AdminEvents.ROLE_SELECTED);
    // sel.setData(r);
    // EventDispatcher.forwardEvent(sel);
    // EventDispatcher.forwardEvent(AdminEvents.DELETE_ROLE_CLICKED);
    //
    // assertTrue(!rM.getStore().contains(r));
    // }
    //
    // @Test
    // public void testAddnRemoveRole()
    // {
    // init();
    // int count = rV.relToolOpened;
    //
    // ResourceGroupModelData g = gM.getStore().getAt(0);
    // AppEvent selg = new AppEvent(AdminEvents.RESOURCEGROUP_SELECTED);
    // selg.setData(g);
    // EventDispatcher.forwardEvent(selg);
    // EventDispatcher.forwardEvent(AdminEvents.ADD_ROLE_CLICKED);
    //
    // assertEquals(rV.relToolOpened, ++count);
    //
    // AppEvent cre = new AppEvent(AdminEvents.CREATE_ROLE_CONFIRMED);
    // cre.setData("added role " + random);
    // EventDispatcher.forwardEvent(cre);
    //
    // RoleModelData r = rM.selected();
    //
    // // assertTrue(rM.getStore().contains(r));
    // assertTrue(g.getRoleIds().contains(r.getId()));
    //
    // AppEvent selr = new AppEvent(AdminEvents.ROLE_SELECTED);
    // selr.setData(r);
    // EventDispatcher.forwardEvent(selr);
    // EventDispatcher.forwardEvent(AdminEvents.REMOVE_ROLE_CLICKED);
    //
    // assertTrue(!rM.getStore().contains(r));
    // assertTrue(g.getRoleIds() == null || !g.getRoleIds().contains(r.getId()));
    //
    // rM.setSelected(r);
    // EventDispatcher.forwardEvent(AdminEvents.DELETE_ROLE_CLICKED);
    // }
    //
    // @Test
    // public void testEditRole()
    // {
    // init();
    //
    // RoleModelData r = selectRole();
    // String name = r.getName();
    // String rName = "edited role " + random;
    //
    // AppEvent ed1 = new AppEvent(AdminEvents.EDIT_ROLE_CONFIRMED);
    // ed1.setData(rName);
    // EventDispatcher.forwardEvent(ed1);
    //
    // assertEquals(r.getName(), rName);
    // assertEquals(rM.selected().getName(), rName);
    //
    // AppEvent ed2 = new AppEvent(AdminEvents.EDIT_ROLE_CONFIRMED);
    // ed2.setData(name);
    // EventDispatcher.forwardEvent(ed2);
    //
    // assertEquals(r.getName(), name);
    // assertEquals(rM.selected().getName(), name);
    // }

}
