package fr.hd3d.admin.ui.client.view;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.event.dom.client.KeyCodes;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * A very simple window for handling item's creation and edition by specifying their name.
 * 
 * @author HD3D
 * 
 */
public class LabelDialog extends Dialog
{

    private TextField<String> text = new TextField<String>();

    protected LabelDialog(String title, EventType type, String name)
    {
        setHeading(title);
        setContent();
        setHideOnButtonClick(true);
        setModal(true);
        if (name != null)
        {
            text.setValue(name);
        }
        else
        {
            text.setRawValue("");
        }
        setButtons(Dialog.OKCANCEL);
        Button ok = getButtonById(OK);
        ok.addListener(Events.Select, new LabelDialogListener(type));
        text.addKeyListener(new LabelDialogKeyListener(type));
        Button cancel = getButtonById(CANCEL);
        cancel.setText(AdminMainView.CONSTANTS.Cancel());

        this.setFocusWidget(text);
    }

    /**
     * Builds a dialog fit for item creation.
     * 
     * @param title
     *            the dialog heading
     * @param type
     *            the event to forward for creation
     * @return the creation label dialog
     */
    public static LabelDialog getCreationDialog(String title, EventType type)
    {
        LabelDialog dialog = new LabelDialog(title, type, null);
        dialog.setDefaultText("");
        return dialog;
    }

    /**
     * Builds a dialog fit for item name edition
     * 
     * @param title
     *            the dialog heading
     * @param type
     *            the event to forward for edition
     * @param name
     *            the item's former name
     * @return the edition label dialog
     */
    public static LabelDialog getEditionDialog(String title, EventType type, String name)
    {
        LabelDialog dialog = new LabelDialog(title, type, name);
        return dialog;
    }

    /**
     * Set the default text value in the name edition text field.
     * 
     * @param text
     *            the default text value
     */
    public void setDefaultText(String text)
    {
        this.text.setRawValue(text);
    }

    /**
     * Sets the 'Type Text' label and the text field.
     */
    private void setContent()
    {
        setAutoHeight(true);
        setWidth(360);

        FormPanel panel = new FormPanel();
        panel.setHeaderVisible(false);
        panel.setBodyBorder(false);
        panel.setWidth(360);
        panel.setAutoHeight(true);

        FormLayout layout = new FormLayout();
        layout.setLabelWidth(80);
        panel.setLayout(layout);

        text.setFieldLabel(AdminMainView.MESSAGES.TypeText());
        text.setWidth(160);
        text.setAllowBlank(false);
        text.setMaxLength(255);

        panel.add(text, new FormData("-40"));
        add(panel);
    }

    /**
     * When OK button is clicked, it checks if text data are corrects then it forwards an event with the text data. If
     * text data are not valid an error is raised.
     * 
     * @param type
     */
    private void onClic(EventType type)
    {
        String value = (String) text.getValue();
        if (value.length() > 255)
        {
            value = value.substring(0, 255);
        }
        AppEvent event;
        if (value != null)
        {
            event = new AppEvent(type);
            event.setData(value);
        }
        else
        {
            event = new AppEvent(CommonEvents.ERROR);
            event.setData(AdminMainView.MESSAGES.NullTextFieldError());
        }
        EventDispatcher.forwardEvent(event);
    }

    /**
     * A listener specifically designed to be affected to the 'OK' button of the label dialog.
     * 
     * @author HD3D
     * 
     */
    class LabelDialogListener implements Listener<ButtonEvent>
    {
        private EventType type;

        public LabelDialogListener(EventType type)
        {
            this.type = type;
        }

        public void handleEvent(ButtonEvent be)
        {
            onClic(type);
        }

    }

    /**
     * A listener specifically designed to be affected to the key enter up action of the label dialog.
     * 
     * @author HD3D
     * 
     */
    class LabelDialogKeyListener extends KeyListener
    {
        private EventType type;

        public LabelDialogKeyListener(EventType type)
        {
            this.type = type;
        }

        public void componentKeyUp(ComponentEvent event)
        {
            if (event.getKeyCode() == KeyCodes.KEY_ENTER)
            {
                onClic(type);
                hide();
                text.clear();
            }
        }
    }
}
