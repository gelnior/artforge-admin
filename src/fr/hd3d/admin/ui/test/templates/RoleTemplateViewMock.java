package fr.hd3d.admin.ui.test.templates;

import fr.hd3d.admin.ui.client.templates.model.RoleTemplateModel;
import fr.hd3d.admin.ui.client.templates.view.IRoleTemplateView;
import fr.hd3d.admin.ui.test.AbstractRoleViewMock;


public class RoleTemplateViewMock extends AbstractRoleViewMock implements IRoleTemplateView
{

    public boolean shown = false;

    public RoleTemplateViewMock(RoleTemplateModel model)
    {
        super(model);
    }

    public void onTemplateDeselection()
    {
        shown = false;
    }

    public void onTemplateSelection(boolean canUpdate)
    {
        shown = true;
    }

}
