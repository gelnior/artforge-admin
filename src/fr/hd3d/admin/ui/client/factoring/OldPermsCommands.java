package fr.hd3d.admin.ui.client.factoring;

import java.util.List;

import fr.hd3d.common.ui.client.modeldata.NameModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.undo.UndoableCommand;


@Deprecated
public abstract class OldPermsCommands extends UndoableCommand
{

    private OldPermsModel model;
    private RoleModelData role;
    private NameModelData right;

    private boolean bans;
    private boolean add;

    public OldPermsCommands(OldPermsModel model, NameModelData right, boolean bans, boolean add)
    {
        this.model = model;
        this.role = model.getRole();
        this.right = right;
        this.bans = bans;
        this.add = add;
    }

    public void undo()
    {
        if (add)
        {
            remove();
        }
        else
        {
            add();
        }
    }

    public void execute()
    {
        if (add)
        {
            add();
        }
        else
        {
            remove();
        }
    }

    private void remove()
    {
        List<String> rights = get();
        rights.remove(right.getName());
        set(rights);
        model.refreshStore(right, false, bans);
    }

    private void add()
    {
        List<String> rights = get();
        rights.add(right.getName());
        set(rights);
        model.refreshStore(right, true, bans);
    }

    private void set(List<String> rights)
    {
        if (bans)
        {
            role.setBans(rights);
        }
        else
        {
            role.setPermissions(rights);
        }
    }

    private List<String> get()
    {
        if (bans)
        {
            return role.getBans();
        }
        return role.getPermissions();
    }

    public static OldPermsCommands addPermissionCommand(OldPermsModel model, NameModelData right)
    {
        return new AddPermission(model, right);
    }

    static class AddPermission extends OldPermsCommands
    {
        public AddPermission(OldPermsModel model, NameModelData right)
        {
            super(model, right, false, true);
        }
    }

    public static OldPermsCommands addBanCommand(OldPermsModel model, NameModelData right)
    {
        return new AddBan(model, right);
    }

    static class AddBan extends OldPermsCommands
    {
        public AddBan(OldPermsModel model, NameModelData right)
        {
            super(model, right, true, true);
        }
    }

    public static OldPermsCommands removePermissionCommand(OldPermsModel model, NameModelData right)
    {
        return new RemovePermission(model, right);
    }

    static class RemovePermission extends OldPermsCommands
    {
        public RemovePermission(OldPermsModel model, NameModelData right)
        {
            super(model, right, false, false);
        }
    }

    public static OldPermsCommands removeBanCommand(OldPermsModel model, NameModelData right)
    {
        return new RemoveBan(model, right);
    }

    static class RemoveBan extends OldPermsCommands
    {
        public RemoveBan(OldPermsModel model, NameModelData right)
        {
            super(model, right, true, false);
        }
    }

}
