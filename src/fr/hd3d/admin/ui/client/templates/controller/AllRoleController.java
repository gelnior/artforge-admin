package fr.hd3d.admin.ui.client.templates.controller;

import fr.hd3d.admin.ui.client.templates.model.AllRoleModel;
import fr.hd3d.admin.ui.client.templates.view.IRoleTemplateView;


/**
 * A controller specifically designed to handle other (not owned) role templates.
 * 
 * @author HD3D
 * 
 */
public class AllRoleController extends RoleTemplateController<AllRoleModel>
{

    public AllRoleController(AllRoleModel model, IRoleTemplateView view)
    {
        super(model, view, false);
    }

    protected void onAddingDemand()
    {
        model.addRoleToTemplate();
    }

    protected void onRemovalDemand()
    {} // do precisely nothing

}
