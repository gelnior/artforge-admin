package fr.hd3d.admin.ui.client.mountpoint.view;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.widget.form.TextField;

import fr.hd3d.admin.ui.client.mountpoint.model.MountPointCreatorModel;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.widget.dialog.FormDialog;


public class MountPointCreatorDialog extends FormDialog
{
    private MountPointCreatorModel model;

    protected TextField<String> storageName = new TextField<String>();
    protected TextField<String> os = new TextField<String>();
    protected TextField<String> path = new TextField<String>();

    public MountPointCreatorDialog()
    {
        super(null, "Create a mount point");

        model = new MountPointCreatorModel();

        this.setWidth(333);
        this.addFields();

    }

    private void addFields()
    {
        storageName.setFieldLabel("Storage Name");
        os.setFieldLabel("Operating System");
        path.setFieldLabel("Mount point");

        addField(storageName);
        addField(os);
        addField(path);

    }

    public void reset()
    {
        storageName.reset();
        os.reset();
        path.reset();
    }

    @Override
    protected void onOkClicked() throws Hd3dException
    {

        if (storageName == null || storageName.equals(""))
        {
            throw new Hd3dException("The storage name is not valid.");
        }
        if (os == null || os.equals(""))
        {
            throw new Hd3dException("The operating system is not valid.");
        }
        if (path == null || path.equals(""))
        {
            throw new Hd3dException("The path is not valid.");
        }
        this.model.setMountPoint(storageName.getValue(), os.getValue(), path.getValue());

        this.model.save(okEvent);
    }

}
