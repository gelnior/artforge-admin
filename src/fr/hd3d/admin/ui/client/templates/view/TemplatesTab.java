package fr.hd3d.admin.ui.client.templates.view;

import fr.hd3d.admin.ui.client.controller.SubController;
import fr.hd3d.admin.ui.client.templates.controller.OwnedRoleController;
import fr.hd3d.admin.ui.client.templates.controller.SecurityTemplateController;
import fr.hd3d.admin.ui.client.templates.controller.TemplatesPermsController;
import fr.hd3d.admin.ui.client.templates.controller.TemplatesController;
import fr.hd3d.admin.ui.client.templates.controller.AllRoleController;
import fr.hd3d.admin.ui.client.templates.model.SecurityTemplateModel;
import fr.hd3d.admin.ui.client.templates.model.OwnedRoleModel;
import fr.hd3d.admin.ui.client.templates.model.AllRoleModel;
import fr.hd3d.admin.ui.client.templates.model.TemplatesPermsModel;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.admin.ui.client.view.SubView;


/**
 * The 'Security Templates Management' user interface.
 * 
 * @author HD3D
 * 
 */
public class TemplatesTab extends SubView
{

    // security templates model
    /** the security template wrapping model */
    private SecurityTemplateModel stModel = new SecurityTemplateModel();

    // owned roles panel
    /** the owned roles model */
    private OwnedRoleModel orModel = new OwnedRoleModel(stModel);
    /** the owned roles grid panel */
    private OwnedRolePanel orPanel = new OwnedRolePanel(orModel);
    /** the owned role events controller */
    private OwnedRoleController orController = new OwnedRoleController(orModel, orPanel);

    // other roles panel
    /** the other roles model */
    private AllRoleModel arModel = new AllRoleModel(stModel);
    /** the other roles grid panel */
    private AllRolePanel arPanel = new AllRolePanel(arModel);
    /** the other roles event controller */
    private AllRoleController arController = new AllRoleController(arModel, arPanel);

    // security templates panel
    /** the main security templates panel */
    private SecurityTemplatePanel stPanel = new SecurityTemplatePanel(stModel, orPanel, arPanel);
    /** the main security templates controller */
    private SecurityTemplateController stController = new SecurityTemplateController(stModel, stPanel);

    // perms n bans
    /** the permissions and bans model */
    private TemplatesPermsModel peModel = new TemplatesPermsModel();
    /** the permissions and bans grids panel */
    private TemplatesPermsPanel pePanel = new TemplatesPermsPanel(peModel);
    /** the permissions and bans event controller */
    private TemplatesPermsController peController = new TemplatesPermsController(peModel, pePanel);

    public SubController init()
    {
        TemplatesController controller = new TemplatesController(this);
        controller.addChild(peController);
        stController.addChild(orController);
        stController.addChild(arController);
        controller.addChild(stController);
        return controller;
    }

    public void initWidgets()
    {
        super.initWidgets();

        this.addCenter(stPanel);
        this.addEast(pePanel, 600);
    }

    protected void setStyle()
    {
        setHeading(AdminMainView.CONSTANTS.TemplatesTabHeader());
        super.setStyle();
    }

}
