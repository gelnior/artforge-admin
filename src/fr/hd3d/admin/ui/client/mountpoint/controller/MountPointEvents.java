package fr.hd3d.admin.ui.client.mountpoint.controller;

import com.extjs.gxt.ui.client.event.EventType;


public class MountPointEvents
{

    public static final EventType TAB_MOUNT_POINT_SELECTED = new EventType();
    public static final EventType ADD_MOUNTPOINT_CLICKED = new EventType();
    public static final EventType DELETE_MOUNTPOINT_CLICKED = new EventType();
    public static final EventType REFRESH_CLICKED = new EventType();
    public static final EventType DELETE_MOUNTPOINT_CONFIRMED = new EventType();
    public static final EventType AFTER_EDIT = new EventType();

}
