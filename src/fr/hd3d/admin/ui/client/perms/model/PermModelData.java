package fr.hd3d.admin.ui.client.perms.model;

import com.extjs.gxt.ui.client.data.BaseModel;


/**
 * A model data that represents a permission node on which a 'create', 'read', 'update', 'delete' and 'ALL' permission
 * value can be set. Permission model data can be of different types: object type (OBJECT), version, object instance
 * (ID), other... Permission values can be 'banned', 'permitted', 'default' or 'none', the latter meaning that the right
 * value cannot be set. Since the permission nodes submit to a tree-based hierarchy, the right values are wrapped in
 * so-called shielded fields that are able to count other node's influences on the field's values. For example, if a
 * parent 'ALL' node's right to 'read' is set to 'permitted', then the child node will be affected insofar as its own
 * 'read' right field's 'permission shield' will be increased to take into account its parent node's influence. Shield
 * values can be transfered to child nodes through dedicated methods.
 * 
 * @author HD3D
 * 
 */
public class PermModelData extends BaseModel
{

    private static final long serialVersionUID = 2938059066005372785L;

    /** the permission node's identifier */
    public final static String IDENTIFIER = "identifier";
    /** the permission node's display label */
    public final static String LABEL = "label";
    /** the node's type */
    public final static String TYPE = "type";
    /** the node's permission prefix */
    public final static String PATH = "path";
    /** the 'read' permission field name */
    public final static String CAN_READ = "canRead";
    /** the 'delete' permission field name */
    public final static String CAN_DELETE = "canDelete";
    /** the 'update' permission field name */
    public final static String CAN_UPDATE = "canUpdate";
    /** the 'create' permission field name */
    public final static String CAN_CREATE = "canCreate";
    /** the 'ALL' permission field name */
    public final static String CAN_ALL = "canAll";

    // types
    /** the object class type value */
    public final static int OBJECT = 0;
    /** the object instance type value */
    public final static int ID = 1;
    /** the version name type value */
    public final static int VERSION = 2;
    /** the 'wild card' type value */
    public final static int ALL = 3;
    /** another, but known, node type, yet not included in the former types */
    public final static int OTHER = 4;
    /** a type value specifying that the node type is unknown */
    public final static int UNKNOWN = 5;

    // values
    /** the right value cannot be set */
    public final static int NONE = 10;
    /** the 'default', meaning that the subsequent right is neither 'permitted' nor 'banned' */
    public final static int DEFAULT = NONE + 1;
    /** the 'permitted' value */
    public final static int PERMITTED = NONE + 2;
    /** the 'banned' value */
    public final static int BANNED = NONE + 3;
    /** the difference (positive) between a permission value and its 'deduced' counterpart */
    public final static int DEDUCED_GAP = 3;
    /** a value denoting that this permission's value is 'permitted' because a parent node is */
    public final static int DEDUCED_PERMISSION = PERMITTED + DEDUCED_GAP;
    /** a value denoting that this permission's value is 'banned' because a parent node is */
    public final static int DEDUCED_BAN = BANNED + DEDUCED_GAP;

    /** whether the node has been expanded */
    public boolean expanded = false;

    /**
     * Creates a permission representing model data.
     * 
     * @param identifier
     *            the permission node's identifier
     * @param type
     *            the permission node's type
     * @param path
     *            the permission string prefix of the node
     */
    public PermModelData(String identifier, int type, String path)
    {
        super();
        setIdentifier(identifier);
        setType(type);
        setPath(path);

        /* temporary */
        setLabel(identifier);

        // set permission fields to default
        setCanAll(DEFAULT);
        setCanCreate(DEFAULT);
        setCanDelete(DEFAULT);
        setCanRead(DEFAULT);
        setCanUpdate(DEFAULT);
    }

    /**
     * Transfers the node's fields' shield values to the specified child node.
     * 
     * @param child
     *            the node to which the shields will be transfered
     */
    public void transferShields(PermModelData child)
    {
        if (getType() == PermModelData.ALL)
        {
            getCanRead().transferShields(child.getCanRead());
            getCanUpdate().transferShields(child.getCanUpdate());
            getCanDelete().transferShields(child.getCanDelete());
            getCanCreate().transferShields(child.getCanCreate());
        }
        else
        {
            getCanAll().transferShields(child.getCanRead());
            getCanAll().transferShields(child.getCanUpdate());
            getCanAll().transferShields(child.getCanDelete());
            getCanAll().transferShields(child.getCanCreate());
        }
        getCanAll().transferShields(child.getCanAll());
    }

    /**
     * Indicates whether the node contains non-'default' values amid its permission fields.
     * 
     * @return true if some of the node's values are not 'default'
     */
    public boolean hasValues()
    {
        boolean result = false;
        result = result || getCanAll().hasValue();
        result = result || getCanRead().hasValue();
        result = result || getCanCreate().hasValue();
        result = result || getCanUpdate().hasValue();
        result = result || getCanDelete().hasValue();
        return result;
    }

    /**
     * Indicates whether the node contains a 'banned' or 'permitted' value depending on the boolean parameter's value.
     * 
     * @param isBan
     *            whether to look for 'banned' or 'permitted' values
     * @return true if some of the node's values are 'banned' if <code>isBan</code> is true or 'permitted' if not
     */
    public boolean hasValues(boolean isBan)
    {
        boolean result = false;
        result = result || getCanAll().hasValue(isBan);
        result = result || getCanRead().hasValue(isBan);
        result = result || getCanCreate().hasValue(isBan);
        result = result || getCanUpdate().hasValue(isBan);
        result = result || getCanDelete().hasValue(isBan);
        return result;
    }

    /**
     * Sets the permission node's identifier.
     * 
     * @param identifier
     *            the identifier to set
     */
    public void setIdentifier(String identifier)
    {
        set(IDENTIFIER, identifier);
    }

    /**
     * Gets the permission node's identifier.
     * 
     * @return the node's identifier
     */
    public String getIdentifier()
    {
        return (String) get(IDENTIFIER);
    }

    /**
     * Sets the permission node's label.
     * 
     * @param label
     *            the label to set
     */
    public void setLabel(String label)
    {
        set(LABEL, label);
    }

    /**
     * Gets the permission node's label.
     * 
     * @return the node's label
     */
    public String getLabel()
    {
        return (String) get(LABEL);
    }

    /**
     * Sets the permission node's path.
     * 
     * @param path
     *            the path to set
     */
    public void setPath(String path)
    {
        set(PATH, path);
    }

    /**
     * Gets the permission node's path.
     * 
     * @return the node's path
     */
    public String getPath()
    {
        return (String) get(PATH);
    }

    /**
     * Sets the permission node's type.
     * 
     * @param i
     *            the type to set
     */
    public void setType(int i)
    {
        set(TYPE, new Integer(i));
    }

    /**
     * Gets the permission node's type.
     * 
     * @return the node's type
     */
    public int getType()
    {
        Object object = get(TYPE);
        if (object instanceof Double)
        {
            Double value = (Double) object;
            return value.intValue();
        }
        return ((Integer) object).intValue();
    }

    /**
     * Sets the permission node's 'read' right.
     * 
     * @param i
     *            the new right value
     */
    public void setCanRead(int i)
    {
        if (getCanRead() == null)
        {
            set(CAN_READ, new ShieldedField(getType() == ID || getType() == ALL));
        }
        getCanRead().setValue(i);
    }

    /**
     * Gets the shielded field containing the 'read' right value.
     * 
     * @return the shielded field for the requested right
     */
    public ShieldedField getCanRead()
    {
        return (ShieldedField) get(CAN_READ);
    }

    /**
     * Sets the permission node's 'update' right.
     * 
     * @param i
     *            the new right value
     */
    public void setCanUpdate(int i)
    {
        if (getCanUpdate() == null)
        {
            set(CAN_UPDATE, new ShieldedField(getType() == ID || getType() == ALL));
        }
        getCanUpdate().setValue(i);
    }

    /**
     * Gets the shielded field containing the 'update' right value.
     * 
     * @return the shielded field for the requested right
     */
    public ShieldedField getCanUpdate()
    {
        return (ShieldedField) get(CAN_UPDATE);
    }

    /**
     * Sets the permission node's 'delete' right.
     * 
     * @param i
     *            the new right value
     */
    public void setCanDelete(int i)
    {
        if (getCanDelete() == null)
        {
            set(CAN_DELETE, new ShieldedField(getType() == ID || getType() == ALL));
        }
        getCanDelete().setValue(i);
    }

    /**
     * Gets the shielded field containing the 'delete' right value.
     * 
     * @return the shielded field for the requested right
     */
    public ShieldedField getCanDelete()
    {
        return (ShieldedField) get(CAN_DELETE);
    }

    /**
     * Sets the permission node's 'create' right.
     * 
     * @param i
     *            the new right value
     */
    public void setCanCreate(int i)
    {
        if (getCanCreate() == null)
        {
            set(CAN_CREATE, new ShieldedField(getType() == OBJECT));
        }
        getCanCreate().setValue(i);
    }

    /**
     * Gets the shielded field containing the 'create' right value.
     * 
     * @return the shielded field for the requested right
     */
    public ShieldedField getCanCreate()
    {
        return (ShieldedField) get(CAN_CREATE);
    }

    /**
     * Sets the permission node's 'ALL' right.
     * 
     * @param i
     *            the new right value
     */
    public void setCanAll(int i)
    {
        if (getCanAll() == null)
        {
            set(CAN_ALL, new ShieldedField(getType() != ALL));
        }
        getCanAll().setValue(i);
    }

    /**
     * Gets the shielded field containing the 'ALL' right value.
     * 
     * @return the shielded field for the requested right
     */
    public ShieldedField getCanAll()
    {
        return (ShieldedField) get(CAN_ALL);
    }

    public int hashCode()
    {
        int prime = 31;
        int result = getPath().hashCode() * (getType() - VERSION);
        result = prime * result + getIdentifier().hashCode();
        result = prime * result + getType();
        return result;
    }

    public boolean equals(Object o)
    {
        if (o instanceof PermModelData)
        {
            PermModelData that = (PermModelData) o;
            if (getType() == that.getType())
            {
                if (getType() != VERSION)
                {
                    if (!getPath().equals(that.getPath()))
                    {
                        return false;
                    }
                }
                return getIdentifier().equals(that.getIdentifier());
            }
            return false;
        }
        return false;
    }

}
