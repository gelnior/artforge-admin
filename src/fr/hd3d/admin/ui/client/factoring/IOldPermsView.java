package fr.hd3d.admin.ui.client.factoring;

import com.extjs.gxt.ui.client.mvc.AppEvent;


@Deprecated
public interface IOldPermsView
{

    /**
     * Initializes all the inner widget content.
     */
    public void setup();

    /**
     * Performs enabling/disabling actions upon role selection.
     */
    public void onRoleSelection();

    public void showGrid();

    /**
     * Invokes a dialog upon 'create right' demand.
     * 
     * @param isBanType
     *            whether the right to add is a ban or a permission
     */
    public void invokeRightCreator(boolean isBanType);

    /**
     * Performs enabling/disabling actions upon undo-able action.
     */
    public void onUndoableAction();

    /**
     * Performs enabling/disabling actions upon 'undo'.
     */
    public void onUndo();

    /**
     * Disables the 'redo' button.
     */
    public void onEmptyRedoStack();

    /**
     * Disables the 'undo' button.
     */
    public void onEmptyUndoStack();

    /**
     * Shows a 'quit and/or save' dialog that allows the user to cancel or save all modifications on selected role's
     * properties.
     * 
     * @param delayedEvent
     *            the event that triggered this call
     */
    public void showQuitAndSaveDialog(AppEvent delayedEvent);

}
