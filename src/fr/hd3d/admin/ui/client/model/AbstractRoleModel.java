package fr.hd3d.admin.ui.client.model;

import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.admin.ui.client.model.modeldata.SingleRoleReader;
import fr.hd3d.admin.ui.client.rights.model.SubGridModel;
import fr.hd3d.common.ui.client.modeldata.reader.IPagingReader;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;


/**
 * The adequate model that concentrates all role-related relevant methods.
 * 
 * @author HD3D
 * 
 */
public abstract class AbstractRoleModel extends SubGridModel<RoleModelData>
{

    protected IPagingReader<RoleModelData> getPagingReader()
    {
        return SingleRoleReader.getPagingReader();
    }

    protected IReader<RoleModelData> getSimpleReader()
    {
        return SingleRoleReader.getReader();
    }

    /**
     * Defines the event type that indicates that the selected role should be refreshed from server.
     * 
     * @return the 'refresh is needed' event type
     */
    public abstract EventType refreshNeededEvent();

    /**
     * Creates a role with specified name, adds it to the store and posts it to the server.
     * 
     * @param name
     *            the new role's name
     */
    public void createLine(String name)
    {
        RoleModelData role = new RoleModelData();
        role.setName(name);
        EventType type = onCreationWhileAssociated(role);
        store.add(role);
        selected = role;
        role.save(type);
    }

    /**
     * Meant to be overwritten by children. When the created group is associated with a role or a template, this method
     * may for example bind the newly created role to a group or a security template.
     * 
     * @param role
     *            the newly created role
     * @return the event type to relay
     */
    protected EventType onCreationWhileAssociated(RoleModelData role)
    {
        return refreshNeededEvent();
    }

    /**
     * Changes the selected role name and puts in the server.
     * 
     * @param name
     *            the new role name
     */
    public void editLine(String name)
    {
        store.remove(selected);
        selected.setName(name);
        store.add(selected);
        selected.save(refreshNeededEvent());
    }

    /**
     * Deletes the selected role, removing it from the store and sending a delete request to the server.
     */
    public void deleteLine()
    {
        store.remove(selected);
        if (selected != null)
        {
            selected.delete(refreshNeededEvent());
            setSelected(null);
        }
    }

    /**
     * Gets the role model data class name. Can be overridden by children classes in case of model data change.
     * 
     * @return the role model data class name
     */
    public String getRoleClassName()
    {
        return RoleModelData.SIMPLE_CLASS_NAME;
    }

    /**
     * Defines the event type that reports a creation request.
     * 
     * @return the creation request event type.
     */
    public abstract EventType creationClickEvent();

    /**
     * Defines the event type that reports a role creation.
     * 
     * @return the creation event type.
     */
    public abstract EventType creationConfirmEvent();

    /**
     * Defines the event type that reports an edition request.
     * 
     * @return the edition request event type.
     */
    public abstract EventType editionClickEvent();

    /**
     * Defines the event type that reports a role edition.
     * 
     * @return the edition event type.
     */
    public abstract EventType editionConfirmEvent();

    /**
     * Defines the event type that reports a deletion request.
     * 
     * @return the deletion request event type.
     */
    public abstract EventType deleteClickEvent();

    /**
     * Defines the event type that reports an adding request.
     * 
     * @return the adding request event type.
     */
    public abstract EventType addingClickEvent();

    /**
     * Defines the event type that reports a removal request.
     * 
     * @return the removal request event type.
     */
    public abstract EventType removeClickEvent();

}
