package fr.hd3d.admin.ui.test.users;

import fr.hd3d.admin.ui.client.users.model.AccountsModel;
import fr.hd3d.admin.ui.client.users.view.IAccountView;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;


public class AccountViewMock implements IAccountView
{

    private AccountsModel model;

    public PersonModelData user;
    public boolean init = false;
    public int demands = 0;

    public AccountViewMock(AccountsModel model)
    {
        this.model = model;
    }

    public void onAccountCreationDemand()
    {
        demands = 4 * demands + 3;
    }

    public void onAccountDeletionDemand()
    {
        demands = 4 * demands + 1;
    }

    public void onAccountEditionDemand()
    {
        demands = 4 * demands + 2;
    }

    public void onUserSelection(boolean canCreate, boolean canUpdate, boolean canDelete)
    {
        user = model.getSelected();
    }

    public void onUsersRefresh()
    {
        user = null;
    }

    public void setup()
    {
        init = true;
    }

}
