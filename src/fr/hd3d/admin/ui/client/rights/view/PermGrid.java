package fr.hd3d.admin.ui.client.rights.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.google.gwt.core.client.GWT;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.NameModelData;


public class PermGrid extends Grid<NameModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Column configuration needed by the grid to set columns. */
    // private final List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

    // private boolean bans;

    public PermGrid(boolean bans, ListStore<NameModelData> store)
    {
        super(store, setColumns(bans));
        // this.bans = bans;

        // this.setColumns();
        // reconfigure(store, cm);
        this.setStyle();
    }

    private static ColumnModel setColumns(boolean bans)
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();
        addColumnToConfig(columns, NameModelData.NAME_FIELD, CONSTANTS.Name(), 200, false, null);
        RemoveButtonRenderer removeRenderer;
        if (bans)
        {
            removeRenderer = new RemoveButtonRenderer(AdminEvents.REMOVE_BAN);
        }
        else
        {
            removeRenderer = new RemoveButtonRenderer(AdminEvents.REMOVE_PERMISSION);
        }
        addColumnToConfig(columns, "delete", "", 40, false, removeRenderer);
        return new ColumnModel(columns);
    }

    private void setStyle()
    {
        setAutoExpandColumn(NameModelData.NAME_FIELD);
        setAutoExpandMax(1200);
        setHideHeaders(true);
        setLoadMask(false);
        setBorders(true);
    }

    private static void addColumnToConfig(List<ColumnConfig> columns, String name, String header, int width,
            boolean resizable, GridCellRenderer<NameModelData> renderer)
    {
        final ColumnConfig column = new ColumnConfig();
        column.setId(name);
        column.setHeader(header);
        column.setWidth(width);
        column.setResizable(resizable);
        column.setRenderer(renderer);

        columns.add(column);
    }

    /**
     * adds a button with proper listener, icon, text...
     */
    protected Button setupButton(String text, String toolTip, String iconCSS, SelectionListener<ButtonEvent> listener)
    {
        Button result = new Button();
        if (text != null)
        {
            result.setText(text);
        }
        if (toolTip != null)
        {
            result.setToolTip(toolTip);
        }
        if (iconCSS != null)
        {
            result.setIconStyle(iconCSS);
        }
        if (listener != null)
        {
            result.addSelectionListener(listener);
        }
        return result;
    }

    /**
     * This awkward method prevents a but that I didn't bother to solve. When the grid is hidden and it is shown upon
     * usual <code>show()</code> method call, the grid is not rendered correctly...
     */
    public void reShow()
    {
        super.show();
        // reconfigure(store, cm);
        // // reconfigure(store, cm);
    }

}
