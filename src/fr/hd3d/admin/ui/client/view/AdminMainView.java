package fr.hd3d.admin.ui.client.view;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.Viewport;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;

import fr.hd3d.admin.ui.client.constant.AdminConstants;
import fr.hd3d.admin.ui.client.constant.AdminMessages;
import fr.hd3d.admin.ui.client.controller.AdminMainController;
import fr.hd3d.admin.ui.client.controller.SubController;
import fr.hd3d.admin.ui.client.model.AdminMainModel;
import fr.hd3d.admin.ui.client.mountpoint.controller.MountPointEvents;
import fr.hd3d.admin.ui.client.mountpoint.view.MountPointTab;
import fr.hd3d.admin.ui.client.rights.view.RightsTab;
import fr.hd3d.admin.ui.client.templates.view.TemplatesTab;
import fr.hd3d.admin.ui.client.users.view.UsersTab;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.listener.EventBaseListener;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.widget.mainview.MainView;


/**
 * This is the main Admin view. It consists in three tabs, each of which is dedicated to one aspect of the application,
 * which are 'Roles and Rights Management', 'User Accounts Management' and 'Security Templates Management'.
 * 
 * @author HD3D
 * 
 */
public class AdminMainView extends MainView implements IAdminMainView
{
    /** constants */
    public static AdminConstants CONSTANTS = GWT.create(AdminConstants.class);
    /** messages */
    public static AdminMessages MESSAGES = GWT.create(AdminMessages.class);

    /** the main and here peculiarly unused model */
    private final AdminMainModel model = new AdminMainModel();

    /** the main tab panel */
    private final TabPanel mainPanel = new TabPanel();

    /** the rights panel, capable of handling roles and their permissions and bans */
    private final SubView rightsTab = new RightsTab();

    /** the users panel, capable of handling user accounts */
    private final SubView usersTab = new UsersTab();

    /** the templates panel, capable of handling security templates */
    private final SubView templatesTab = new TemplatesTab();

    /** the mount point panel, capable of handling mount point */
    private final SubView mountPointTab = new MountPointTab();

    /**
     * Default constructor. Sets the application name for the start panel.
     */
    public AdminMainView()
    {
        super(CONSTANTS.Admin());
    }

    /**
     * Initializes all the application's main controllers and affect them to the actual Admin main controller as
     * children.
     */
    public void init()
    {
        AdminMainController mainController = new AdminMainController(model, this);

        SubController rightsController = rightsTab.init();
        mainController.addChild(rightsController);

        SubController usersController = usersTab.init();
        mainController.addChild(usersController);

        SubController templatesController = templatesTab.init();
        mainController.addChild(templatesController);

        SubController mountPointController = mountPointTab.init();
        mainController.addChild(mountPointController);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.addController(mainController);

        // local setting
        ExcludedField.addExcludedField(ResourceGroupModelData.RESOURCE_IDS_FIELD.toLowerCase());
    }

    /**
     * Displays an error message depending on error code passed as a parameter.
     * 
     * @param error
     *            The code of the error to display.
     */
    @Override
    public void displayError(Integer error)
    {
        super.displayError(error);

        switch (error)
        {
            default:
                ;
        }
    }

    /**
     * Initializes the user interface widgets and displays it in the navigator.
     */
    public void initWidgets()
    {
        this.setStyle();

        addTab(usersTab);
        addTab(rightsTab);
        addTab(templatesTab);
        TabItem item = addTab(mountPointTab);
        item.addListener(Events.Select, new EventBaseListener(MountPointEvents.TAB_MOUNT_POINT_SELECTED));

        Viewport viewport = new Viewport();
        viewport.setLayout(new FitLayout());
        viewport.add(mainPanel);

        RootPanel.get().add(viewport);
    }

    /**
     * Sets the style attributes, borders etc.
     */
    protected void setStyle()
    {
        mainPanel.setResizeTabs(false);
    }

    /**
     * Adds a widget as a tab to the main interface panel.
     * 
     * @param wid
     *            the widget to add
     */
    private TabItem addTab(SubView wid)
    {
        TabItem item = new TabItem();
        item.setLayout(new FitLayout());
        item.setText(wid.getHeading());
        item.setClosable(false);
        item.add(wid, new FitData());
        mainPanel.add(item);
        return item;
    }

}
