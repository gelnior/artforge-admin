package fr.hd3d.admin.ui.client.model;

import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * Handles the administration global data (which is nothing right now).
 * 
 * @author HD3D
 */
public class AdminMainModel extends MainModel
{

    boolean serverConnectionOk;

    public AdminMainModel()
    {
        super();
    }

    /**
     * Checks whether a connection has been established with the server.
     * 
     * @return true if a connection has been successfully established with the server
     */
    public boolean isConnected()
    {
        return serverConnectionOk;
    }

    /**
     * Assumes that a connection has been successfully established with the server.
     */
    public void setConnected()
    {
        serverConnectionOk = true;
    }

}
