package fr.hd3d.admin.ui.client.users.view;

import java.util.ArrayList;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.HeaderGroupConfig;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.PagingToolBar;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;

import fr.hd3d.admin.ui.client.config.AdminConfig;
import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.factoring.simplegrid.Hd3dSimpleGrid;
import fr.hd3d.admin.ui.client.users.model.AccountsModel;
import fr.hd3d.admin.ui.client.users.model.PersonFilterField;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.admin.ui.client.view.ConfirmationDialog;
import fr.hd3d.admin.ui.client.view.listener.GridSelectionListener;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.grid.BaseGrid;


/**
 * This panel provides a person model data grid and an interface that allows user account deletion, creation and
 * edition.
 * 
 * @author HD3D
 * 
 */
public class AccountsPanel extends BorderedPanel implements IAccountView
{

    /** the user accounts main model */
    private AccountsModel model;
    /** the inner grid */
    // private Hd3dSimpleGrid<PersonModelData> grid;
    private BaseGrid<PersonModelData> grid;

    private ToolBar toolBar = new ToolBar();
    private Button refresh;
    private Button create;
    private Button edit;
    private Button delete;

    /** the account creation dialog */
    private AccountFormDialog createDialog = new CreateAccountDialog(null);
    /** the change password dialog */
    private AccountFormDialog editDialog = new ChangePasswordDialog(null);
    /** the delete account confirmation dialog */
    private ConfirmationDialog deleteDialog = new ConfirmationDialog(AdminMainView.CONSTANTS.DeleteAccountDialog(),
            AdminMainView.MESSAGES.WantToDeleteAccount(), AdminEvents.USER_DELETE_ACCOUNT_CONFIRMED, null);

    /**
     * Creates an account panel for the specified user accounts model.
     * 
     * @param model
     *            the adequate user accounts model
     */
    public AccountsPanel(AccountsModel model)
    {
        super();
        this.model = model;
    }

    public void setup()
    {
        gridSetup();

        setStyle();

        setupToolBar();
        setTopComponent(toolBar);

        grid.addListener(Events.CellClick, new GridSelectionListener<PersonModelData>(AdminEvents.USER_SELECTED));

        ContentPanel container = new ContentPanel();
        container.setHeaderVisible(false);
        container.setBorders(false);
        container.setBodyBorder(false);
        container.setLayout(new FitLayout());

        final PagingToolBar toolBar = new PagingToolBar(AdminConfig.PAGING_LIMIT);
        toolBar.bind(this.model.getLoader());
        toolBar.setStyleAttribute("padding", "5px");

        grid.getStore().sort(PersonModelData.PERSON_LAST_NAME_FIELD, SortDir.DESC);
        grid.getStore().getLoader().setSortField(PersonModelData.PERSON_LAST_NAME_FIELD);
        grid.getStore().getLoader().setSortDir(SortDir.ASC);
        container.add(grid);
        container.setBottomComponent(toolBar);

        addCenter(container);
    }

    public void onUserSelection(boolean canCreate, boolean canUpdate, boolean canDelete)
    {
        PersonModelData person = model.getSelected();
        if (person != null)
        {
            Long status = person.getAccountStatus();
            int realStatus = status < 0 ? Integer.valueOf(String.valueOf(status)) : 0;
            switch (realStatus)
            {
                case AdminConfig.ACCOUNT_MISSING:
                case AdminConfig.ACCOUNT_EXTERNAL:
                    create.enable();
                    edit.disable();
                    delete.disable();
                    break;
                case AdminConfig.ACCOUNT_DEFAULTPASSWORD:
                case AdminConfig.ACCOUNT_NULLPASSWORD:
                case AdminConfig.ACCOUNT_OK:
                    create.disable();
                    edit.enable();
                    delete.enable();
                    break;
                case AdminConfig.ACCOUNT_MULTIPLE:
                    create.disable();
                    edit.disable();
                    delete.enable();
                    break;
            }
            if (!canCreate)
            {
                create.disable();
            }
            if (!canUpdate)
            {
                edit.disable();
            }
            if (!canDelete)
            {
                delete.disable();
            }
        }
    }

    public void onUsersRefresh()
    {
        create.disable();
        edit.disable();
        delete.disable();
    }

    public void onAccountCreationDemand()
    {
        createDialog.setupModel(model.getSelected().getLogin());
        createDialog.setPosition(getAbsoluteLeft() + 60, getAbsoluteTop() + 60);
        createDialog.show();
    }

    public void onAccountDeletionDemand()
    {
        deleteDialog.setHeaderAdding(model.getSelected().getLogin());
        deleteDialog.setPosition(getAbsoluteLeft() + 60, getAbsoluteTop() + 60);
        deleteDialog.show();
    }

    public void onAccountEditionDemand()
    {
        editDialog.setupModel(model.getSelected().getLogin());
        editDialog.setPosition(getAbsoluteLeft() + 60, getAbsoluteTop() + 60);
        editDialog.show();
    }

    /**
     * Initializes the data grid component.
     */
    // protected void gridSetup()
    // {
    // grid = new BaseGrid<PersonModelData>();
    // grid.setShowId(false);
    //
    // grid.addColumn(PersonModelData.PERSON_LAST_NAME_FIELD, AdminMainView.CONSTANTS.LastName(), 100, false);
    // grid.addColumn(PersonModelData.PERSON_FIRST_NAME_FIELD, AdminMainView.CONSTANTS.FirstName(), 100, false);
    // grid.addColumn(PersonModelData.PERSON_LOGIN_FIELD, AdminMainView.CONSTANTS.Login(), 80, false);
    // grid.addColumn(PersonModelData.EMAIL_FIELD, AdminMainView.CONSTANTS.eMail(), 120, false);
    // grid.addRenderer(PersonModelData.EMAIL_FIELD, UserColumnRenderers.getEMailRenderer());
    //
    // grid.addColumn("Account Status", AdminMainView.CONSTANTS.AccountStatus(), 100, true);
    // grid.addRenderer("Account Status", UserColumnRenderers.getAccountStatusRenderer());
    // grid.addColumn("Account Info", AdminMainView.CONSTANTS.LastConnection(), 160, false);
    // grid.addRenderer("Account Info", UserColumnRenderers.getAccountInfoRenderer());
    //
    // grid.setup(model);
    // grid.getColumnModel().addHeaderGroup(0, 0, new HeaderGroupConfig(AdminMainView.CONSTANTS.UserInfos(), 1, 4));
    // grid.getColumnModel().addHeaderGroup(0, 4, new HeaderGroupConfig(AdminMainView.CONSTANTS.AccountInfo(), 1, 2));
    // grid.resetColumns();
    //
    // grid.setHideHeaders(false);
    // grid.setBorders(true);
    // grid.setAutoExpandColumn("AccountInfo");
    // }
    protected void gridSetup()
    {
        grid = new BaseGrid<PersonModelData>(this.model.getStore(), this.getColumnModel());
        // grid.setup(model);
        grid.getColumnModel().addHeaderGroup(0, 0, new HeaderGroupConfig(AdminMainView.CONSTANTS.UserInfos(), 1, 4));
        grid.getColumnModel().addHeaderGroup(0, 4, new HeaderGroupConfig(AdminMainView.CONSTANTS.AccountInfo(), 1, 2));
        // grid.resetColumns();

        grid.setHideHeaders(false);
        grid.setBorders(true);
        grid.getView().setForceFit(true);
    }

    private ColumnModel getColumnModel()
    {
        ArrayList<ColumnConfig> columns = new ArrayList<ColumnConfig>();
        GridUtils.addColumnConfig(columns, PersonModelData.PERSON_LAST_NAME_FIELD, AdminMainView.CONSTANTS.LastName(),
                100, false);
        GridUtils.addColumnConfig(columns, PersonModelData.PERSON_FIRST_NAME_FIELD,
                AdminMainView.CONSTANTS.FirstName(), 100, false);
        GridUtils.addColumnConfig(columns, PersonModelData.PERSON_LOGIN_FIELD, AdminMainView.CONSTANTS.Login(), 80,
                false);
        ColumnConfig emailColumn = GridUtils.addColumnConfig(columns, PersonModelData.EMAIL_FIELD,
                AdminMainView.CONSTANTS.eMail(), 120, false);
        emailColumn.setRenderer(UserColumnRenderers.getEMailRenderer());

        ColumnConfig accountStatusColumn = GridUtils.addColumnConfig(columns, "Account Status", AdminMainView.CONSTANTS
                .AccountStatus(), 100, true);
        accountStatusColumn.setRenderer(UserColumnRenderers.getAccountStatusRenderer());
        ColumnConfig accountInfoColumn = GridUtils.addColumnConfig(columns, "AccountInfo", AdminMainView.CONSTANTS
                .LastConnection(), 160, false);
        accountInfoColumn.setRenderer(UserColumnRenderers.getAccountInfoRenderer());
        return new ColumnModel(columns);
    }

    /**
     * Sets the style attributes such as borders, heading etc.
     */
    protected void setStyle()
    {
        setBodyBorder(false);
        setHeaderVisible(false);
        setHeading(AdminMainView.CONSTANTS.Users());
    }

    /**
     * Builds a proper action tool bar.
     */
    protected void setupToolBar()
    {
        refresh = setupButton(null, AdminMainView.MESSAGES.RefreshUsersTip(), "all-icon",
                AdminEvents.REFRESH_USERS_LIST);
        toolBar.add(refresh);

        toolBar.add(new SeparatorToolItem());

        create = setupButton(null, AdminMainView.MESSAGES.CreateAccountTip(), "create-icon",
                AdminEvents.USER_CREATE_ACCOUNT_CLICKED);
        create.disable();
        toolBar.add(create);

        edit = setupButton(null, AdminMainView.MESSAGES.EditAccountTip(), "edit-icon",
                AdminEvents.USER_EDIT_ACCOUNT_CLICKED);
        edit.disable();
        toolBar.add(edit);

        delete = setupButton(null, AdminMainView.MESSAGES.DeleteAccountTip(), "delete-icon",
                AdminEvents.USER_DELETE_ACCOUNT_CLICKED);
        delete.disable();
        toolBar.add(delete);

        toolBar.setStyleAttribute("padding", "5px");

        toolBar.add(new SeparatorToolItem());
        // FilterServiceStoreField<PersonModelData> personFilter = new FilterServiceStoreField<PersonModelData>(
        // (ServicesPagingStore<PersonModelData>) this.model.getStore(), AdminConfig.PAGING_LIMIT - 1);
        // personFilter.setNameField(PersonModelData.PERSON_FIRST_NAME_FIELD);
        PersonFilterField personFilter = new PersonFilterField(this.model.getStore());
        toolBar.add(personFilter);
    }

    /**
     * Sets a button with proper listener, icon, text...
     * 
     * @return a button with the proper settings
     */
    protected Button setupButton(String text, String toolTip, String iconCSS, EventType eventType)
    {
        Button result = new Button();
        if (text != null)
        {
            result.setText(text);
        }
        if (toolTip != null)
        {
            result.setToolTip(toolTip);
        }
        if (iconCSS != null)
        {
            result.setIconStyle(iconCSS);
        }
        if (eventType != null)
        {
            result.addSelectionListener(new ButtonClickListener(eventType));
        }
        return result;
    }

}
