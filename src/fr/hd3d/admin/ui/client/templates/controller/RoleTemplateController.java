package fr.hd3d.admin.ui.client.templates.controller;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.controller.AbstractRoleController;
import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.templates.model.RoleTemplateModel;
import fr.hd3d.admin.ui.client.templates.view.IRoleTemplateView;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ProjectSecurityTemplateModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;


/**
 * The role controller specification needed to handle security template selection.
 * 
 * @author HD3D
 * 
 */
public abstract class RoleTemplateController<M extends RoleTemplateModel> extends
        AbstractRoleController<M, IRoleTemplateView>
{

    /** whether the role template panel represents the owned role templates or the others */
    private boolean isOwned;

    public RoleTemplateController(M model, IRoleTemplateView view, boolean isOwned)
    {
        super(model, view);
        this.isOwned = isOwned;
    }

    @Override
    protected void doHandleEvent(AppEvent event)
    {
        super.doHandleEvent(event);
        EventType type = event.getType();

        if (type == AdminEvents.TEMPLATE_CREATE_CONFIRMED || type == AdminEvents.TEMPLATE_SELECTED)
        {
            clearConstraints();
            setupConstraints();
            fullyLoaded = false;
            deselectAll();
            refreshStore();
        }
        else if (type == model.refreshAllEvent())
        {
            deselectAll();
            fullyLoaded = false;
            clearConstraints();
            setupConstraints();
            refreshStore();
        }
        else if (type == AdminEvents.TEMPLATE_DELETE_CONFIRMED)
        {
            clearConstraints();
            onConstraintsClearance();
            view.onTemplateDeselection();
        }
    }

    protected void registerEvents()
    {
        super.registerEvents();
        registerEventTypes(AdminEvents.TEMPLATE_CREATE_CONFIRMED);
        registerEventTypes(AdminEvents.TEMPLATE_DELETE_CONFIRMED);
        registerEventTypes(AdminEvents.TEMPLATE_SELECTED);
        registerEventTypes(model.refreshAllEvent());
    }

    protected String path()
    {
        return ServicesPath.getPath("Role Templates");
    }

    protected void onConstraintsClearance()
    {} // for overriding only

    protected void setupConstraints()
    {
        ProjectSecurityTemplateModelData template = model.getSelectedTemplate();
        if (template != null)
        {
            List<Long> roleIds = template.getRoleIds();
            if (roleIds != null && !roleIds.isEmpty())
            {
                Constraint constraint;
                if (isOwned)
                {
                    constraint = new Constraint(EConstraintOperator.in, Hd3dModelData.ID_FIELD, roleIds, null);
                }
                else
                {
                    constraint = new Constraint(EConstraintOperator.notin, Hd3dModelData.ID_FIELD, roleIds, null);
                }
                constraints.add(constraint);
            }
            view.onTemplateSelection(template.getUserCanUpdate());
        }
        else
        {
            view.onTemplateDeselection();
            onConstraintsClearance();
        }
    }

}
