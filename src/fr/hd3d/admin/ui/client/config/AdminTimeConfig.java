package fr.hd3d.admin.ui.client.config;

import com.google.gwt.i18n.client.DateTimeFormat;


/**
 * Contains all the time configs so that it is separated from the usual config, which does not depend on GWT functions.
 * 
 * @author HD3D
 * 
 */
public class AdminTimeConfig
{

    /** Display format for date-time -value. */
    public static DateTimeFormat DATE_TIME_FORMAT = DateTimeFormat.getFormat("dd-MM-yyyy HH:mm");
    /** Display format for date value. */
    public static DateTimeFormat DATE_FORMAT = DateTimeFormat.getFormat("dd-MM-yyyy");
    /** Display format for time value. */
    public static DateTimeFormat TIME_FORMAT = DateTimeFormat.getFormat("HH:mm");
    /** Json format for date value. */
    public static DateTimeFormat DATESTAMP_FORMAT = DateTimeFormat.getFormat("yyyy-MM-dd");

}
