package fr.hd3d.admin.ui.client.perms.view;

import java.util.Arrays;
import java.util.List;

import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FillData;
import com.extjs.gxt.ui.client.widget.layout.FillLayout;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;

import fr.hd3d.admin.ui.client.model.modeldata.GenericModelData;
import fr.hd3d.admin.ui.client.perms.model.PermModelData;
import fr.hd3d.admin.ui.client.perms.model.PermsTreeStoreUtil;
import fr.hd3d.admin.ui.client.view.AdminMainView;


/**
 * A dialog that allows to choose which child nodes to add to an expanding version-type node.
 * 
 * @author HD3D
 * 
 */
public class ManualVersionFilter extends Dialog
{

    /** a pointer for the underlying tree store */
    private static TreeStore<PermModelData> storePointer;
    /** a pointer for the underlying tree node */
    private static PermModelData nodePointer;
    /** the event type to relay upon expansion */
    private static EventType expansionOver;

    /** the class unique instance */
    private static ManualVersionFilter instance;

    // left grid - not to add
    private static ListStore<PermModelData> left = new ListStore<PermModelData>();
    private static Grid<PermModelData> leftGrid;

    // right grid - to add
    private static ListStore<PermModelData> right = new ListStore<PermModelData>();
    private static Grid<PermModelData> rightGrid;

    protected ManualVersionFilter()
    {
        super();

        setup();
    }

    /**
     * Gets the singleton class unique instance.
     * 
     * @return a dialog that allows manual filtering the service URLs components to add to a version-type node
     */
    public static ManualVersionFilter getInstance()
    {
        if (instance == null)
        {
            instance = new ManualVersionFilter();
        }
        return instance;
    }

    private void setup()
    {
        setStyle();
        setHideOnButtonClick(true);

        setLeftGrid();
        setRightGrid();

        this.setButtons(Dialog.OKCANCEL);

        Button cancel = getButtonById(CANCEL);
        cancel.setText(AdminMainView.CONSTANTS.Cancel());
        Button ok = getButtonById(OK);
        ok.addSelectionListener(new Ok());
    }

    private void setStyle()
    {
        setHeading(AdminMainView.CONSTANTS.ExpandNodeDialog());
        setHeaderVisible(true);
        setLayout(new FillLayout(Style.Orientation.HORIZONTAL));

        setHeight(400);
        setWidth(330);
        setModal(true);
    }

    private void setLeftGrid()
    {
        ContentPanel panel = new ContentPanel();
        panel.setLayout(new FitLayout());
        panel.setHeading(AdminMainView.CONSTANTS.ExistingData());

        ColumnConfig label = new ColumnConfig(PermModelData.LABEL, AdminMainView.CONSTANTS.Label(), 150);
        label.setResizable(false);
        leftGrid = new Grid<PermModelData>(left, new ColumnModel(Arrays.asList(label)));
        leftGrid.setAutoExpandColumn(GenericModelData.LABEL_FIELD);
        leftGrid.setAutoExpandMin(150);
        leftGrid.setWidth(150);
        leftGrid.setAutoWidth(false);

        leftGrid.addListener(Events.CellDoubleClick, new LeftListener());
        panel.add(leftGrid, new FitData());
        add(panel, new FillData());
    }

    private void setRightGrid()
    {
        ContentPanel panel = new ContentPanel();
        panel.setLayout(new FitLayout());
        panel.setHeading(AdminMainView.CONSTANTS.DataToAdd());

        ColumnConfig label = new ColumnConfig(PermModelData.LABEL, AdminMainView.CONSTANTS.Label(), 150);
        label.setResizable(false);
        rightGrid = new Grid<PermModelData>(right, new ColumnModel(Arrays.asList(label)));
        rightGrid.setAutoExpandColumn(GenericModelData.LABEL_FIELD);
        rightGrid.setAutoExpandMin(150);
        rightGrid.setWidth(150);
        rightGrid.setAutoWidth(false);

        rightGrid.addListener(Events.CellDoubleClick, new RightListener());
        panel.add(rightGrid, new FitData());
        add(panel, new FillData());
    }

    /**
     * Shows a manual filtering dialog initialized with the specified URL paths. The filtered parts will eventually be
     * added to the version node to expand.
     * 
     * @param store
     *            the server data store
     * @param tree
     *            the requesting tree store
     * @param node
     *            the expanding node
     * @param expansionFinished
     *            the event type to forward in case the full expansion has been performed
     */
    public void show(List<PermModelData> store, TreeStore<PermModelData> tree, PermModelData node,
            EventType expansionFinished)
    {
        storePointer = tree;
        nodePointer = node;
        expansionOver = expansionFinished;

        refreshContent(store);
        show();
    }

    private void refreshContent(List<PermModelData> store)
    {
        left.removeAll();
        right.removeAll();

        left.add(store);

        left.sort(GenericModelData.LABEL_FIELD, SortDir.ASC);
    }

    /**
     * This listener removes selected data object from the left store and adds it to the one on the right hand side.
     * 
     * @author HD3D
     * 
     */
    static class LeftListener implements Listener<GridEvent<PermModelData>>
    {
        public void handleEvent(GridEvent<PermModelData> be)
        {
            PermModelData selected = be.getModel();
            left.remove(selected);
            right.add(selected);
        }
    }

    /**
     * This listener removes selected data object from the right store and adds it to the one on the left hand side.
     * 
     * @author HD3D
     * 
     */
    static class RightListener implements Listener<GridEvent<PermModelData>>
    {
        public void handleEvent(GridEvent<PermModelData> be)
        {
            PermModelData selected = be.getModel();
            right.remove(selected);
            left.add(selected);
        }
    }

    /**
     * When clicking on the 'OK' button, the user orders data in the right hand side store to be transferred to
     * underlying model. It will then add selected data to the node's children if does not have it yet.
     * 
     * @author HD3D
     * 
     */
    static class Ok extends SelectionListener<ButtonEvent>
    {
        public void componentSelected(ButtonEvent ce)
        {
            PermsTreeStoreUtil.appendChosenChildren(storePointer, nodePointer, right.getModels(), expansionOver);
        }
    }

}
