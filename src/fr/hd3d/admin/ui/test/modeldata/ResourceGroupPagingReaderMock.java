package fr.hd3d.admin.ui.test.modeldata;

import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.Hd3dPagingJsonReaderMock;


public class ResourceGroupPagingReaderMock extends Hd3dPagingJsonReaderMock<ResourceGroupModelData>
{

    public ResourceGroupPagingReaderMock()
    {
        super(ResourceGroupModelData.getModelType());
    }

    public ResourceGroupModelData newModelInstance()
    {
        return new ResourceGroupModelData();
    }

}
