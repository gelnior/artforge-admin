package fr.hd3d.admin.ui.test.modeldata;

import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.Hd3dListJsonReaderMock;


public class ResourceGroupReaderMock extends Hd3dListJsonReaderMock<ResourceGroupModelData>
{

    public ResourceGroupReaderMock()
    {
        super();
        setModelType(ResourceGroupModelData.getModelType());
    }

    public ResourceGroupModelData newModelInstance()
    {
        return new ResourceGroupModelData();
    }

}
