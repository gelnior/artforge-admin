package fr.hd3d.admin.ui.client.perms.view;

import java.util.Arrays;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.TreeGridEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.IconHelper;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.HeaderGroupConfig;
import com.extjs.gxt.ui.client.widget.treegrid.TreeGrid;

import fr.hd3d.admin.ui.client.perms.model.PermModelData;
import fr.hd3d.admin.ui.client.perms.model.PermsModel;
import fr.hd3d.admin.ui.client.perms.model.PermsTreeStoreUtil;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * A permission model data tree grid with a predefined column model.
 * 
 * @author HD3D
 * 
 */
public class PermGrid extends TreeGrid<PermModelData>
{

    /** the permission model (containing the permission model data tree store of the grid) */
    private PermsModel model;

    /**
     * Creates and initializes a grid with the provided permission model tree store.
     * 
     * @param model
     *            the store providing permission model
     */
    public PermGrid(PermsModel model)
    {
        super(model.getStore(), getColumns());

        this.model = model;

        addListener(Events.CellClick, new ColumnClickListener());
        addListener(Events.Expand, new NodeExpandListener());
        addListener(Events.BeforeCollapse, new EndFirstExpansion());
        this.setStyle();
    }

    /**
     * Builds all the relevant columns for the tree grid.
     * 
     * @return the tree grid's column model
     */
    private static ColumnModel getColumns()
    {
        ColumnConfig expand = new ColumnConfig(PermModelData.TYPE, "", 30);
        expand.setRenderer(PermsColumnRenderers.getExpandRenderer());
        ColumnConfig name = new ColumnConfig(PermModelData.IDENTIFIER, AdminMainView.CONSTANTS.ServiceTree(), 280);
        name.setRenderer(PermsColumnRenderers.getTypeRenderer());

        ColumnConfig canCreate = new ColumnConfig(PermModelData.CAN_CREATE, AdminMainView.CONSTANTS.CanCreate(), 56);
        canCreate.setRenderer(PermsColumnRenderers.getPermRenderer());
        ColumnConfig canRead = new ColumnConfig(PermModelData.CAN_READ, AdminMainView.CONSTANTS.CanRead(), 56);
        canRead.setRenderer(PermsColumnRenderers.getPermRenderer());
        ColumnConfig canUpdate = new ColumnConfig(PermModelData.CAN_UPDATE, AdminMainView.CONSTANTS.CanUpdate(), 56);
        canUpdate.setRenderer(PermsColumnRenderers.getPermRenderer());
        ColumnConfig canDelete = new ColumnConfig(PermModelData.CAN_DELETE, AdminMainView.CONSTANTS.CanDelete(), 56);
        canDelete.setRenderer(PermsColumnRenderers.getPermRenderer());
        ColumnConfig canAll = new ColumnConfig(PermModelData.CAN_ALL, AdminMainView.CONSTANTS.AllButton(), 56);
        canAll.setRenderer(PermsColumnRenderers.getPermRenderer());

        ColumnModel cModel = new ColumnModel(Arrays.asList(expand, name, canCreate, canRead, canUpdate, canDelete,
                canAll));
        cModel.addHeaderGroup(0, 2, new HeaderGroupConfig(AdminMainView.CONSTANTS.Permissions(), 1, 5));

        return cModel;
    }

    /**
     * Sets grid heading, borders, column expansion etc.
     */
    private void setStyle()
    {
        setAutoExpandColumn(PermModelData.IDENTIFIER);
        setAutoExpandMin(250);
        setAutoWidth(true);

        setHideHeaders(false);
        setLoadMask(true);
        setBorders(false);

        getStyle().setLeafIcon(IconHelper.createStyle(""));
        // getStyle().setNodeCloseIcon(IconHelper.createStyle(""));
        // getStyle().setNodeOpenIcon(IconHelper.createStyle(""));
    }

    /**
     * A listener specifically designed for deciding of which event to fire depending on what column the user has
     * clicked on.
     * 
     * @author HD3D
     * 
     */
    class ColumnClickListener implements Listener<TreeGridEvent<PermModelData>>
    {
        public void handleEvent(TreeGridEvent<PermModelData> be)
        {
            model.expansionFinishedEvent();

            int col = be.getColIndex();
            PermModelData perm = be.getModel();
            EventType relay;
            switch (col)
            {
                case 0:
                    if (perm.expanded)
                    {
                        relay = model.collapseNodeEvent();
                    }
                    else
                    {
                        relay = model.expandNodeEvent();
                    }
                    break;
                case 2:
                    relay = model.canCreateClickedEvent();
                    break;
                case 3:
                    relay = model.canReadClickedEvent();
                    break;
                case 4:
                    relay = model.canUpdateClickedEvent();
                    break;
                case 5:
                    relay = model.canDeleteClickedEvent();
                    break;
                case 6:
                    relay = model.canAllClickedEvent();
                    break;
                default:
                    relay = null;
            }
            if (perm != null && relay != null)
            {
                AppEvent event = new AppEvent(relay);
                event.setData(perm);
                EventDispatcher.forwardEvent(event);
            }
        }
    }

    /**
     * Orders the grid to update the ID child nodes label on first expansion.
     * 
     * @author HD3D
     * 
     */
    class NodeExpandListener implements Listener<TreeGridEvent<PermModelData>>
    {
        public void handleEvent(TreeGridEvent<PermModelData> be)
        {
            if (model.isFirstExpansion())
            {
                PermModelData node = be.getModel(); // the node that will be expanded
                if (PermsTreeStoreUtil.mustBeUpdated(model.getStore(), node))
                {
                    model.requestData(node, true); // order data update
                }
            }
        }
    }

    /**
     * Brings the first expansion's listener influence to an end.
     * 
     * @author HD3D
     * 
     */
    class EndFirstExpansion implements Listener<TreeGridEvent<PermModelData>>
    {
        public void handleEvent(TreeGridEvent<PermModelData> be)
        {
            if (model.isFirstExpansion())
            {
                model.firstExpansionOver();
            }
        }
    }

}
