package fr.hd3d.admin.ui.client.perms.view;

import java.util.Arrays;
import java.util.List;

import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FillData;
import com.extjs.gxt.ui.client.widget.layout.FillLayout;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;

import fr.hd3d.admin.ui.client.model.modeldata.GenericModelData;
import fr.hd3d.admin.ui.client.perms.model.PermModelData;
import fr.hd3d.admin.ui.client.perms.model.PermsModel;
import fr.hd3d.admin.ui.client.view.AdminMainView;


/**
 * A singleton dialog that allows filtering of incoming server data object instances, so that an user can choose which
 * to add to permission trees.
 * 
 * @author HD3D
 * 
 */
public class ManualFilterDialog extends Dialog
{

    /** a pointer for the underlying model */
    private static PermsModel modelPointer;
    /** a pointer for the underlying node */
    private static PermModelData nodePointer;

    /** the class unique instance */
    private static ManualFilterDialog instance;

    // left grid - not to add
    private static ListStore<GenericModelData> left = new ListStore<GenericModelData>();
    private static Grid<GenericModelData> leftGrid;

    // right grid - to add
    private static ListStore<GenericModelData> right = new ListStore<GenericModelData>();
    private static Grid<GenericModelData> rightGrid;

    protected ManualFilterDialog()
    {
        super();

        setup();
    }

    /**
     * Gets the singleton class unique instance.
     * 
     * @return a dialog that allows manual filtering of incoming server data
     */
    public static ManualFilterDialog getInstance()
    {
        if (instance == null)
        {
            instance = new ManualFilterDialog();
        }
        return instance;
    }

    private void setup()
    {
        setStyle();
        setHideOnButtonClick(true);

        setLeftGrid();
        setRightGrid();

        this.setButtons(Dialog.OKCANCEL);

        Button cancel = getButtonById(CANCEL);
        cancel.setText(AdminMainView.CONSTANTS.Cancel());
        Button ok = getButtonById(OK);
        ok.addSelectionListener(new Ok());
    }

    private void setStyle()
    {
        setHeading(AdminMainView.CONSTANTS.ExpandNodeDialog());
        setHeaderVisible(true);
        setLayout(new FillLayout(Style.Orientation.HORIZONTAL));

        setHeight(400);
        setWidth(440);
        setModal(true);
    }

    private void setLeftGrid()
    {
        ContentPanel panel = new ContentPanel();
        panel.setLayout(new FitLayout());
        panel.setHeading(AdminMainView.CONSTANTS.ExistingData());

        ColumnConfig id = new ColumnConfig(GenericModelData.ID_FIELD, "Id", 60);
        id.setResizable(false);
        ColumnConfig name = new ColumnConfig("label", AdminMainView.CONSTANTS.Label(), 140);
        name.setResizable(false);
        name.setRenderer(PermsColumnRenderers.getShortTypeRenderer());
        leftGrid = new Grid<GenericModelData>(left, new ColumnModel(Arrays.asList(id, name)));
        leftGrid.setAutoExpandColumn("label");
        leftGrid.setAutoExpandMin(140);
        leftGrid.setWidth(200);
        leftGrid.setAutoWidth(false);

        leftGrid.addListener(Events.CellDoubleClick, new LeftListener());
        panel.add(leftGrid, new FitData());
        add(panel, new FillData());
    }

    private void setRightGrid()
    {
        ContentPanel panel = new ContentPanel();
        panel.setLayout(new FitLayout());
        panel.setHeading(AdminMainView.CONSTANTS.DataToAdd());

        ColumnConfig id = new ColumnConfig(GenericModelData.ID_FIELD, "Id", 60);
        id.setResizable(false);
        ColumnConfig name = new ColumnConfig(GenericModelData.LABEL_FIELD, AdminMainView.CONSTANTS.Label(), 140);
        name.setResizable(false);
        name.setRenderer(PermsColumnRenderers.getShortTypeRenderer());
        rightGrid = new Grid<GenericModelData>(right, new ColumnModel(Arrays.asList(id, name)));
        rightGrid.setAutoExpandColumn(GenericModelData.LABEL_FIELD);
        rightGrid.setAutoExpandMin(140);
        rightGrid.setWidth(200);
        rightGrid.setAutoWidth(false);

        rightGrid.addListener(Events.CellDoubleClick, new RightListener());
        panel.add(rightGrid, new FitData());
        add(panel, new FillData());
    }

    /**
     * Shows a manual filtering dialog initialized with the specified server data store. The filtered data will
     * eventually be added to the node to expand.
     * 
     * @param store
     *            the server data store
     * @param model
     *            the requesting model
     * @param node
     *            the expanding node
     */
    public void show(List<GenericModelData> store, PermsModel model, PermModelData node)
    {
        modelPointer = model;
        nodePointer = node;

        refreshContent(store);

        show();
    }

    private void refreshContent(List<GenericModelData> store)
    {
        left.removeAll();
        right.removeAll();

        List<PermModelData> children = modelPointer.getStore().getChildren(nodePointer);
        if (children != null && !children.isEmpty())
        {
            boolean has;
            for (GenericModelData data : store)
            {
                has = false;
                for (PermModelData child : children)
                {
                    if (child.getLabel().equals(data.getLabel()))
                    {
                        right.add(data);
                        has = true;
                        break;
                    }
                }
                if (!has)
                {
                    left.add(data);
                }
            }
        }
        else
        {
            left.add(store);
        }

        left.sort(GenericModelData.LABEL_FIELD, SortDir.ASC);
    }

    /**
     * This listener removes selected data object from the left store and adds it to the one on the right hand side.
     * 
     * @author HD3D
     * 
     */
    static class LeftListener implements Listener<GridEvent<GenericModelData>>
    {
        public void handleEvent(GridEvent<GenericModelData> be)
        {
            GenericModelData selected = be.getModel();
            left.remove(selected);
            right.add(selected);
        }
    }

    /**
     * This listener removes selected data object from the right store and adds it to the one on the left hand side.
     * 
     * @author HD3D
     * 
     */
    static class RightListener implements Listener<GridEvent<GenericModelData>>
    {
        public void handleEvent(GridEvent<GenericModelData> be)
        {
            GenericModelData selected = be.getModel();
            right.remove(selected);
            left.add(selected);
        }
    }

    /**
     * When clicking on the 'OK' button, the user orders data in the right hand side store to be transferred to
     * underlying model. It will then add selected data to the node's children if does not have it yet.
     * 
     * @author HD3D
     * 
     */
    static class Ok extends SelectionListener<ButtonEvent>
    {
        public void componentSelected(ButtonEvent ce)
        {
            modelPointer.appendData(nodePointer, right.getModels());
        }
    }

}
