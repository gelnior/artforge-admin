package fr.hd3d.admin.ui.test.rights;

import fr.hd3d.admin.ui.client.perms.view.IPermsView;
import fr.hd3d.admin.ui.client.rights.model.RightsPermsModel;
import fr.hd3d.admin.ui.test.perms.PermsViewMock;


public class RightsPermsViewMock extends PermsViewMock implements IPermsView
{

    public RightsPermsViewMock(RightsPermsModel model)
    {
        super(model);
    }

}
