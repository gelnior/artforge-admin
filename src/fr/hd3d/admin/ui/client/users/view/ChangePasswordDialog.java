package fr.hd3d.admin.ui.client.users.view;

import com.extjs.gxt.ui.client.binding.FieldBinding;
import com.extjs.gxt.ui.client.widget.form.TextField;

import fr.hd3d.admin.ui.client.users.model.AccountFormModel;
import fr.hd3d.admin.ui.client.view.AdminMainView;


/**
 * A dialog showed on 'change password' request that will allow an user account password modification. The former
 * password can be optionally specified in case the caller would not be granted the 'super admin' privileges.
 * 
 * @author HD3D
 * 
 */
public class ChangePasswordDialog extends AccountFormDialog
{

    private TextField<String> oldPass;

    public ChangePasswordDialog(String username)
    {
        super(AdminMainView.CONSTANTS.ChangePasswordDialog(), username, false);
    }

    public void show()
    {
        oldPass.clearInvalid();
        oldPass.setValue("");
        super.show();
    }

    protected void setFormFields()
    {
        oldPass = new TextField<String>();
        oldPass.setFieldLabel(AdminMainView.CONSTANTS.OldPassword());
        oldPass.setPassword(true);
        oldPass.setAllowBlank(true);
        fieldSet.add(oldPass, formData);

        subFieldSet.setHeading(AdminMainView.CONSTANTS.EnterNewPassword());
        subFieldSet.setExpanded(true);

        super.setFormFields();

        password.setAllowBlank(false);
        repeat.setAllowBlank(false);
    }

    protected void setFieldBinding()
    {
        passwordBinding.addFieldBinding(new FieldBinding(oldPass, AccountFormModel.PASSWORD_ATTRIBUTE));
        passwordBinding.addFieldBinding(new FieldBinding(password, AccountFormModel.NEWPASS_ATTRIBUTE));

        super.setFieldBinding();
    }

}
