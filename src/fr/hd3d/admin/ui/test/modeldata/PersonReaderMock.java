package fr.hd3d.admin.ui.test.modeldata;

import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.Hd3dPagingJsonReaderMock;


public class PersonReaderMock extends Hd3dPagingJsonReaderMock<PersonModelData>
{

    public PersonReaderMock()
    {
        super(PersonModelData.getModelType());
    }

    public PersonModelData newModelInstance()
    {
        return new PersonModelData();
    }

}
