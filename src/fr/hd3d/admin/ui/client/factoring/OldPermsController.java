package fr.hd3d.admin.ui.client.factoring;

import java.util.LinkedList;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.NameModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.undo.CommandManager;


@Deprecated
public abstract class OldPermsController extends Controller
{

    protected OldPermsModel model;
    protected IOldPermsView view;

    protected final CommandManager manager;
    protected boolean unsavedChanges = false;

    private List<EventType> roleSelectionEvents;
    private List<EventType> roleDeselectionEvents;
    private List<EventType> roleModificationEvents;

    public OldPermsController(OldPermsModel model, IOldPermsView view)
    {
        this.model = model;
        this.view = view;

        manager = new CommandManager();

        buildUpRoleEvents();
        registerEvents();
    }

    public void handleEvent(AppEvent event)
    {
        this.forwardToChild(event);
        EventType type = event.getType();
        if (type == CommonEvents.PERMISSION_INITIALIZED)
        {
            this.onPermissionInitialized(event);
        }
        else if (roleSelectionEvents.contains(type))
        {
            if (unsavedChanges)
            {
                onPrematuredSelectionChange(event);
            }
            else
            {
                RoleModelData selectedRole = event.<RoleModelData> getData();
                model.setRole(selectedRole);
                onRoleSelection();
            }
        }
        else if (roleDeselectionEvents.contains(type))
        {
            if (unsavedChanges)
            {
                onPrematuredSelectionChange(event);
            }
            else
            {
                model.setRole(null);
                onRoleSelection();
            }
        }
        else if (roleModificationEvents.contains(type))
        {
            if (unsavedChanges)
            {
                onPrematuredSelectionChange(event);
            }
            else
            {
                onRoleSelection();
            }
        }
        else if (type == model.addPermissionDemandEvent())
        {
            view.invokeRightCreator(false);
        }
        else if (type == model.addPermissionEvent())
        {
            NameModelData right = event.<NameModelData> getData();
            onPermissionAdding(right);
        }
        else if (type == model.addBanDemandEvent())
        {
            view.invokeRightCreator(true);
        }
        else if (type == model.addBanEvent())
        {
            NameModelData right = event.<NameModelData> getData();
            onBanAdding(right);
        }
        else if (type == model.removeBanEvent())
        {
            NameModelData right = event.<NameModelData> getData();
            onBanRemoval(right);
        }
        else if (type == model.removePermissionEvent())
        {
            NameModelData right = event.<NameModelData> getData();
            onPermissionRemoval(right);
        }
        else if (type == model.saveEvent())
        {
            RoleModelData roleToSave = event.<RoleModelData> getData();
            onSave(roleToSave);
        }
        else if (type == model.fullUndoEvent())
        {
            onFullUndo();
        }
        else if (type == model.undoEvent())
        {
            onUndo();
        }
        else if (type == model.redoEvent())
        {
            onRedo();
        }
    }

    protected void registerEvents()
    {
        for (EventType type : roleSelectionEvents)
        {
            registerEventTypes(type);
        }
        for (EventType type : roleDeselectionEvents)
        {
            registerEventTypes(type);
        }
        for (EventType type : roleModificationEvents)
        {
            registerEventTypes(type);
        }
        registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
        registerEventTypes(model.addPermissionDemandEvent());
        registerEventTypes(model.addPermissionEvent());
        registerEventTypes(model.addBanDemandEvent());
        registerEventTypes(model.addBanEvent());
        registerEventTypes(model.removePermissionEvent());
        registerEventTypes(model.removeBanEvent());
        registerEventTypes(model.saveEvent());
        registerEventTypes(model.undoEvent());
        registerEventTypes(model.redoEvent());
        registerEventTypes(model.fullUndoEvent());
    }

    /**
     * Initializes all the role related events.
     */
    protected void buildUpRoleEvents()
    {
        roleSelectionEvents = defineRoleSelectionEvents();
        if (roleSelectionEvents == null)
        {
            roleSelectionEvents = new LinkedList<EventType>();
        }
        roleDeselectionEvents = defineRoleDeselectionEvents();
        if (roleDeselectionEvents == null)
        {
            roleDeselectionEvents = new LinkedList<EventType>();
        }
        roleModificationEvents = defineRoleModificationEvents();
        if (roleModificationEvents == null)
        {
            roleModificationEvents = new LinkedList<EventType>();
        }
    }

    /**
     * Defines the events that imply a role selection.
     * 
     * @return the list of the relevant event types
     */
    protected abstract List<EventType> defineRoleSelectionEvents();

    /**
     * Defines the events that imply a role selection clearance (deletion, etc.).
     * 
     * @return the list of the relevant event types
     */
    protected abstract List<EventType> defineRoleDeselectionEvents();

    /**
     * Defines the events that imply a role modification on the server.
     * 
     * @return the list of the relevant event types
     */
    protected abstract List<EventType> defineRoleModificationEvents();

    protected void onPermissionInitialized(AppEvent event)
    {
        model.refreshStore();
        view.setup();
    }

    protected void onPrematuredSelectionChange(AppEvent delayedEvent)
    {
        view.showQuitAndSaveDialog(delayedEvent);
    }

    protected void onRoleSelection()
    {
        clearCommandManager();

        model.refreshStore();
        view.onRoleSelection();
    }

    protected void onPermissionAdding(NameModelData right)
    {
        beforeUndoableAction();
        manager.executeCommand(OldPermsCommands.addPermissionCommand(model, right));
        afterUndoableAction();
    }

    protected void onBanAdding(NameModelData right)
    {
        beforeUndoableAction();
        manager.executeCommand(OldPermsCommands.addBanCommand(model, right));
        afterUndoableAction();
    }

    protected void onBanRemoval(NameModelData right)
    {
        beforeUndoableAction();
        manager.executeCommand(OldPermsCommands.removeBanCommand(model, right));
        afterUndoableAction();
    }

    protected void onPermissionRemoval(NameModelData right)
    {
        beforeUndoableAction();
        manager.executeCommand(OldPermsCommands.removePermissionCommand(model, right));
        afterUndoableAction();
    }

    protected void onSave(RoleModelData role)
    {
        unsavedChanges = false;
        clearCommandManager();
        model.saveChanges(role);
    }

    protected void onFullUndo()
    {
        unsavedChanges = false;
        while (!manager.getUndoCommandStack().isEmpty())
        {
            manager.undo();
        }
        clearCommandManager();
    }

    protected void onUndo()
    {
        manager.undo();
        if (manager.getUndoCommandStack().isEmpty())
        {
            view.onEmptyUndoStack();
            unsavedChanges = false;
        }
        view.onUndo();
        view.showGrid();
    }

    protected void onRedo()
    {
        manager.redo();
        if (manager.getRedoCommandStack().isEmpty())
        {
            view.onEmptyRedoStack();
        }
        afterUndoableAction();
    }

    protected void beforeUndoableAction()
    {
        if (!manager.getRedoCommandStack().isEmpty())
        {
            manager.getRedoCommandStack().clear();
            view.onEmptyRedoStack();
        }
    }

    protected void afterUndoableAction()
    {
        view.onUndoableAction();
        view.showGrid();
        unsavedChanges = true;
    }

    protected void clearCommandManager()
    {
        manager.clearStacks();
        view.onEmptyRedoStack();
        view.onEmptyUndoStack();
    }

}
