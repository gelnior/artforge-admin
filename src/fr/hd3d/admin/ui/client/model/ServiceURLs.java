package fr.hd3d.admin.ui.client.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.restlet.client.data.Method;

import com.google.gwt.core.client.GWT;

import fr.hd3d.admin.ui.client.config.AdminConfig;
import fr.hd3d.admin.ui.client.model.callback.ServiceURLsCallBack;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;


/**
 * A tree that will contain all the service URL patterns. It is initialized upon construction and the initialization is
 * performed by the static {@link #init(ServiceURLs)} method defined below.
 * 
 * @author HD3D
 * 
 */
public class ServiceURLs
{

    /** an object node type */
    public final static int OBJECT_NODE = 100;
    /** an id node type */
    public final static int ID_NODE = 101;
    /** unknown node type */
    public final static int OTHER_NODE = 102;

    /** the method mark */
    public final static String OP_MARK = "{op}";
    /** the instance id mark */
    public final static String ID_MARK = "{id}";

    /* a list of URL parts which do not represent an object class */
    private static Set<String> exceptions;
    /* a list of URL parts which are not to shown in the tree */
    private static Set<String> hidden;
    /* a map of resource path correspondence */
    private static Map<String, String> bounce;

    private static ServiceURLs singleton;

    /* for nodes */
    private static int id_count = 0;

    /* the list of the nodes, indexed by their path */
    private Map<String, Node> nodes;
    /* the correspondence between nodes and child nodes */
    private Map<Node, Set<Node>> children;
    /* the root nodes list */
    private Set<Node> roots;

    private ServiceURLs()
    {
        nodes = new HashMap<String, Node>();
        children = new HashMap<Node, Set<Node>>();
        roots = new HashSet<Node>();

        /* initializes according to services */
        init(this);
    }

    /**
     * Gets the singleton instance of the class. If the instance is null, creates and initializes a new service URLs
     * tree.
     * 
     * @return a service URLs tree
     */
    public static ServiceURLs getInstance()
    {
        if (singleton == null)
        {
            singleton = new ServiceURLs();
        }
        return singleton;
    }

    /**
     * Checks whether the permission model data node with specified name is an reported exception, that is, do not
     * represent an object class.
     * 
     * @param nodeName
     *            a node name
     * @return true if the node with the specified name do not represent an object class
     */
    public boolean isException(String nodeName)
    {
        return exceptions.contains(nodeName);
    }

    /**
     * If the specified path is to be redirected, returns the actual server path, else returns the path parameter
     * itself. It actually replaces last URL segment if it has to be redirected.
     * 
     * @param path
     *            the presumed server resource path
     * @return the actual server path
     */
    public String redirect(String path)
    {
        String[] parts = path.split("/");
        String last = parts[parts.length - 1]; // select last URL segment
        if (bounce.containsKey(last))
        {
            path = path.replace(last, bounce.get(last));
        }
        return path;
    }

    /**
     * The service URLs tree basic node class. Each node consists mainly in a node type and a name string. From the
     * tree's global point of view, nodes are found with aid of a designating path. The path is a string built by
     * appending all the parent node's names in descendant order till the designated node is met. In practical terms,
     * appended node names are separated with a column in order to match a permission string prefix. For example, the
     * path towards a node 'john' will be 'gaga:doe:john' if 'doe' and 'gaga' are 'john' 's parents in respective
     * ascendant order.
     * 
     * @author HD3D
     * 
     */
    class Node
    {
        private String name;
        private int type;
        private int id;

        public Node(String name, int type)
        {
            this.name = name;
            this.type = type;
            id = id_count++;
        }

        public int hashCode()
        {
            final int prime = 31;
            int result = 1;
            result = prime * result + id;
            return result;
        }

        public boolean equals(Object obj)
        {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Node other = (Node) obj;
            if (id != other.id)
                return false;
            return true;
        }

        public String toString()
        {
            return name;
        }
    }

    /**
     * Checks whether the tree node reached by the specified path has a child node with a given name.
     * 
     * @param path
     *            a permission string prefix
     * @param name
     *            the node name to look for
     * @return true if the tree contains a node with the specified name right under the specified path
     */
    public boolean hasChild(String path, String name)
    {
        if (name != null)
        {
            Node parent = getNode(path);
            if (parent != null)
            {
                Set<Node> infants = children.get(parent);
                if (infants != null && !infants.isEmpty())
                {
                    for (Node child : infants)
                    {
                        if (name.equals(child.name))
                        {
                            return true;
                        }
                    }
                }
            }
            else
            {
                for (Node node : roots)
                {
                    if (name.equals(node.name))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Checks whether the tree node reached by the specified path has a node of the ID type.
     * 
     * @param path
     *            a permission string prefix
     * @return true if the node at the specified path has an ID child node
     */
    public boolean hasIdChild(String path)
    {
        Node parent = getNode(path);
        Set<Node> infants = children.get(parent);
        if (infants != null && !infants.isEmpty())
        {
            for (Node child : infants)
            {
                if (child.type == ID_NODE)
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Gets the list of all the tree's root items.
     * 
     * @return a set containing all root node's names
     */
    public List<String> getRoots()
    {
        List<String> fold = new ArrayList<String>();
        for (Node node : roots)
        {
            fold.add(node.name);
        }
        return fold;
    }

    /**
     * Gets the child nodes list of the node reached by the specified path.
     * 
     * @param path
     *            a permission string prefix
     * @return the list of the child nodes' names
     */
    public List<String> getFold(String path)
    {
        List<String> fold = new ArrayList<String>();
        Node parent = getNode(path);
        Set<Node> infants = children.get(parent);
        if (infants != null && !infants.isEmpty())
        {
            for (Node child : infants)
            {
                if (child.type == ID_NODE)
                {
                    fold.add(ID_MARK);
                }
                else
                {
                    fold.add(child.name);
                }
            }
        }
        return fold;
    }

    /**
     * Gets the name of the node reached by the specified path
     * 
     * @param path
     *            a permission string prefix
     * @return the node's name
     */
    public String getNodeName(String path)
    {
        return getNode(path).name;
    }

    /**
     * Gets the type of the node reached by the specified path
     * 
     * @param path
     *            a permission string prefix
     * @return the node's type
     */
    public int getNodeType(String path)
    {
        return getNode(path).type;
    }

    private Node getNode(String path)
    {
        String[] parts = path.split(":");
        if (parts.length > 1)
        {
            String path_template = parts[1]; // cuts the version in incoming
            // path
            for (int k = 2; k < parts.length; k++)
            {
                if (nodes.get(path_template + ":" + parts[k]) == null)
                {
                    path_template += ":" + ID_MARK;
                }
                else
                {
                    path_template += ":" + parts[k];
                }
            }
            return nodes.get(path_template);
        }
        return null;
    }

    private String add(Node node)
    {
        String path = node.name;
        roots.add(node);
        nodes.put(path, node);
        return path;
    }

    private String add(String path, Node node)
    {
        String childPath = path + ":" + node.name;
        Node parent = nodes.get(path);
        nodes.put(childPath, node);
        Set<Node> infants = children.get(parent);
        if (infants == null)
        {
            infants = new HashSet<Node>();
        }
        infants.add(node);
        children.put(parent, infants);
        return childPath;
    }

    /**
     * Initializes the tree according to the provided list of URL patterns.
     * 
     * @param patterns
     *            the list of service URL patterns
     */
    public void init(List<List<String>> patterns)
    {
        String path;
        int type;
        boolean isDataObject;
        for (List<String> pattern : patterns)
        {
            /*
             * A pattern = [<isDataObject>, <version>,...,..,<method>]. Example of pattern: [DataObject, v1, ..., ...,
             * read]
             */
            /*
             * if pattern contains only 3 fields (<isDataObject>, <version> and <method>) no need to process
             */
            if (pattern.size() < 4)
                continue;

            /* <isDataObject> field */
            isDataObject = AdminConfig.ISDATAOBJECT.equals(pattern.remove(0));
            /* remove <version> field */
            pattern.remove(0);
            /* remove '{op}' marker at the end */
            pattern.remove(OP_MARK);

            path = null;

            for (String piece : pattern)
            {
                if (ID_MARK.equals(piece))
                {
                    type = ID_NODE;
                }
                else if (!hidden.contains(piece))
                {
                    type = OBJECT_NODE;
                    if (!isDataObject && pattern.get(pattern.size() - 1).equals(piece))
                    {
                        exceptions.add(piece);
                    }
                }
                else
                {
                    break; // no need to add the node
                }
                if (path == null) // first piece will be root
                {
                    if (nodes.get(piece) == null)
                    {
                        path = add(new Node(piece, type));
                    }
                    else
                    {
                        path = piece;
                    }
                }
                else
                {
                    if (nodes.get(path + ":" + piece) == null)
                    {
                        path = add(path, new Node(piece, type));
                    }
                    else
                    {
                        path += ":" + piece;
                    }
                }
            }
        }
        // print();
    }

    /**
     * Initializes the tree according to the services configuration, and launches exceptional and hidden nodes record
     * initialization.
     * 
     * @param tree
     *            the tree to initialize
     */
    private static void init(ServiceURLs tree)
    {
        initHiddenNodes();
        initExceptionalNodes();
        initBounces();

        IRestRequestHandler handler = RestRequestHandlerSingleton.getInstance();
        handler.handleRequest(Method.GET, AdminConfig.SERVICESURLS_PATH, null, new ServiceURLsCallBack(tree));
    }

    /* Define all URLs that do not design an object class */
    private static void initExceptionalNodes()
    {
        exceptions = new TreeSet<String>();

        exceptions.add("search_constituents");
        exceptions.add("daysAndActivities");
        exceptions.add("filteredTaskGroupTasks");
        exceptions.add("plannedTasks");
        exceptions.add("duplicatePlanningMaster");
    }

    /* Defines all the URLs that will not be shown */
    private static void initHiddenNodes()
    {
        hidden = new TreeSet<String>();

        hidden.add("whois");
        hidden.add("cleanup");
        hidden.add("free-query");
        hidden.add("ping");
        hidden.add("services-urls");
        hidden.add("asset-revisions");
        hidden.add("Work-objects-range");
        hidden.add("version");

        /* temporary: test features */
        hidden.add("testupload");
        hidden.add("add-db-reuse-bank");
        hidden.add("icesdownload");
        hidden.add("ices");
        hidden.add("init-db-inventory");
        hidden.add("init-db");
        hidden.add("init-db-bunch-files");
        hidden.add("init-db-with-pasta");
        hidden.add("erase-db");
        hidden.add("testices");
    }

    /* Defines redirections for certain paths */
    private static void initBounces()
    {
        bounce = new HashMap<String, String>();

        // sets redirections for several resources
        bounce.put("apply-template", "templates");
        bounce.put("duplicate-planning-master", "plannings");
    }

    /* debug */
    public void print()
    {
        String result = "nodes" + '\n';
        Node node;
        for (String path : nodes.keySet())
        {
            node = nodes.get(path);
            result += path + " " + node.name + " " + children.get(node) + "\n";
        }
        result += '\n' + "ROOTS" + '\n';
        for (Node n : roots)
        {
            result += n.name + '\n';
        }
        GWT.log("services URLs", new Throwable(result));
    }

}
