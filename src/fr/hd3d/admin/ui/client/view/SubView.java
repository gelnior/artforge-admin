package fr.hd3d.admin.ui.client.view;

import fr.hd3d.common.ui.client.widget.BorderedPanel;


/**
 * This is the class to extend in order to be added to the main application panel.
 * 
 * @author HD3D
 * 
 */
public abstract class SubView extends BorderedPanel implements ISubView
{

    public void initWidgets()
    {
        setStyle();
    }

    /**
     * Sets titles, sizes, borders and layouts...
     */
    protected void setStyle()
    {
        setBodyBorder(false);
        setBorders(false);
        setHeaderVisible(false);
    }

}
