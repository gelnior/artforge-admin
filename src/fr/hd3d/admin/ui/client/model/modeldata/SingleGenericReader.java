package fr.hd3d.admin.ui.client.model.modeldata;

import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.ListLoadResult;

import fr.hd3d.common.ui.client.modeldata.reader.IPagingReader;


/**
 * This singleton class provides the application with the adequate Generic reader, paging or not.
 * 
 * @author HD3D
 * 
 */
public abstract class SingleGenericReader
{

    private static DataReader<ListLoadResult<GenericModelData>> lReader;
    private static IPagingReader<GenericModelData> pReader;

    /**
     * Call in order to get a Generic paging reader.
     * 
     * @return the adequate Generic paging reader
     */
    public static IPagingReader<GenericModelData> getPagingReader()
    {
        if (pReader == null)
        {
            pReader = new GenericPagingReader();
        }
        return pReader;
    }

    /**
     * Call in order to get a Generic reader.
     * 
     * @return the adequate Generic reader
     */
    public static DataReader<ListLoadResult<GenericModelData>> getReader()
    {
        if (lReader == null)
        {
            lReader = new GenericReader();
        }
        return lReader;
    }

    /**
     * Sets the inner instance to the specified parameter.
     * 
     * @param reader
     *            the new reader instance
     */
    public static void setInstance(DataReader<ListLoadResult<GenericModelData>> reader)
    {
        if (reader != null)
        {
            lReader = reader;
        }
    }

    /**
     * Sets the inner instance to the specified parameter.
     * 
     * @param reader
     *            the new paging reader instance
     */
    public static void setInstance(IPagingReader<GenericModelData> reader)
    {
        if (reader != null)
        {
            pReader = reader;
        }
    }

}
