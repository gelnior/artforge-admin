package fr.hd3d.admin.ui.client;

import com.google.gwt.core.client.EntryPoint;

import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;


/**
 * Entry point classes for the Admin application. It sets up the main view, and launch START event.
 */
public class AdministrationInterface implements EntryPoint
{
    /**
     * This is the entry point method.
     */
    public void onModuleLoad()
    {
        // PermissionUtil.setDebugOn();

        AdminMainView admin = new AdminMainView();
        admin.init();
        EventDispatcher.forwardEvent(CommonEvents.START);
    }
}
