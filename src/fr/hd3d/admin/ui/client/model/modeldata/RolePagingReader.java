package fr.hd3d.admin.ui.client.model.modeldata;

import fr.hd3d.common.ui.client.modeldata.reader.Hd3dPagingJsonReader;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;


public class RolePagingReader extends Hd3dPagingJsonReader<RoleModelData>
{

    public RolePagingReader()
    {
        super(RoleModelData.getModelType());
    }

    public RoleModelData newModelInstance()
    {
        return new RoleModelData();
    }

}
