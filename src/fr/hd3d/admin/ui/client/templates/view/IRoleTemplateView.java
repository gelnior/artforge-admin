package fr.hd3d.admin.ui.client.templates.view;

import fr.hd3d.admin.ui.client.view.IAbstractRoleView;


/**
 * A specification of the role view that can adapt to scurity template selection.
 * 
 * @author HD3D
 * 
 */
public interface IRoleTemplateView extends IAbstractRoleView
{

    /**
     * Performs enabling/disabling operations upon security template selection.
     * 
     * @param canUpdate
     *            whether the user can update selected template or not
     */
    public void onTemplateSelection(boolean canUpdate);

    /**
     * Performs enabling/disabling operations upon security template selection clearance.
     */
    public void onTemplateDeselection();

}
