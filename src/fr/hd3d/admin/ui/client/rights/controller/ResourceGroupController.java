package fr.hd3d.admin.ui.client.rights.controller;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.rights.model.ResourceGroupModel;
import fr.hd3d.admin.ui.client.rights.view.IResourceGroupView;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;


/**
 * The <code>SubGridController</code> specification for handling resource group model data and its constraints on
 * persons and projects relations.
 * 
 * @author HD3D
 * 
 */
public class ResourceGroupController extends
        SubGridController<ResourceGroupModelData, ResourceGroupModel, IResourceGroupView>
{

    /** the selected project */
    ProjectModelData selectedProject;

    /** the selected person */
    PersonModelData selectedPerson;

    public ResourceGroupController(ResourceGroupModel model, IResourceGroupView view)
    {
        super(model, view);
    }

    protected String path()
    {
        return ServicesPath.RESOURCE_GROUPS;
    }

    protected void doHandleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == AdminEvents.ALL_ROLES_CLICKED)
        {
            deselectAll();
        }
        else if (type == AdminEvents.PERSON_SELECTED)
        {
            fullyLoaded = false;
            clearConstraints();
            selectedProject = null;
            selectedPerson = event.<PersonModelData> getData();
            setupConstraints();
            refreshStore();
            deselectAll();
        }
        else if (type == AdminEvents.PROJECT_SELECTED)
        {
            fullyLoaded = false;
            clearConstraints();
            selectedPerson = null;
            selectedProject = event.<ProjectModelData> getData();
            setupConstraints();
            refreshStore();
            deselectAll();
        }
    }

    protected void registerEvents()
    {
        super.registerEvents();
        registerEventTypes(AdminEvents.ALL_ROLES_CLICKED);
        registerEventTypes(AdminEvents.PERSON_SELECTED);
        registerEventTypes(AdminEvents.PROJECT_SELECTED);
    }

    protected void onConstraintsClearance()
    {
        selectedPerson = null;
        selectedProject = null;
        view.resetComboBoxes(true, true);
        EventDispatcher.forwardEvent(new AppEvent(AdminEvents.ALL_ROLES_CLICKED));
    }

    protected void setupConstraints()
    {
        if (selectedPerson != null)
        {
            List<Long> groupIds = selectedPerson.getResourceGroupIds();
            if (groupIds != null && !groupIds.isEmpty())
            {
                Constraint constraint = new Constraint(EConstraintOperator.in, Hd3dModelData.ID_FIELD, groupIds, null);
                constraints.add(constraint);
            }
            else
            {
                view.hideGrid();
            }
            view.resetComboBoxes(false, true);
        }
        else if (selectedProject != null)
        {
            List<Long> groupIds = selectedProject.getResourceGroupIds();
            if (groupIds != null && !groupIds.isEmpty())
            {
                Constraint constraint = new Constraint(EConstraintOperator.in, Hd3dModelData.ID_FIELD, groupIds, null);
                constraints.add(constraint);
            }
            else
            {
                view.hideGrid();
            }
            view.resetComboBoxes(true, false);
        }
        EventDispatcher.forwardEvent(new AppEvent(AdminEvents.ALL_ROLES_CLICKED));
    }

}
