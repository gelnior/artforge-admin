package fr.hd3d.admin.ui.client.users.view;

import java.util.Date;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.admin.ui.client.config.AdminConfig;
import fr.hd3d.admin.ui.client.config.AdminTimeConfig;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;


/**
 * This class provides with all the column rendering objects for the user accounts grid.
 * 
 * @author HD3D
 * 
 */
public final class UserColumnRenderers
{

    /**
     * Creates a rendering object for the account status column.
     * 
     * @return a renderer depending on the account status
     */
    public static GridCellRenderer<PersonModelData> getAccountStatusRenderer()
    {
        return new AccountStatusRenderer();
    }

    /**
     * Creates a rendering object for the account information.
     * 
     * @return a renderer that take date format into account
     */
    public static GridCellRenderer<PersonModelData> getAccountInfoRenderer()
    {
        return new AccountInfoRenderer();
    }

    /**
     * Creates a rendering object for the e-mail address column.
     * 
     * @return a renderer that renders e-mail addresses
     */
    public static GridCellRenderer<PersonModelData> getEMailRenderer()
    {
        return new EMailRenderer();
    }

    static class AccountStatusRenderer implements GridCellRenderer<PersonModelData>
    {
        public String render(PersonModelData model, String property, ColumnData config, int rowIndex, int colIndex,
                ListStore<PersonModelData> store, Grid<PersonModelData> grid)
        {
            String message;
            String style;
            Long status = model.getAccountStatus();
            int realStatus = status > 0 ? 0 : Integer.valueOf(String.valueOf(status));
            switch (realStatus)
            {
                case AdminConfig.ACCOUNT_OK:
                    message = AdminMainView.MESSAGES.OkAccountStatus();
                    style = "green";
                    break;
                case AdminConfig.ACCOUNT_MISSING:
                    message = AdminMainView.MESSAGES.NoAccountStatus();
                    style = "brown";
                    break;
                case AdminConfig.ACCOUNT_DEFAULTPASSWORD:
                    message = AdminMainView.MESSAGES.DefaultPasswordStatus();
                    style = "orange";
                    break;
                case AdminConfig.ACCOUNT_NULLPASSWORD:
                    message = AdminMainView.MESSAGES.NullPasswordStatus();
                    style = "yellow";
                    break;
                case AdminConfig.ACCOUNT_MULTIPLE:
                    message = AdminMainView.MESSAGES.MoreAccountsStatus();
                    style = "red";
                    break;
                case AdminConfig.ACCOUNT_EXTERNAL:
                    message = AdminMainView.MESSAGES.ExternalAccountStatus();
                    style = "teal";
                    break;
                default:
                    message = AdminMainView.MESSAGES.UnknownStatus();
                    style = "red";
            }
            return "<span style='color:" + style + "'>" + message + "</span>";
        }
    }

    static class AccountInfoRenderer implements GridCellRenderer<PersonModelData>
    {
        public Object render(PersonModelData model, String property, ColumnData config, int rowIndex, int colIndex,
                ListStore<PersonModelData> store, Grid<PersonModelData> grid)
        {
            Long status = model.getAccountStatus();
            int realStatus = status > 0 ? 1 : Integer.valueOf(String.valueOf(status));
            switch (realStatus)
            {
                case 1:
                    return AdminTimeConfig.DATE_TIME_FORMAT.format(new Date(status));
                case 0:
                    return "<span style='color:grey'>" + AdminMainView.CONSTANTS.NoConnection() + "</span>";
                default:
                    return null;
            }
        }
    }

    static class EMailRenderer implements GridCellRenderer<PersonModelData>
    {

        public Object render(PersonModelData model, String property, ColumnData config, int rowIndex, int colIndex,
                ListStore<PersonModelData> store, Grid<PersonModelData> grid)
        {
            String eMail = model.getEmail();
            if (eMail != null && eMail.contains("@"))
            {
                return "<span style='color:blue;text-decoration:underline'>" + eMail + "</span>";
            }
            else
            {
                return AdminMainView.MESSAGES.NoEMailAddress();
            }
        }
    }

}
