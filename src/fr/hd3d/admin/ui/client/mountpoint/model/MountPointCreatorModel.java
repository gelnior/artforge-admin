package fr.hd3d.admin.ui.client.mountpoint.model;

import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.modeldata.asset.MountPointModelData;


public class MountPointCreatorModel
{
    private MountPointModelData mountPoint = null;

    public void setMountPoint(String storageName, String os, String path) throws Hd3dException
    {
        mountPoint = new MountPointModelData(storageName, os, path);
    }

    public void save(EventType okEvent)
    {
        mountPoint.save(okEvent);
    }
}
