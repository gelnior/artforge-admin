package fr.hd3d.admin.ui.client.templates.model;

import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.admin.ui.client.model.AbstractRoleModel;
import fr.hd3d.common.ui.client.modeldata.technical.ProjectSecurityTemplateModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;


/**
 * This model specifies the role model provided by {@link fr.hd3d.admin.ui.client.model.AbstractRoleModel} inasmuch as
 * it takes the security template selection into account.
 * 
 * @author HD3D
 * 
 */
public abstract class RoleTemplateModel extends AbstractRoleModel
{

    /** the parent model */
    protected SecurityTemplateModel parent;

    public RoleTemplateModel(SecurityTemplateModel parent)
    {
        this.parent = parent;
    }

    public boolean wantsPaging()
    {
        return false;
    }

    protected EventType onCreationWhileAssociated(RoleModelData role)
    {
        role.setDefaultPath(ServicesPath.getPath("Role Templates"));
        return super.onCreationWhileAssociated(role);
    }

    public String getRoleClassName()
    {
        return "Role Template";
    }

    /**
     * Gets the selected project security template model data from the parent model.
     * 
     * @return the selected security template
     */
    public ProjectSecurityTemplateModelData getSelectedTemplate()
    {
        return parent.getSelected();
    }

    /**
     * Defines the event type that indicates a request for refreshing the store.
     * 
     * @return the 'refresh all' event type
     */
    public abstract EventType refreshAllEvent();

}
