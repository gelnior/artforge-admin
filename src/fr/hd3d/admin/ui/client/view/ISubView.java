package fr.hd3d.admin.ui.client.view;

import fr.hd3d.admin.ui.client.controller.SubController;


public interface ISubView
{

    /**
     * Creates the controller that will handle the sub view events
     * 
     * @return the sub view's specific controller
     */
    public SubController init();

    /**
     * Initializes the sub view's widgets
     */
    public void initWidgets();

    /**
     * 
     * @return the heading of the widget in order to name the main view's corresponding tab
     */
    public String getHeading();

}
