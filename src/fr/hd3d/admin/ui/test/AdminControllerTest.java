package fr.hd3d.admin.ui.test;

import org.junit.Test;

import fr.hd3d.admin.ui.client.model.AdminMainModel;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.test.UITestCase;


public class AdminControllerTest extends UITestCase
{

    AdminMainModel model = new AdminMainModel();

    @Test
    public void testServerConnection()
    {
        RestRequestHandlerSingleton.getInstance().getRequest("ping/", new TestCallback(model));
        assertTrue(model.isConnected());
    }

}
