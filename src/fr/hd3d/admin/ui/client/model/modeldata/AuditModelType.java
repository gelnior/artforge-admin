package fr.hd3d.admin.ui.client.model.modeldata;

import java.util.Date;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.AuditModelData;


/**
 * Describes the audit object fields.
 * 
 * @author HD3D
 */
public class AuditModelType extends ModelType
{
    /**
     * Default constructor.
     */
    public AuditModelType()
    {
        super();

        this.setPrimaryFields();
        this.setObjectFields();
    }

    /**
     * Set main fields like the the id field.
     */
    private void setPrimaryFields()
    {
        this.setRoot(Hd3dModelData.ROOT);
        this.setTotalName(Hd3dModelData.NB_RECORDS);

        DataField idDataField = new DataField(Hd3dModelData.ID_FIELD);
        idDataField.setType(Long.class);

        this.addField(idDataField);
    }

    /**
     * Sets object specific fields.
     */
    private void setObjectFields()
    {
        DataField entityIdDataField = new DataField(AuditModelData.ENTITY_ID_FIELD);
        entityIdDataField.setType(Long.class);

        DataField dateDataField = new DataField(AuditModelData.DATE_FIELD);
        dateDataField.setType(Date.class);
        dateDataField.setFormat(AuditModelData.TIMESTAMP_FORMAT_PATTERN);

        DataField dateMsDataField = new DataField(AuditModelData.DATE_MS_FIELD);
        dateMsDataField.setType(Long.class);

        this.addField(AuditModelData.CLASS_NAME_FIELD);
        this.addField(AuditModelData.OPERATION_FIELD);
        this.addField(entityIdDataField);
        this.addField(dateDataField);
        this.addField(dateMsDataField);
        this.addField(AuditModelData.ENTITY_NAME_FIELD);
        this.addField(AuditModelData.MODIFICATION_FIELD);
    }
}
