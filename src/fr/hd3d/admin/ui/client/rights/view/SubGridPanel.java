package fr.hd3d.admin.ui.client.rights.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.toolbar.PagingToolBar;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;

import fr.hd3d.admin.ui.client.config.AdminConfig;
import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.factoring.simplegrid.Hd3dSimpleGrid;
import fr.hd3d.admin.ui.client.rights.model.SubGridModel;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.admin.ui.client.view.listener.GridSelectionListener;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;
import fr.hd3d.common.ui.client.listener.EventRelay;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationDialog;


/**
 * A wrapping view for displaying a simple single fielded non-modifiable grid provided with a selection mechanism,
 * refreshment and supporting constraints on the data to be loaded.
 * 
 * @author HD3D
 * 
 * @param <D>
 *            the model data type
 * @param <M>
 *            the adequate model for handling selection and refreshment
 */
public abstract class SubGridPanel<D extends Hd3dModelData, M extends SubGridModel<D>> extends ContentPanel implements
        ISubGridView<D>
{

    /** The inner grid */
    protected Hd3dSimpleGrid<D> auxGrid;

    /** The grid model */
    protected M model;

    protected PagingToolBar pagingBar = new PagingToolBar(AdminConfig.PAGING_LIMIT);
    protected ToolBar actionBar = new ToolBar();

    protected Button allButton;

    protected RelationDialog relDiag = new RelationDialog();

    /**
     * Creates the panel as a <code>ContentPanel</code> and associates it with the model parameter.
     * 
     * @param model
     *            the model for the inner grid
     */
    public SubGridPanel(M model)
    {
        super();
        this.model = model;
    }

    /**
     * Defines the <code>D</code> field that will be displayed in the grid (without specific rendering).
     * 
     * @return the field name of the field to display
     */
    protected abstract String nameField();

    public void setup()
    {
        gridSetup();

        setStyle();

        if (model.wantsPaging())
        {
            setPagingToolBar();
            setBottomComponent(pagingBar);
        }

        setActionToolBar();
        setTopComponent(actionBar);

        relDiag.addListener(Events.Hide, new EventRelay(AdminEvents.ALL_GROUPS_CLICKED, null));

        // TODO no context menu for now
        // auxGrid.setContextMenu(new SubGridMenu());

        auxGrid.addListener(Events.CellClick, new GridSelectionListener<D>(model.selectionEventType()));
        add(auxGrid, new FitData());
    }

    /**
     * Sets the panel's style, layout, borders, etc.
     */
    protected void setStyle()
    {
        setBodyBorder(true);
        setBorders(false);
        setHeaderVisible(true);
        setLayout(new FitLayout());
    }

    /**
     * Initializes the data grid component.
     */
    protected void gridSetup()
    {
        auxGrid = new Hd3dSimpleGrid<D>();
        auxGrid.addColumn(nameField(), AdminMainView.CONSTANTS.Name(), 300, true);
        auxGrid.setup(model);
        auxGrid.setAutoExpandColumn(nameField());
        auxGrid.setBorders(false);
    }

    /**
     * Sets the paging tool bar for the data grid.
     */
    protected void setPagingToolBar()
    {
        pagingBar.bind(model.getLoader());
        pagingBar.refresh();
        pagingBar.setStyleAttribute("padding", "5px");
    }

    /**
     * Sets the action tool bar for the data grid.
     */
    protected void setActionToolBar()
    {
        allButton = setupSubGridButton(AdminMainView.CONSTANTS.AllButton(), AdminMainView.MESSAGES.ReloadAll(),
                "all-icon", model.allEventType());
        allButton.disable();
        actionBar.add(allButton);
        actionBar.setStyleAttribute("padding", "5px");
    }

    public void hideGrid()
    {
        if (auxGrid.isVisible())
        {
            auxGrid.hide();
            pagingBar.disable();
        }
    }

    public void showGrid()
    {
        if (!auxGrid.isVisible())
        {
            auxGrid.show();
            pagingBar.enable();
        }
    }

    public void onFullDeselection()
    {}

    public void deselectAll(boolean fullLoaded)
    {
        auxGrid.getSelectionModel().deselectAll();
        if (fullLoaded)
        {
            allButton.disable();
        }
        else
        {
            allButton.enable();
        }
        onFullDeselection();
    }

    /**
     * Sets a button with proper listener, icon, text...
     * 
     * @return the button with all proper settings
     */
    protected Button setupSubGridButton(String text, String toolTip, String iconCSS, EventType eventType)
    {
        Button result = new Button();
        if (text != null)
        {
            result.setText(text);
        }
        if (toolTip != null)
        {
            result.setToolTip(toolTip);
        }
        if (iconCSS != null)
        {
            result.setIconStyle(iconCSS);
        }
        if (eventType != null)
        {
            result.addSelectionListener(new ButtonClickListener(eventType));
        }
        return result;
    }

    /**
     * A simple grid context menu
     * 
     * @author HD3D
     * 
     */
    class SubGridMenu extends Menu
    {
        public SubGridMenu()
        {
            super();
            setup();
        }

        public void setup()
        {
            MenuItem item = new MenuItem();
            item.setText(AdminMainView.MESSAGES.OpenRelationTool());
            item.addListener(Events.Select, new SubGridMenuListener());
            add(item);
        }
    }

    /**
     * A simple grid menu listener
     * 
     * @author HD3D
     * 
     */
    class SubGridMenuListener implements Listener<BaseEvent>
    {
        public void handleEvent(BaseEvent be)
        {
            List<Hd3dModelData> selection = new ArrayList<Hd3dModelData>();
            D item = auxGrid.getSelectionModel().getSelectedItem();
            selection.add(item);
            relDiag.show(selection);
            relDiag.setHeading(relDiag.getHeading().split(" - ")[0] + " - " + item.get(nameField()));
        }
    }

}
