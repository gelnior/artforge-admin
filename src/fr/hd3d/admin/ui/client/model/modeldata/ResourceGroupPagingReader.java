package fr.hd3d.admin.ui.client.model.modeldata;

import fr.hd3d.common.ui.client.modeldata.reader.Hd3dPagingJsonReader;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;


public class ResourceGroupPagingReader extends Hd3dPagingJsonReader<ResourceGroupModelData>
{

    public ResourceGroupPagingReader()
    {
        super(ResourceGroupModelData.getModelType());
    }

    public ResourceGroupModelData newModelInstance()
    {
        return new ResourceGroupModelData();
    }

}
