package fr.hd3d.admin.ui.client.model.modeldata;

import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.ListLoadResult;

import fr.hd3d.common.ui.client.modeldata.reader.ProjectSecurityTemplateReader;
import fr.hd3d.common.ui.client.modeldata.technical.ProjectSecurityTemplateModelData;


/**
 * This singleton class provides the application with the adequate project security template reader.
 * 
 * @author HD3D
 * 
 */
public class SingleTemplateReader
{

    private static DataReader<ListLoadResult<ProjectSecurityTemplateModelData>> lReader;

    /**
     * Call in order to get a project security template reader.
     * 
     * @return the adequate project security template reader
     */
    public static DataReader<ListLoadResult<ProjectSecurityTemplateModelData>> getReader()
    {
        if (lReader == null)
        {
            lReader = new ProjectSecurityTemplateReader();
        }
        return lReader;
    }

    /**
     * Sets the inner instance to the specified parameter.
     * 
     * @param reader
     *            the new reader instance
     */
    public static void setInstance(DataReader<ListLoadResult<ProjectSecurityTemplateModelData>> reader)
    {
        if (reader != null)
        {
            lReader = reader;
        }
    }

}
