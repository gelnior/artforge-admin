package fr.hd3d.admin.ui.client.model.modeldata;

import fr.hd3d.common.ui.client.modeldata.reader.IPagingReader;
import fr.hd3d.common.ui.client.modeldata.reader.PagingPersonReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;


/**
 * This singleton class provides the application with the adequate person reader.
 * 
 * @author HD3D
 * 
 */
public abstract class SinglePersonReader
{

    private static IPagingReader<PersonModelData> lReader;

    /**
     * Call in order to get a person reader.
     * 
     * @return the adequate person reader
     */
    public static IPagingReader<PersonModelData> getReader()
    {
        if (lReader == null)
        {
            lReader = new PagingPersonReader();
        }
        return lReader;
    }

    /**
     * Sets the inner instance to the specified parameter.
     * 
     * @param reader
     *            the new reader instance
     */
    public static void setInstance(IPagingReader<PersonModelData> reader)
    {
        if (reader != null)
        {
            lReader = reader;
        }
    }

}
