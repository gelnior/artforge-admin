package fr.hd3d.admin.ui.client.config;

/**
 * Constants needed by the Admin application.
 * 
 * @author HD3D
 */
public class AdminConfig
{

    /** Default offset for paging grid. */
    public static int PAGING_LIMIT = 50;

    /** The direct path to the UserAccounts resource */
    public static String USERACCOUNTS_PATH = "user-accounts/";
    /** The deduced account permission path */
    public static String USERACCOUNTS_PERM = "user-accounts";

    /** The direct path to the ServicesURLs resource */
    public static String SERVICESURLS_PATH = "services-urls/";

    /** The project ID marker (for security templates) */
    public static String PROJECT_ID_MARK = "{projectId}";

    /** The server 'secret' attribute for user accounts resource */
    public static final String USERACCOUNT_PASSWORD_ATTRIBUTE = "account_secret";

    /** The server 'new password' attribute for user accounts resource */
    public static final String USERACCOUNT_NEWPASS_ATTRIBUTE = "new_password";

    /*
     * the account status code defined on the server. Since the values must be synchronized with that of the server's
     * fr.hd3d.services.security.util.UserAccountsUtil class, don't change the following unless you pass on modification
     * from the server
     */
    public final static int ACCOUNT_OK = 0;
    public final static int ACCOUNT_MISSING = -1;
    public final static int ACCOUNT_DEFAULTPASSWORD = -2;
    public final static int ACCOUNT_NULLPASSWORD = -3;
    public final static int ACCOUNT_MULTIPLE = -4;
    public final static int ACCOUNT_EXTERNAL = -5;

    /* The permission strings */
    public final static String CREATE = "create";
    public final static String READ = "read";
    public final static String UPDATE = "update";
    public final static String DELETE = "delete";

    /* data object marker for service URL tree */
    public final static String ISDATAOBJECT = "DataObject";
    public final static String NOTDATAOBJECT = "Other";

    // public static final int ACCOUNT_OFFSET = 100;

}
