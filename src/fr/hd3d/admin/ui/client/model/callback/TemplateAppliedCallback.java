package fr.hd3d.admin.ui.client.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


/**
 * This call-back orders the view to display an information speech bubble when a security template has correctly been
 * applied to a project.
 * 
 * @author HD3D
 * 
 */
public class TemplateAppliedCallback extends BaseCallback
{

    private String templateName;
    private String projectName;

    /**
     * Creates a call-back that displays an information speech bubble when a template is applied.
     * 
     * @param templateName
     *            the template name
     * @param projectName
     *            the project name
     */
    public TemplateAppliedCallback(String templateName, String projectName)
    {
        super();
        this.templateName = templateName;
        this.projectName = projectName;
    }

    protected void onSuccess(Request request, Response response)
    {
        Info.display(AdminMainView.CONSTANTS.TemplateSuccessfullyApplied(), AdminMainView.MESSAGES
                .TemplateSuccessfullyApplied(), templateName, projectName);
    }

}
