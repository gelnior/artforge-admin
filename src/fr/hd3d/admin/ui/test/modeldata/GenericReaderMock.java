package fr.hd3d.admin.ui.test.modeldata;

import fr.hd3d.admin.ui.client.model.modeldata.GenericModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.Hd3dListJsonReaderMock;


public class GenericReaderMock extends Hd3dListJsonReaderMock<GenericModelData>
{

    public GenericReaderMock()
    {
        super();
        setModelType(GenericModelData.getModelType());
    }

    public GenericModelData newModelInstance()
    {
        return new GenericModelData();
    }

}
