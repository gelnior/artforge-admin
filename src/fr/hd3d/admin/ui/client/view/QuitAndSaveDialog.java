package fr.hd3d.admin.ui.client.view;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * A dialog that is prompted when unsaved changes might be lost.
 * 
 * @author HD3D
 * 
 */
public class QuitAndSaveDialog extends Dialog
{

    /** the dialog heading */
    protected String heading;
    /** the item with unsaved changes */
    protected Hd3dModelData item;
    /** the 'save' event type */
    protected EventType saveEventType;
    /** the 'don't save' event type */
    protected EventType cancelEventType;
    /** the event that prompted the dialog */
    protected AppEvent delayedEvent;
    /** whether the listener must save the item or not */
    protected boolean autoSave;

    protected LabelToolItem label = new LabelToolItem();

    /**
     * Builds a quit and/or save dialog with specified parameters and readies it for display.
     * 
     * @param heading
     *            the dialog heading
     * @param item
     *            the item with unsaved changes
     * @param saveEventType
     *            the 'save' event, auto-saving will be performed if this parameter is null
     * @param cancelEventType
     *            the 'cancel' event
     * @param delayedEvent
     *            the event interrupted by this dialog
     */
    public QuitAndSaveDialog(String heading, Hd3dModelData item, EventType saveEventType, EventType cancelEventType,
            AppEvent delayedEvent)
    {
        this.heading = heading;
        this.item = item;
        this.saveEventType = saveEventType;
        this.cancelEventType = cancelEventType;
        this.delayedEvent = delayedEvent;
        if (saveEventType == null)
        {
            this.autoSave = true;
        }

        setup();
    }

    /**
     * Sets the value of delayed event, which will be triggered after the 'save' or 'cancel' events.
     * 
     * @param delayedEvent
     *            the new delayed event
     */
    public void setDelayedEvent(AppEvent delayedEvent)
    {
        this.delayedEvent = delayedEvent;
    }

    /**
     * Sets the item that will be save when clicking on the 'save' button.
     * 
     * @param toSave
     *            the item to save
     * 
     */
    public void setItemToSave(Hd3dModelData toSave)
    {
        item = toSave;
        if (item != null)
        {
            setItemName((String) item.get(RecordModelData.NAME_FIELD));
        }
    }

    /**
     * Appends the concerned item name to the dialog label.
     * 
     * @param adding
     *            the item name
     */
    protected void setItemName(String adding)
    {
        label.setLabel(AdminMainView.MESSAGES.WantToSaveChanges() + " " + adding + AdminMainView.MESSAGES.QMark());
    }

    /**
     * Builds the dialog components.
     */
    protected void setup()
    {
        setHeading(heading);
        setLabel();
        setHideOnButtonClick(true);
        setModal(true);
        setButtons(Dialog.YESNOCANCEL);

        Button save = getButtonById(YES);
        save.setText(AdminMainView.CONSTANTS.Save());
        save.addSelectionListener(new QuitAndSaveRelay(saveEventType));

        Button nosave = getButtonById(NO);
        nosave.setText(AdminMainView.CONSTANTS.UndoAll());
        nosave.addSelectionListener(new QuitAndSaveRelay(cancelEventType));

        Button cancel = getButtonById(CANCEL);
        cancel.setText(AdminMainView.CONSTANTS.Cancel());
    }

    /**
     * Sets and adds the label.
     */
    protected void setLabel()
    {
        add(label);
    }

    /**
     * A button listener specifically designed for the save or quit button.
     * 
     * @author HD3D
     * 
     */
    class QuitAndSaveRelay extends ButtonClickListener
    {
        public QuitAndSaveRelay(EventType type)
        {
            super(type);
        }

        public void componentSelected(ButtonEvent be)
        {
            if (event.getType() != cancelEventType && autoSave)
            {
                item.save();
            }
            super.componentSelected(be);
            if (delayedEvent != null)
            {
                EventDispatcher.forwardEvent(delayedEvent);
            }
        }
    }

}
