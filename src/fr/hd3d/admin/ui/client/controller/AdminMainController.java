package fr.hd3d.admin.ui.client.controller;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.model.AdminMainModel;
import fr.hd3d.admin.ui.client.view.IAdminMainView;
import fr.hd3d.common.ui.client.widget.mainview.MainController;


/**
 * This is the application main controller, dedicated to configuration, error and start events handling.
 * 
 * @author HD3D
 * 
 */
public class AdminMainController extends MainController
{
    /** the admin main view */
    final protected IAdminMainView view;
    /** the admin main model */
    final protected AdminMainModel model;

    /**
     * Instantiates the main controller for the specified model and view.
     * 
     * @param model
     *            the main model
     * @param view
     *            the main view
     */
    public AdminMainController(AdminMainModel model, IAdminMainView view)
    {
        super(model, view);

        this.model = model;
        this.view = view;
    }

    public void handleEvent(AppEvent event)
    {
        this.forwardToChild(event);
        super.handleEvent(event);
    }

    protected void onPermissionsInitialized(AppEvent event)
    {
        model.setConnected();
        super.onPermissionsInitialized(event);

        view.initWidgets();
        view.hideStartPanel();
    }

    protected void registerEvents()
    {
        super.registerEvents();
    }

}
