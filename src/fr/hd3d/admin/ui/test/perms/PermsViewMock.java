package fr.hd3d.admin.ui.test.perms;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.perms.model.PermModelData;
import fr.hd3d.admin.ui.client.perms.model.PermsModel;
import fr.hd3d.admin.ui.client.perms.view.IPermsView;
import fr.hd3d.common.ui.client.event.EventDispatcher;


public class PermsViewMock implements IPermsView
{

    public boolean gridShown = true;
    public boolean init = false;
    public boolean undoSaveEnabled = false;
    public boolean redoEnabled = false;

    public String roleName;

    private PermsModel model;

    public PermsViewMock(PermsModel model)
    {
        this.model = model;
    }

    public void onEmptyRedoStack()
    {
        redoEnabled = false;
    }

    public void onEmptyUndoStack(boolean saveAlso)
    {
        undoSaveEnabled = false;
    }

    public void onRoleSelection()
    {
        roleName = model.getRoleName();
        if (model.getRole() == null)
        {
            gridShown = false;
        }
    }

    public void onUndo()
    {
        redoEnabled = true;
    }

    public void onUndoableAction()
    {
        undoSaveEnabled = true;
    }

    public void setup()
    {
        init = true;
    }

    public void showGrid()
    {
        gridShown = true;
    }

    public void setNodeExpanded(PermModelData node, boolean expand)
    {}

    public void refreshGrid()
    {}

    public void showQuitAndSaveDialog(AppEvent delayedEvent)
    {
        EventDispatcher.forwardEvent(model.fullUndoEvent());
        EventDispatcher.forwardEvent(delayedEvent);
    }

    public String getAllChildrenString()
    {
        return "*";
    }

    public String getAnyVersionString()
    {
        return "*";
    }

}
