package fr.hd3d.admin.ui.client.event;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * All the Admin application specific events.
 * 
 * @author HD3D
 * 
 */
public class AdminEvents
{

    // the event type number start
    public static final int START = 22000;
    private static int next = START;

    // rights events : roles
    public static final EventType SERVER_ROLES_MODIFICATION = new EventType(next++);
    public static final EventType ROLE_SELECTED = new EventType(next++);
    public static final EventType ALL_ROLES_CLICKED = new EventType(next++);
    public static final EventType CREATE_ROLE_CLICKED = new EventType(next++);
    public static final EventType DELETE_ROLE_CLICKED = new EventType(next++);
    public static final EventType EDIT_ROLE_CLICKED = new EventType(next++);
    public static final EventType CREATE_ROLE_CONFIRMED = new EventType(next++);
    public static final EventType EDIT_ROLE_CONFIRMED = new EventType(next++);
    public static final EventType REMOVE_ROLE_CLICKED = new EventType(next++);
    public static final EventType ADD_ROLE_CLICKED = new EventType(next++);

    // rights events : groups
    public static final EventType SERVER_SELECTED_GROUP_MODIFICATION = new EventType(next++);
    public static final EventType RESOURCEGROUP_SELECTED = new EventType(next++);
    public static final EventType ALL_GROUPS_CLICKED = new EventType(next++);
    public static final EventType PROJECT_SELECTED = new EventType(next++);
    public static final EventType PERSON_SELECTED = new EventType(next++);

    // user accounts events
    public static final EventType REFRESH_USERS_LIST = new EventType(next++);
    public static final EventType USER_SELECTED = new EventType(next++);
    public static final EventType USER_CREATE_ACCOUNT_CLICKED = new EventType(next++);
    public static final EventType USER_EDIT_ACCOUNT_CLICKED = new EventType(next++);
    public static final EventType USER_DELETE_ACCOUNT_CLICKED = new EventType(next++);
    public static final EventType USER_DELETE_ACCOUNT_CONFIRMED = new EventType(next++);

    // templates
    public static final EventType TEMPLATE_SELECTED = new EventType(next++);
    public static final EventType TEMPLATE_CREATE_CLICKED = new EventType(next++);
    public static final EventType TEMPLATE_CREATE_CONFIRMED = new EventType(next++);
    public static final EventType TEMPLATE_EDIT_CLICKED = new EventType(next++);
    public static final EventType TEMPLATE_EDIT_CONFIRMED = new EventType(next++);
    public static final EventType TEMPLATE_DELETE_CLICKED = new EventType(next++);
    public static final EventType TEMPLATE_DELETE_CONFIRMED = new EventType(next++);
    public static final EventType TEMPLATE_COMMENT_SAVE = new EventType(next++);
    public static final EventType TEMP_PROJECT_SELECTED = new EventType(next++);
    public static final EventType APPLY_TO_PROJECT = new EventType(next++);

    // templates : template's roles
    public static final EventType TEMP_OWN_SELECTED = new EventType(next++);
    public static final EventType TEMP_OWN_REFRESH_CLICKED = new EventType(next++);
    public static final EventType TEMP_OWN_CREATE_CLICKED = new EventType(next++);
    public static final EventType TEMP_OWN_DELETE_CLICKED = new EventType(next++);
    public static final EventType TEMP_OWN_EDIT_CLICKED = new EventType(next++);
    public static final EventType TEMP_OWN_CREATE_CONFIRMED = new EventType(next++);
    public static final EventType TEMP_OWN_EDIT_CONFIRMED = new EventType(next++);
    public static final EventType TEMP_OWN_REMOVE_CLICKED = new EventType(next++);
    public static final EventType TEMP_OWN_SERVER_MODIFICATION = new EventType(next++);

    // templates : all roles
    public static final EventType TEMP_ALL_SELECTED = new EventType(next++);
    public static final EventType TEMP_ALL_REFRESH_CLICKED = new EventType(next++);
    public static final EventType TEMP_ALL_CREATE_CLICKED = new EventType(next++);
    public static final EventType TEMP_ALL_DELETE_CLICKED = new EventType(next++);
    public static final EventType TEMP_ALL_EDIT_CLICKED = new EventType(next++);
    public static final EventType TEMP_ALL_CREATE_CONFIRMED = new EventType(next++);
    public static final EventType TEMP_ALL_EDIT_CONFIRMED = new EventType(next++);
    public static final EventType TEMP_ALL_ADD_CLICKED = new EventType(next++);
    public static final EventType TEMP_ALL_SERVER_MODIFICATION = new EventType(next++);

    // permissions and bans : inner events
    public static final EventType SERVICE_URLS_INITIALIZED = new EventType(next++);
    // rights
    public static final EventType CAN_CREATE_CLICKED = new EventType(next++);
    public static final EventType CAN_ALL_CLICKED = new EventType(next++);
    public static final EventType CAN_READ_CLICKED = new EventType(next++);
    public static final EventType CAN_UPDATE_CLICKED = new EventType(next++);
    public static final EventType CAN_DELETE_CLICKED = new EventType(next++);
    public static final EventType EXPAND_NODE = new EventType(next++);
    public static final EventType EXPANSION_FINISHED = new EventType(next++);
    public static final EventType REDUCE_NODE = new EventType(next++);
    public static final EventType GET_NODE_CHILDREN = new EventType(next++);
    public static final EventType PERM_LIST_SAVE = new EventType(next++);
    public static final EventType PERM_LIST_UNDO = new EventType(next++);
    public static final EventType PERM_LIST_REDO = new EventType(next++);
    public static final EventType PERM_LIST_FULL_UNDO = new EventType(next++);
    public static final EventType REFRESH_PERM_GRID = new EventType(next++);
    // templates
    public static final EventType TEMP_CAN_CREATE_CLICKED = new EventType(next++);
    public static final EventType TEMP_CAN_ALL_CLICKED = new EventType(next++);
    public static final EventType TEMP_CAN_READ_CLICKED = new EventType(next++);
    public static final EventType TEMP_CAN_UPDATE_CLICKED = new EventType(next++);
    public static final EventType TEMP_CAN_DELETE_CLICKED = new EventType(next++);
    public static final EventType TEMP_EXPAND_NODE = new EventType(next++);
    public static final EventType TEMP_EXPANSION_FINISHED = new EventType(next++);
    public static final EventType TEMP_REDUCE_NODE = new EventType(next++);
    public static final EventType TEMP_GET_NODE_CHILDREN = new EventType(next++);
    public static final EventType TEMP_PERM_LIST_SAVE = new EventType(next++);
    public static final EventType TEMP_PERM_LIST_UNDO = new EventType(next++);
    public static final EventType TEMP_PERM_LIST_REDO = new EventType(next++);
    public static final EventType TEMP_PERM_LIST_FULL_UNDO = new EventType(next++);
    public static final EventType TEMP_REFRESH_PERM_GRID = new EventType(next++);

    // do nothing
    public static final EventType REMAIN_IDLE = new EventType(next++);

    /* deprecated section */

    // rights : permissions and bans events
    public static final EventType ADD_PERMISSION_CLICKED = new EventType(next++);
    public static final EventType ADD_PERMISSION_CONFIRMED = new EventType(next++);
    public static final EventType ADD_BAN_CLICKED = new EventType(next++);
    public static final EventType ADD_BAN_CONFIRMED = new EventType(next++);
    public static final EventType REMOVE_PERMISSION = new EventType(next++);
    public static final EventType REMOVE_BAN = new EventType(next++);

    // templates : permissions and bans events
    public static final EventType TEMP_ADD_PERMISSION_CLICKED = new EventType(next++);
    public static final EventType TEMP_ADD_PERMISSION_CONFIRMED = new EventType(next++);
    public static final EventType TEMP_ADD_BAN_CLICKED = new EventType(next++);
    public static final EventType TEMP_ADD_BAN_CONFIRMED = new EventType(next++);
    public static final EventType TEMP_REMOVE_PERMISSION = new EventType(next++);
    public static final EventType TEMP_REMOVE_BAN = new EventType(next++);
}
