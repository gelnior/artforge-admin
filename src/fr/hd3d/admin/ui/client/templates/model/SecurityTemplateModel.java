package fr.hd3d.admin.ui.client.templates.model;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.model.callback.TemplateAppliedCallback;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ProjectSecurityTemplateModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;


/**
 * This model is the main model of the 'Security Templates Management' application. It is connected to the two role
 * templates models (owned and not owned) and provides the controller with methods for creating, deleting, editing the
 * selected security template and apply the template to the selected project as well.
 * 
 * @author HD3D
 * 
 */
public class SecurityTemplateModel
{

    /** the selected security template */
    private ProjectSecurityTemplateModelData selected;
    /** the selected project */
    private ProjectModelData project;

    /** the 'owned' roles model */
    private OwnedRoleModel owned;
    /** the 'other' roles model */
    private AllRoleModel all;

    public SecurityTemplateModel()
    {}

    /**
     * Sets the selected security template and refreshes it from the server.
     * 
     * @param selected
     *            the security template to select
     */
    public void setSelected(ProjectSecurityTemplateModelData selected)
    {
        this.selected = selected;
        if (selected != null)
        {
            selected.refresh();
        }
    }

    /**
     * Gets the selected security template.
     * 
     * @return the selected security template model data.
     */
    public ProjectSecurityTemplateModelData getSelected()
    {
        return selected;
    }

    /**
     * Sets the selected project.
     * 
     * @param project
     *            the project to select
     */
    public void setProject(ProjectModelData project)
    {
        this.project = project;
    }

    /**
     * Sets the model for handling the role templates owned by the selected security template. Use once for all.
     * 
     * @param model
     *            the 'owned' roles model
     */
    public void setOwned(OwnedRoleModel model)
    {
        owned = model;
    }

    /**
     * Gets the model for handling the role templates owned by the selected security template.
     * 
     * @return the 'owned' roles model
     */
    public OwnedRoleModel getOwned()
    {
        return owned;
    }

    /**
     * Sets the model for handling the role templates not owned by the selected security template. Use once for all.
     * 
     * @param model
     *            the 'other' roles model
     */
    public void setAll(AllRoleModel model)
    {
        all = model;
    }

    /**
     * Gets the model for handling the role templates not owned by the selected security template.
     * 
     * @return the 'other' roles model
     */
    public AllRoleModel getAll()
    {
        return all;
    }

    /**
     * Refreshes the template by linking it with the newly created role if such is the case.
     */
    public void refreshTemplate()
    {
        RoleModelData role = owned.selected();
        if (role != null && selected != null)
        {
            Long id = role.getId();
            if (id != null && !selected.getRoleIds().contains(id))
            {
                selected.getRoleIds().add(id);
                selected.save();
            }
        }
    }

    /**
     * Sets the comment parameter string as the comment value for the selected security template and puts it on the
     * server.
     * 
     * @param comment
     *            the new comment string
     */
    public void editComment(String comment)
    {
        if (comment != null && comment.length() > 255)
        {
            comment = comment.substring(0, 255);
        }
        selected.setComment(comment);
        selected.save();
    }

    /**
     * Create a new security template with the specified name and posts it on the server.
     * 
     * @param name
     *            the new security template's name
     */
    public void createLine(String name)
    {
        ProjectSecurityTemplateModelData template = new ProjectSecurityTemplateModelData();
        template.setName(name);
        selected = template;
        template.save(AdminEvents.TEMPLATE_SELECTED);
    }

    /**
     * Changes the selected security template's name to the specified name and puts it on the server.
     * 
     * @param name
     *            the new name string
     */
    public void editLine(String name)
    {
        selected.setName(name);
        selected.save();
    }

    /**
     * Deletes the selected security template and sends a delete request to the server.
     */
    public void deleteLine()
    {
        if (selected != null)
        {
            selected.delete();
            setSelected(null);
        }
    }

    /**
     * Sends a request to the server so that it applies the selected security template to the selected project. A
     * success notice is returned to the controller afterwards.
     */
    public void applyTemplate()
    {
        IRestRequestHandler handler = RestRequestHandlerSingleton.getInstance();
        handler.postRequest(ServicesPath.getPath("Apply Template") + selected.getId() + '/' + project.getId(),
                new TemplateAppliedCallback(selected.getName(), project.getName()), null);
    }

}
