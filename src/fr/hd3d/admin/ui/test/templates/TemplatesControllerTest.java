package fr.hd3d.admin.ui.test.templates;

import org.junit.Test;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.model.modeldata.SingleProjectReader;
import fr.hd3d.admin.ui.client.model.modeldata.SingleRoleReader;
import fr.hd3d.admin.ui.client.model.modeldata.SingleTemplateReader;
import fr.hd3d.admin.ui.client.templates.controller.AllRoleController;
import fr.hd3d.admin.ui.client.templates.controller.OwnedRoleController;
import fr.hd3d.admin.ui.client.templates.controller.SecurityTemplateController;
import fr.hd3d.admin.ui.client.templates.controller.TemplatesController;
import fr.hd3d.admin.ui.client.templates.model.AllRoleModel;
import fr.hd3d.admin.ui.client.templates.model.OwnedRoleModel;
import fr.hd3d.admin.ui.client.templates.model.SecurityTemplateModel;
import fr.hd3d.admin.ui.test.modeldata.ProjectReaderMock;
import fr.hd3d.admin.ui.test.modeldata.ProjectSecurityTemplateReaderMock;
import fr.hd3d.admin.ui.test.modeldata.RoleReaderMock;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dModelDataReaderSingleton;
import fr.hd3d.common.ui.client.modeldata.technical.ProjectSecurityTemplateModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.widget.sheeteditor.mock.Hd3dModelDataReaderMock;


public class TemplatesControllerTest extends UITestCase
{

    TemplatesViewMock mockView;
    int aCount = 0;

    int random = (int) (1000 * Math.random());

    OwnedRoleModel oM;
    RoleTemplateViewMock oV;

    AllRoleModel aM;
    RoleTemplateViewMock aV;

    SecurityTemplateModel sM;
    SecurityTemplateViewMock sV;

    public void setUp()
    {
        super.setUp();

        // set super user
        PermissionUtil.setDebugOn();

        // set mock readers
        SingleRoleReader.setSimpleInstance(new RoleReaderMock());
        SingleTemplateReader.setInstance(new ProjectSecurityTemplateReaderMock());
        SingleProjectReader.setInstance(new ProjectReaderMock());
        Hd3dModelDataReaderSingleton.setInstanceAsMock(new Hd3dModelDataReaderMock());

        // security templates
        sM = new SecurityTemplateModel();
        sV = new SecurityTemplateViewMock(sM);
        SecurityTemplateController sC = new SecurityTemplateController(sM, sV);

        // owned role
        oM = new OwnedRoleModel(sM);
        oV = new RoleTemplateViewMock(oM);
        OwnedRoleController oC = new OwnedRoleController(oM, oV);

        // all role
        aM = new AllRoleModel(sM);
        aV = new RoleTemplateViewMock(aM);
        AllRoleController aC = new AllRoleController(aM, aV);

        mockView = new TemplatesViewMock(oV, aV, sV);
        TemplatesController controller = new TemplatesController(mockView);
        sC.addChild(oC);
        sC.addChild(aC);
        controller.addChild(sC);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    private void init()
    {
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);

        aCount = aM.getStore().getCount();
    }

    @Test
    public void testInit()
    {
        init();

        assertTrue(mockView.init);
        assertTrue(aCount != 0);
    }

    private ProjectSecurityTemplateModelData selectTemplate()
    {
        ProjectSecurityTemplateModelData r = new ProjectSecurityTemplateModelData();
        r.setName("new template " + random);
        r.save();

        AppEvent sel = new AppEvent(AdminEvents.TEMPLATE_SELECTED);
        sel.setData(r);
        EventDispatcher.forwardEvent(sel);

        return r;
    }

    @Test
    public void testSelectAndDeleteTemplate()
    {
        init();

        ProjectSecurityTemplateModelData r = selectTemplate();

        assertEquals(r, sM.getSelected());
        assertTrue(sV.tempSelected);
        assertEquals(r, oM.getSelectedTemplate());
        assertEquals(r, aM.getSelectedTemplate());
        assertEquals(oM.getStore().getCount(), 0);

        EventDispatcher.forwardEvent(AdminEvents.TEMPLATE_DELETE_CONFIRMED);

        assertTrue(!sV.tempSelected);
        assertNull(sM.getSelected());
    }

    @Test
    public void testTemplateDemands()
    {
        init();

        ProjectSecurityTemplateModelData r = selectTemplate();

        EventDispatcher.forwardEvent(AdminEvents.TEMPLATE_DELETE_CLICKED);
        assertEquals(sV.delete, 1);

        EventDispatcher.forwardEvent(AdminEvents.TEMPLATE_EDIT_CLICKED);
        assertEquals(sV.edit, 1);

        EventDispatcher.forwardEvent(AdminEvents.TEMPLATE_CREATE_CLICKED);
        assertEquals(sV.create, 1);

        r.delete();
    }

    @Test
    public void testCreateAndEdit()
    {
        init();

        String name = "created template " + random;
        AppEvent cre = new AppEvent(AdminEvents.TEMPLATE_CREATE_CONFIRMED);
        cre.setData(name);
        EventDispatcher.forwardEvent(cre);
        ProjectSecurityTemplateModelData r = sM.getSelected();

        assertEquals(r.getName(), name);

        name = "edited template " + random;
        AppEvent edi = new AppEvent(AdminEvents.TEMPLATE_EDIT_CONFIRMED);
        edi.setData(name);
        EventDispatcher.forwardEvent(edi);

        assertEquals(r.getName(), name);

        r.delete();
    }

    @Test
    public void testAddAndRemove()
    {
        init();

        ProjectSecurityTemplateModelData r = selectTemplate();

        assertEquals(oM.getStore().getCount(), 0);

        RoleModelData role = aM.getStore().getAt(0);
        AppEvent sel1 = new AppEvent(AdminEvents.TEMP_ALL_SELECTED);
        sel1.setData(role);
        EventDispatcher.forwardEvent(sel1);

        assertEquals(aM.selected(), role);

        EventDispatcher.forwardEvent(AdminEvents.TEMP_ALL_ADD_CLICKED);

        assertEquals(oM.getStore().getCount(), 1);
        assertTrue(r.getRoleIds().contains(role.getId()));

        r.refresh();
        assertTrue(r.getRoleNames().contains(role.getName()));

        AppEvent sel2 = new AppEvent(AdminEvents.TEMP_OWN_SELECTED);
        sel2.setData(role);
        EventDispatcher.forwardEvent(sel2);
        EventDispatcher.forwardEvent(AdminEvents.TEMP_OWN_REMOVE_CLICKED);

        assertEquals(oM.getStore().getCount(), 0);
        assertTrue(aM.getStore().contains(role));

        r.delete();
    }

}
