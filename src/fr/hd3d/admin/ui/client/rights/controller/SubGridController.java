package fr.hd3d.admin.ui.client.rights.controller;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.rights.model.SubGridModel;
import fr.hd3d.admin.ui.client.rights.view.ISubGridView;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;


/**
 * A controller factoring all the event handling for a {@link fr.hd3d.admin.ui.client.rights.view.ISubGridView}.
 * 
 * @author HD3D
 * 
 * @param <D>
 *            the model data type
 * @param <M>
 *            the adequate model
 * @param <V>
 *            the adequate view
 */
public abstract class SubGridController<D extends Hd3dModelData, M extends SubGridModel<D>, V extends ISubGridView<D>>
        extends Controller
{

    /** the data model */
    protected M model;

    /** the view containing the grid */
    protected V view;

    /** indicates whether the grid content has been fully loaded from the server */
    protected boolean fullyLoaded = true;

    /** a list of constraints that will be applied to the model's store upon data loading */
    protected List<IUrlParameter> constraints = new ArrayList<IUrlParameter>();

    /**
     * Creates a controller for the specified view and model
     * 
     * @param model
     * @param view
     */
    public SubGridController(M model, V view)
    {
        this.model = model;
        this.view = view;
        registerEvents();
    }

    /**
     * Defines the path that should be use by the model's proxy to load the data.
     * 
     * @return the path to server data resource
     */
    protected abstract String path();

    /**
     * Meant to be overwritten by children, this method is called after the events have been forwarded to children. As
     * can {@link #handleEvent(AppEvent)}, it can only handle registered events.
     * 
     * @param event
     *            the event to handle
     */
    protected abstract void doHandleEvent(AppEvent event);

    /**
     * The method called upon constraint clearance, that is to say, when all the constraints have been removed upon user
     * request.
     */
    protected abstract void onConstraintsClearance();

    /**
     * Sets the constraints from a user request.
     */
    protected abstract void setupConstraints();

    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        this.forwardToChild(event);
        if (type == CommonEvents.PERMISSION_INITIALIZED)
        {
            this.onPermissionsInitialized(event);
        }
        else if (type == AdminEvents.REMAIN_IDLE)
        {
            // do precisely nothing
        }
        else if (type == model.allEventType())
        {
            clearConstraints();
            onConstraintsClearance();
            model.resetOffset();
            refreshStore();
            fullyLoaded = true;
            deselectAll();
        }
        else if (type == model.selectionEventType())
        {
            onItemSelection(event);
        }
        doHandleEvent(event);
    }

    /**
     * Called when the permissions have been acquired by the main controller, this method initializes the view and the
     * model, sending to the former the defines path in {@link #path()} which will be used to initialize the proxy.
     */
    protected void onPermissionsInitialized(AppEvent event)
    {
        model.setPath(path());
        model.initProxyData();
        view.setup();
    }

    /**
     * Clears the constraints and re-shows the view's grid (which might have been hidden if the model store has been
     * emptied).
     */
    protected void clearConstraints()
    {
        constraints.clear();
        view.showGrid();
    }

    /**
     * Makes the model refresh its store if the <code>fullyLoaded</code> field is false.
     */
    protected void refreshStore()
    {
        if (!fullyLoaded)
        {
            model.refreshData(constraints);
        }
    }

    /** Register all events the controller can handle. */
    protected void registerEvents()
    {
        registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
        registerEventTypes(model.allEventType());
        registerEventTypes(model.selectionEventType());
    }

    /**
     * Handles an 'item selected' event, sending the selected item to the model.
     * 
     * @param event
     *            the selection event
     */
    protected void onItemSelection(AppEvent event)
    {
        D item = event.<D> getData();
        model.setSelected(item);
        view.onItemSelection(item.getUserCanUpdate(), item.getUserCanDelete());
    }

    /**
     * Handles a selection clearance event, nullifying the model selected item.
     */
    protected void deselectAll()
    {
        model.setSelected(null);
        model.resetOffset();
        view.deselectAll(fullyLoaded);
    }

}
