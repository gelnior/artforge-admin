package fr.hd3d.admin.ui.client.templates.controller;

import fr.hd3d.admin.ui.client.templates.model.OwnedRoleModel;
import fr.hd3d.admin.ui.client.templates.view.IRoleTemplateView;


/**
 * A controller specifically designed to handle owned role templates.
 * 
 * @author HD3D
 * 
 */
public class OwnedRoleController extends RoleTemplateController<OwnedRoleModel>
{

    public OwnedRoleController(OwnedRoleModel model, IRoleTemplateView view)
    {
        super(model, view, true);
    }

    protected void onAddingDemand()
    {} // do precisely nothing

    protected void onRemovalDemand()
    {
        model.removeRoleFromTemplate();
    }

    protected void onConstraintsClearance()
    {
        view.hideGrid();
    }

}
