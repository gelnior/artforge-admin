package fr.hd3d.admin.ui.client.templates.view;

import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.FillData;
import com.extjs.gxt.ui.client.widget.layout.FillLayout;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.RowData;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.model.modeldata.SingleTemplateReader;
import fr.hd3d.admin.ui.client.templates.model.SecurityTemplateModel;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.admin.ui.client.view.ConfirmationDialog;
import fr.hd3d.admin.ui.client.view.LabelDialog;
import fr.hd3d.admin.ui.client.view.listener.ComboRelay;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.technical.ProjectSecurityTemplateModelData;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;
import fr.hd3d.common.ui.client.widget.ProjectCombobox;


/**
 * This panel is the main view of the 'Security Template Management' application. It handles all the security templates
 * related graphic events, including creation, edition and deletion requests, as well as security templates and projects
 * selection.
 * 
 * @author HD3D
 * 
 */
public class SecurityTemplatePanel extends ContentPanel implements ISecurityTemplateView
{

    /** the security template main model */
    private SecurityTemplateModel model;

    // roles panels
    private ContentPanel rolesPanel;
    /** the 'owned' roles content panel */
    private OwnedRolePanel ownedPanel;
    /** the 'other' roles content panel */
    private AllRolePanel allPanel;

    // security templates panel
    private ContentPanel tempsPanel;
    /** the selected security template's comment text area */
    private TextArea comment;
    private Button save;

    // templates tool bar
    private ToolBar templateToolBar;
    private Button create;
    private Button delete;
    private Button edit;
    /** the security template selection combo box */
    private ModelDataComboBox<ProjectSecurityTemplateModelData> combo;

    // project tool bar
    private ToolBar projectToolBar;
    private Button apply;
    /** the project selection combo box */
    private ProjectCombobox projects;

    // dialogs
    /** the security template creation request dialog */
    private Dialog createLine = LabelDialog.getCreationDialog(AdminMainView.MESSAGES.CreateTemplateWindow(),
            AdminEvents.TEMPLATE_CREATE_CONFIRMED);
    /** the security template edition request dialog */
    private LabelDialog editLine = LabelDialog.getEditionDialog(AdminMainView.MESSAGES.EditTemplateWindow(),
            AdminEvents.TEMPLATE_EDIT_CONFIRMED, null);
    /** the security template deletion confirmation dialog */
    private ConfirmationDialog deleteLine = new ConfirmationDialog(AdminMainView.CONSTANTS.DeleteTemplateWindow(),
            AdminMainView.MESSAGES.WantToDeleteTemplate(), AdminEvents.TEMPLATE_DELETE_CONFIRMED, null);

    /**
     * Builds up a security template main panel containing two role panels: one for the owned roles and one for the
     * others.
     * 
     * @param model
     *            the security templates main model
     * @param ownedPanel
     *            the 'owned' roles panel
     * @param allPanel
     *            the 'other' roles panel
     */
    public SecurityTemplatePanel(SecurityTemplateModel model, OwnedRolePanel ownedPanel, AllRolePanel allPanel)
    {
        this.model = model;

        this.ownedPanel = ownedPanel;
        this.allPanel = allPanel;
    }

    public void setup()
    {
        setStyle();

        tempsPanel = new ContentPanel();
        setupTempsPanel();

        rolesPanel = new ContentPanel();
        setupRolesPanel();

        BorderLayoutData bData1 = new BorderLayoutData(LayoutRegion.NORTH);
        bData1.setMargins(new Margins(0, 0, 0, 0));
        bData1.setSize(0.22f);
        add(tempsPanel, bData1);

        BorderLayoutData bData2 = new BorderLayoutData(LayoutRegion.CENTER);
        bData2.setMargins(new Margins(5, 0, 0, 0));
        bData2.setSize(0.78f);
        add(rolesPanel, bData2);
    }

    /**
     * Sets the layout, the borders etc.
     */
    protected void setStyle()
    {
        setBorders(false);
        setBodyBorder(false);
        setHeaderVisible(false);
        setLayout(new BorderLayout());
    }

    /**
     * Builds the security template north panel.
     */
    private void setupTempsPanel()
    {
        tempsPanel.setLayout(new FitLayout());
        tempsPanel.setBorders(true);
        tempsPanel.setBodyBorder(false);
        tempsPanel.getHeader().setBorders(false);
        tempsPanel.setHeaderVisible(true);
        tempsPanel.setHeading(AdminMainView.CONSTANTS.SecurityTemplates());

        templateToolBar = new ToolBar();
        templateToolBar.setStyleAttribute("padding", "5px");

        setupTemplatesBar();
        tempsPanel.setTopComponent(templateToolBar);

        setupCommentArea();

        projectToolBar = new ToolBar();
        projectToolBar.setStyleAttribute("padding", "5px");
        projectToolBar.setBorders(false);
        setupProjectsBar();
        tempsPanel.setBottomComponent(projectToolBar);
    }

    /**
     * Builds the central role panel.
     */
    private void setupRolesPanel()
    {
        rolesPanel.setLayout(new FillLayout(Style.Orientation.HORIZONTAL));
        rolesPanel.setBorders(false);
        rolesPanel.setBodyBorder(false);
        rolesPanel.setHeaderVisible(false);

        // initially, owned is disable
        rolesPanel.add(ownedPanel, new FillData());
        ownedPanel.disable();

        rolesPanel.add(allPanel, new FillData());
    }

    /**
     * Builds the security template operations bar.
     */
    @SuppressWarnings("unchecked")
    private void setupTemplatesBar()
    {
        combo = new ModelDataComboBox<ProjectSecurityTemplateModelData>(
                (IReader<ProjectSecurityTemplateModelData>) SingleTemplateReader.getReader());
        combo.setEmptyText(AdminMainView.CONSTANTS.SecTemp());
        combo.setWidth(300);
        combo.setTypeAhead(true);
        combo.setTriggerAction(TriggerAction.ALL);
        combo.addListener(Events.Select, new ComboRelay(AdminEvents.TEMPLATE_SELECTED, combo));

        templateToolBar.add(combo);
        templateToolBar.add(new SeparatorToolItem());

        create = setupButton(null, AdminMainView.MESSAGES.CreateTemplate(), "create-icon",
                AdminEvents.TEMPLATE_CREATE_CLICKED);
        if (!PermissionUtil.hasCreateRights(ProjectSecurityTemplateModelData.SIMPLE_CLASS_NAME))
        {
            create.disable();
        }
        templateToolBar.add(create);

        edit = setupButton(null, AdminMainView.MESSAGES.EditTemplate(), "edit-icon", AdminEvents.TEMPLATE_EDIT_CLICKED);
        edit.disable();
        templateToolBar.add(edit);

        delete = setupButton(null, AdminMainView.MESSAGES.DeleteTemplate(), "delete-icon",
                AdminEvents.TEMPLATE_DELETE_CLICKED);
        delete.disable();
        templateToolBar.add(delete);

        save = setupButton(null, AdminMainView.MESSAGES.SaveComment(), "save-icon", null);
        save.addSelectionListener(new SaveCommentRelay());
        save.disable();
        templateToolBar.add(save);
    }

    /**
     * Builds the security template comment area.
     */
    private void setupCommentArea()
    {
        ContentPanel container = new ContentPanel();
        container.setBorders(false);
        container.setBodyBorder(false);
        container.setHeaderVisible(false);
        container.setLayout(new RowLayout());

        LabelToolItem text = new LabelToolItem("<span style='color:blue'>" + AdminMainView.CONSTANTS.TemplateComment()
                + "</span>");
        container.add(text, new RowData(1, 0.32));

        comment = new TextArea();
        comment.setEmptyText("...");
        comment.setMaxLength(255);
        comment.disable();
        container.add(comment, new RowData(1, 0.68));

        tempsPanel.add(container, new FitData());
    }

    /**
     * Builds the project operations tool bar.
     */
    private void setupProjectsBar()
    {
        // projectToolBar.add(new FillToolItem());
        // projectToolBar.add(new LabelToolItem(AdminMainView.CONSTANTS.SelectProject()));
        // projectToolBar.add(new SeparatorToolItem());

        projects = new ProjectCombobox();
        projects.setEmptyText(AdminMainView.CONSTANTS.SelectProject());
        projects.setWidth(150);
        projects.setTypeAhead(true);
        projects.setTriggerAction(TriggerAction.ALL);
        projects.addListener(Events.Select, new ComboRelay(AdminEvents.TEMP_PROJECT_SELECTED, projects));
        projects.selectLastSelected();

        projects.disable();
        projectToolBar.add(projects);
        projectToolBar.add(new SeparatorToolItem());

        apply = setupButton(AdminMainView.CONSTANTS.ApplyTemplate(), AdminMainView.MESSAGES.ApplyTemplate(), null,
                AdminEvents.APPLY_TO_PROJECT);
        apply.disable();
        projectToolBar.add(apply);
    }

    public void selectNewTemplate()
    {
        ProjectSecurityTemplateModelData selected = model.getSelected();
        combo.getStore().add(selected);
        combo.setValue(selected);
        onTemplateSelection(selected.getUserCanUpdate(), selected.getUserCanDelete());
    }

    public void reselectTemplate()
    {
        combo.getStore().add(model.getSelected());
        combo.setValue(model.getSelected());
    }

    public void deselectTemplate()
    {
        combo.getStore().remove(model.getSelected());
    }

    public void onTemplateSelection(boolean canUpdate, boolean canDelete)
    {
        ownedPanel.setHeading(model.getSelected().getName() + " - " + AdminMainView.CONSTANTS.OwnedRoles());
        ownedPanel.enable();

        if (canUpdate)
        {
            edit.enable();
        }
        if (canDelete)
        {
            delete.enable();
        }

        projects.enable();

        comment.setValue(model.getSelected().getComment());
        if (canUpdate)
        {
            comment.enable();
            save.enable();
        }
    }

    public void onTemplateDeletion()
    {
        combo.setValue(null);
        deselectTemplate();
    }

    public void onTemplateDeselection()
    {
        ownedPanel.setHeading(AdminMainView.CONSTANTS.OwnedRoles());
        ownedPanel.disable();

        edit.disable();
        delete.disable();

        projects.disable();

        comment.setValue(comment.getEmptyText());
        comment.disable();
        save.disable();
    }

    public void onProjectSelection(boolean canApply)
    {
        if (canApply)
        {
            apply.enable();
        }
    }

    public void onCreateDemand()
    {
        createLine.setPosition(create.getAbsoluteLeft(), create.getAbsoluteTop() + 30);
        createLine.show();
    }

    public void onEditDemand()
    {
        editLine.setDefaultText((String) model.getSelected().getName());
        editLine.setPosition(edit.getAbsoluteLeft(), edit.getAbsoluteTop() + 30);
        editLine.show();
    }

    public void onDeleteDemand()
    {
        deleteLine.setHeaderAdding(model.getSelected().getName());
        deleteLine.setPosition(delete.getAbsoluteLeft(), delete.getAbsoluteTop() + 30);
        deleteLine.show();
    }

    /**
     * Builds a button with proper listener, icon, text...
     * 
     * @return the button with proper settings
     */
    private Button setupButton(String text, String toolTip, String iconCSS, EventType eventType)
    {
        Button result = new Button();
        if (text != null)
        {
            result.setText(text);
        }
        if (toolTip != null)
        {
            result.setToolTip(toolTip);
        }
        if (iconCSS != null)
        {
            result.setIconStyle(iconCSS);
        }
        if (eventType != null)
        {
            result.addSelectionListener(new ButtonClickListener(eventType));
        }
        return result;
    }

    /**
     * A specific button listener for forwarding the 'save comment' request.
     * 
     * @author HD3D
     * 
     */
    class SaveCommentRelay extends ButtonClickListener
    {
        public SaveCommentRelay()
        {
            super(AdminEvents.TEMPLATE_COMMENT_SAVE);
        }

        public void componentSelected(ButtonEvent be)
        {
            event.setData(comment.getValue());
            EventDispatcher.forwardEvent(event);
        }
    }

}
