package fr.hd3d.admin.ui.client.model.modeldata;

import fr.hd3d.common.ui.client.modeldata.reader.Hd3dPagingJsonReader;


public class GenericPagingReader extends Hd3dPagingJsonReader<GenericModelData>
{

    public GenericPagingReader()
    {
        super(GenericModelData.getModelType());
    }

    public GenericModelData newModelInstance()
    {
        return new GenericModelData();
    }

}
