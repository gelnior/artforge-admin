package fr.hd3d.admin.ui.test.modeldata;

import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.Hd3dListJsonReaderMock;


public class RoleReaderMock extends Hd3dListJsonReaderMock<RoleModelData>
{

    public RoleReaderMock()
    {
        super();
        setModelType(RoleModelData.getModelType());
    }

    public RoleModelData newModelInstance()
    {
        return new RoleModelData();
    }

}
