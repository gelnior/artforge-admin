package fr.hd3d.admin.ui.client.rights.view;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.model.modeldata.SingleProjectReader;
import fr.hd3d.admin.ui.client.rights.model.ResourceGroupModel;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.admin.ui.client.view.listener.ComboRelay;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.widget.PersonComboBox;
import fr.hd3d.common.ui.client.widget.SimpleProjectComboBox;


/**
 * The <code>SubGridPanel</code> specification for a resource group model data containing grid panel. It also provides
 * with two constraints defining elements: a person combo box and a project combo box.
 * 
 * @author HD3D
 * 
 */
public class ResourceGroupPanel extends SubGridPanel<ResourceGroupModelData, ResourceGroupModel> implements
        IResourceGroupView
{
    /** the projects combo box */
    protected SimpleProjectComboBox projects;
    /** the persons combo box */
    protected PersonComboBox persons = new PersonComboBox();

    public ResourceGroupPanel(ResourceGroupModel model)
    {
        super(model);
    }

    protected String nameField()
    {
        return ResourceGroupModelData.NAME_FIELD;
    }

    protected void setStyle()
    {
        setHeading(AdminMainView.CONSTANTS.Groups());
        super.setStyle();
    }

    @SuppressWarnings("unchecked")
    protected void setActionToolBar()
    {
        super.setActionToolBar();
        actionBar.add(new SeparatorToolItem());
        projects = new SimpleProjectComboBox((IReader<ProjectModelData>) SingleProjectReader.getReader());
        projects.setEmptyText(AdminMainView.CONSTANTS.Proj());
        projects.setWidth(150);
        projects.setTypeAhead(true);
        projects.setTriggerAction(TriggerAction.ALL);
        projects.addListener(Events.Select, new ComboRelay(AdminEvents.PROJECT_SELECTED, projects));
        actionBar.add(projects);

        actionBar.add(new SeparatorToolItem());

        persons.setEmptyText("Display groups for person...");
        persons.addListener(Events.Select, new ComboRelay(AdminEvents.PERSON_SELECTED, persons));
        actionBar.add(persons);

    }

    public void resetComboBoxes(boolean person, boolean project)
    {
        if (person)
        {
            persons.clearSelections();
        }
        if (project)
        {
            projects.clearSelections();
        }
    }

    public void onItemSelection(boolean canUpdate, boolean canDelete)
    {} // do nothing
}
