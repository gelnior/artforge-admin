package fr.hd3d.admin.ui.client.templates.view;

import fr.hd3d.admin.ui.client.templates.model.RoleTemplateModel;
import fr.hd3d.admin.ui.client.view.AdminMainView;


/**
 * This panel contains the grid with the roles not owned by the selected security template.
 * 
 * @author HD3D
 * 
 */
public class AllRolePanel extends RoleTemplatePanel
{

    /**
     * Builds a role panel with an 'add' button and without 'remove' button
     * 
     * @param model
     *            the adequate role model
     */
    public AllRolePanel(RoleTemplateModel model)
    {
        super(model, true, false);
    }

    protected void setStyle()
    {
        setHeading(AdminMainView.CONSTANTS.AllRoles());
        super.setStyle();
    }

}
