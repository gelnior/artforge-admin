package fr.hd3d.admin.ui.client.rights.view;

import com.extjs.gxt.ui.client.event.Events;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.rights.model.RoleModel;
import fr.hd3d.admin.ui.client.view.AbstractRolePanel;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.common.ui.client.listener.EventRelay;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationDialog;


/**
 * A specification of the <code>AbstractRolePanel</code> that can adapt to resource group selection.
 * 
 * @author HD3D
 * 
 */
public class RolePanel extends AbstractRolePanel implements IRoleView
{

    /** the relation editor dialog */
    private RelationDialog innerRelDiag = new RelationDialog();

    /** whether a group has been selected */
    private boolean groupSelected;
    /** whether the user can update selected group */
    private boolean selectedGroupUpdate;

    /**
     * Builds a role panel with an 'add' and a 'remove' button.
     * 
     * @param model
     *            the adequate role model
     */
    public RolePanel(RoleModel model)
    {
        super(model, true, true);
    }

    protected void setStyle()
    {
        setHeading(AdminMainView.CONSTANTS.Roles());
        super.setStyle();

        innerRelDiag.addListener(Events.Hide, new EventRelay(AdminEvents.SERVER_SELECTED_GROUP_MODIFICATION, null));
    }

    public void onItemSelection(boolean canUpdate, boolean canDelete)
    {
        super.onItemSelection(canUpdate, canDelete);
        if (groupSelected)
        {
            if (selectedGroupUpdate && canUpdate)
            {
                removeButton.enable();
            }
            else
            {
                removeButton.disable();
            }
        }
    }

    public void onGroupSelection(String groupName, boolean canUpdate)
    {
        setHeading(AdminMainView.CONSTANTS.Roles() + " - " + groupName);
        if (canUpdate)
        {
            addButton.enable();
        }
        else
        {
            addButton.disable();
        }
        groupSelected = true;
        selectedGroupUpdate = canUpdate;
    }

    public void onGroupDeselection()
    {
        setHeading(AdminMainView.CONSTANTS.Roles());
        removeButton.disable();
        addButton.disable();
        groupSelected = false;
    }

    /**
     * When relation is requested for a given group, the relation tool is displayed for this group. Relation tool
     * refresh the group before displaying to ensure thats its relations are up to date.
     */
    public void openGroupRelationTool(final ResourceGroupModelData group)
    {
        innerRelDiag.show(group);
        innerRelDiag.setHeading(innerRelDiag.getHeading().split(" - ")[0] + " - " + group.getName());
    }
}
