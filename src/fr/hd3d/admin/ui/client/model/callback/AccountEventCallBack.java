package fr.hd3d.admin.ui.client.model.callback;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.common.ui.client.service.callback.EventCallback;


/**
 * A call-back specifically designed for handling all the user account requests.
 * 
 * @author HD3D
 * 
 */
public class AccountEventCallBack extends EventCallback
{

    /**
     * Creates a call-back with sending a 'refresh user list' event in case of success.
     */
    public AccountEventCallBack()
    {
        super(AdminEvents.REFRESH_USERS_LIST);
    }

}
