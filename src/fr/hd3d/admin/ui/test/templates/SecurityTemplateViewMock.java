package fr.hd3d.admin.ui.test.templates;

import fr.hd3d.admin.ui.client.templates.model.SecurityTemplateModel;
import fr.hd3d.admin.ui.client.templates.view.ISecurityTemplateView;


public class SecurityTemplateViewMock implements ISecurityTemplateView
{

    SecurityTemplateModel model;

    public boolean tempSelected = false;
    public int create = 0;
    public int delete = 0;
    public int edit = 0;
    public boolean init = false;
    public boolean projSelected = false;

    public SecurityTemplateViewMock(SecurityTemplateModel model)
    {
        this.model = model;
    }

    public void deselectTemplate()
    {
        tempSelected = false;
    }

    public void onCreateDemand()
    {
        create++;
    }

    public void onDeleteDemand()
    {
        delete++;
    }

    public void onEditDemand()
    {
        edit++;
    }

    public void onProjectSelection(boolean canApply)
    {
        projSelected = true;
    }

    public void onTemplateDeletion()
    {}

    public void onTemplateDeselection()
    {
        tempSelected = false;
    }

    public void onTemplateSelection(boolean canUpdate, boolean canDelete)
    {
        tempSelected = true;
    }

    public void reselectTemplate()
    {
        tempSelected = true;
    }

    public void selectNewTemplate()
    {
        tempSelected = true;
    }

    public void setup()
    {
        init = true;
    }

}
