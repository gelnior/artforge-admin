package fr.hd3d.admin.ui.client.view;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;

import fr.hd3d.common.ui.client.listener.ButtonClickListener;


/**
 * A simple dialog that asks the user for a confirmation upon a certain action.
 * 
 * @author HD3D
 * 
 */
public class ConfirmationDialog extends Dialog
{

    /** the dialog heading */
    protected String heading;
    /** the event type associated with the 'OK' button */
    protected EventType okEventType;
    /** the event type associated with the 'cancel' button */
    protected EventType cancelEventType;

    /** the message inside the dialog */
    protected String message;
    protected LabelToolItem label = new LabelToolItem();

    /**
     * The constructor of the confirmation dialog. It builds the dialog and readies it for display.
     * 
     * @param heading
     *            the dialog heading
     * @param message
     *            the dialog confirmation message
     * @param okEventType
     *            the event type when 'OK' is chosen
     * @param cancelEventType
     *            the event type when 'cancel' is chosen
     */
    public ConfirmationDialog(String heading, String message, EventType okEventType, EventType cancelEventType)
    {
        this.heading = heading;
        this.message = message;
        this.okEventType = okEventType;
        this.cancelEventType = cancelEventType;

        setup();
    }

    /**
     * Adds a part to the heading.
     * 
     * @param adding
     *            the part to add to the heading
     */
    public void setHeaderAdding(String adding)
    {
        this.setHeading(heading + " - " + adding);
    }

    /**
     * Builds the dialog content.
     */
    protected void setup()
    {
        setHeading(heading);
        setStyle();
        setHideOnButtonClick(true);
        setModal(true);
        setButtons(Dialog.OKCANCEL);

        Button ok = getButtonById(OK);
        setupOkButton(ok);

        Button cancel = getButtonById(CANCEL);
        cancel.setText(AdminMainView.CONSTANTS.Cancel());
        cancel.addListener(Events.Select, new ButtonClickListener(cancelEventType));
    }

    /**
     * Sets the label message.
     */
    protected void setStyle()
    {
        label.setLabel(message);
        add(label);
    }

    /**
     * Sets the 'OK' button, adding the proper listener to it and allowing modification by children class overriding.
     * 
     * @param ok
     *            the 'OK' button
     */
    protected void setupOkButton(Button ok)
    {
        ok.addListener(Events.Select, new ButtonClickListener(okEventType));
    }

}
