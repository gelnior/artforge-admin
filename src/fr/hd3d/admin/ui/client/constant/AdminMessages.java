package fr.hd3d.admin.ui.client.constant;

/**
 * Interface to represent the constants contained in resource bundle:
 * '/home/frank.rousseau/workspace/Admin/war/WEB-INF/classes/fr/hd3d/admin/ui/client/constant/AdminMessages.properties'.
 */
public interface AdminMessages extends com.google.gwt.i18n.client.Constants
{

    /**
     * Translated "add a new ban string to selected role".
     * 
     * @return translated "add a new ban string to selected role"
     */
    @DefaultStringValue("add a new ban string to selected role")
    @Key("AddBan")
    String AddBan();

    /**
     * Translated "add a new permission string to selected role".
     * 
     * @return translated "add a new permission string to selected role"
     */
    @DefaultStringValue("add a new permission string to selected role")
    @Key("AddPermission")
    String AddPermission();

    /**
     * Translated "add an existing role to selected group".
     * 
     * @return translated "add an existing role to selected group"
     */
    @DefaultStringValue("add an existing role to selected group")
    @Key("AddRole")
    String AddRole();

    /**
     * Translated "apply security template to selected project".
     * 
     * @return translated "apply security template to selected project"
     */
    @DefaultStringValue("apply security template to selected project")
    @Key("ApplyTemplate")
    String ApplyTemplate();

    /**
     * Translated "AdminMessages".
     * 
     * @return translated "AdminMessages"
     */
    @DefaultStringValue("AdminMessages")
    @Key("ClassName")
    String ClassName();

    /**
     * Translated "create an account for selected user".
     * 
     * @return translated "create an account for selected user"
     */
    @DefaultStringValue("create an account for selected user")
    @Key("CreateAccountTip")
    String CreateAccountTip();

    /**
     * Translated "create a new role...".
     * 
     * @return translated "create a new role..."
     */
    @DefaultStringValue("create a new role...")
    @Key("CreateRole")
    String CreateRole();

    /**
     * Translated "Create new role with name:".
     * 
     * @return translated "Create new role with name:"
     */
    @DefaultStringValue("Create new role with name:")
    @Key("CreateRoleWindow")
    String CreateRoleWindow();

    /**
     * Translated "create a new security template...".
     * 
     * @return translated "create a new security template..."
     */
    @DefaultStringValue("create a new security template...")
    @Key("CreateTemplate")
    String CreateTemplate();

    /**
     * Translated "Create a new security template with name:".
     * 
     * @return translated "Create a new security template with name:"
     */
    @DefaultStringValue("Create a new security template with name:")
    @Key("CreateTemplateWindow")
    String CreateTemplateWindow();

    /**
     * Translated "Password is default".
     * 
     * @return translated "Password is default"
     */
    @DefaultStringValue("Password is default")
    @Key("DefaultPasswordStatus")
    String DefaultPasswordStatus();

    /**
     * Translated "delete selected user's account".
     * 
     * @return translated "delete selected user's account"
     */
    @DefaultStringValue("delete selected user's account")
    @Key("DeleteAccountTip")
    String DeleteAccountTip();

    /**
     * Translated "delete selected role".
     * 
     * @return translated "delete selected role"
     */
    @DefaultStringValue("delete selected role")
    @Key("DeleteRole")
    String DeleteRole();

    /**
     * Translated "delete selected security template...".
     * 
     * @return translated "delete selected security template..."
     */
    @DefaultStringValue("delete selected security template...")
    @Key("DeleteTemplate")
    String DeleteTemplate();

    /**
     * Translated "change selected user's password".
     * 
     * @return translated "change selected user's password"
     */
    @DefaultStringValue("change selected user's password")
    @Key("EditAccountTip")
    String EditAccountTip();

    /**
     * Translated "edit selected role's name...".
     * 
     * @return translated "edit selected role's name..."
     */
    @DefaultStringValue("edit selected role's name...")
    @Key("EditRole")
    String EditRole();

    /**
     * Translated "Edit role name:".
     * 
     * @return translated "Edit role name:"
     */
    @DefaultStringValue("Edit role name:")
    @Key("EditRoleWindow")
    String EditRoleWindow();

    /**
     * Translated "edit selected security template name...".
     * 
     * @return translated "edit selected security template name..."
     */
    @DefaultStringValue("edit selected security template name...")
    @Key("EditTemplate")
    String EditTemplate();

    /**
     * Translated "Edit security template name:".
     * 
     * @return translated "Edit security template name:"
     */
    @DefaultStringValue("Edit security template name:")
    @Key("EditTemplateWindow")
    String EditTemplateWindow();

    /**
     * Translated "User's account exists but is external".
     * 
     * @return translated "User's account exists but is external"
     */
    @DefaultStringValue("User's account exists but is external")
    @Key("ExternalAccountStatus")
    String ExternalAccountStatus();

    /**
     * Translated "User has more than one account".
     * 
     * @return translated "User has more than one account"
     */
    @DefaultStringValue("User has more than one account")
    @Key("MoreAccountsStatus")
    String MoreAccountsStatus();

    /**
     * Translated "User has no account".
     * 
     * @return translated "User has no account"
     */
    @DefaultStringValue("User has no account")
    @Key("NoAccountStatus")
    String NoAccountStatus();

    /**
     * Translated "no e-mail address is registered".
     * 
     * @return translated "no e-mail address is registered"
     */
    @DefaultStringValue("no e-mail address is registered")
    @Key("NoEMailAddress")
    String NoEMailAddress();

    /**
     * Translated "You are not allowed to change the {0} right on {1}.".
     * 
     * @return translated "You are not allowed to change the {0} right on {1}."
     */
    @DefaultStringValue("You are not allowed to change the {0} right on {1}.")
    @Key("NoRightToClickOnPerm")
    String NoRightToClickOnPerm();

    /**
     * Translated "Password is null".
     * 
     * @return translated "Password is null"
     */
    @DefaultStringValue("Password is null")
    @Key("NullPasswordStatus")
    String NullPasswordStatus();

    /**
     * Translated "text field must not be empty".
     * 
     * @return translated "text field must not be empty"
     */
    @DefaultStringValue("text field must not be empty")
    @Key("NullTextFieldError")
    String NullTextFieldError();

    /**
     * Translated "OK".
     * 
     * @return translated "OK"
     */
    @DefaultStringValue("OK")
    @Key("OkAccountStatus")
    String OkAccountStatus();

    /**
     * Translated "Open relation tool...".
     * 
     * @return translated "Open relation tool..."
     */
    @DefaultStringValue("Open relation tool...")
    @Key("OpenRelationTool")
    String OpenRelationTool();

    /**
     * Translated "Password must be more than 3 chars long".
     * 
     * @return translated "Password must be more than 3 chars long"
     */
    @DefaultStringValue("Password must be more than 3 chars long")
    @Key("PasswordInvalidInput")
    String PasswordInvalidInput();

    /**
     * Translated "Password field must be filled".
     * 
     * @return translated "Password field must be filled"
     */
    @DefaultStringValue("Password field must be filled")
    @Key("PasswordMustNotBeNull")
    String PasswordMustNotBeNull();

    /**
     * Translated "?".
     * 
     * @return translated "?"
     */
    @DefaultStringValue("?")
    @Key("QMark")
    String QMark();

    /**
     * Translated "redo last undone action on selected role's properties".
     * 
     * @return translated "redo last undone action on selected role's properties"
     */
    @DefaultStringValue("redo last undone action on selected role's properties")
    @Key("RedoList")
    String RedoList();

    /**
     * Translated "refresh user list".
     * 
     * @return translated "refresh user list"
     */
    @DefaultStringValue("refresh user list")
    @Key("RefreshUsersTip")
    String RefreshUsersTip();

    /**
     * Translated "reload all items".
     * 
     * @return translated "reload all items"
     */
    @DefaultStringValue("reload all items")
    @Key("ReloadAll")
    String ReloadAll();

    /**
     * Translated "remove right from selected role".
     * 
     * @return translated "remove right from selected role"
     */
    @DefaultStringValue("remove right from selected role")
    @Key("RemoveRight")
    String RemoveRight();

    /**
     * Translated "remove selected role from selected group".
     * 
     * @return translated "remove selected role from selected group"
     */
    @DefaultStringValue("remove selected role from selected group")
    @Key("RemoveRole")
    String RemoveRole();

    /**
     * Translated "Repeated password is not equal to main input".
     * 
     * @return translated "Repeated password is not equal to main input"
     */
    @DefaultStringValue("Repeated password is not equal to main input")
    @Key("RepeatPasswordError")
    String RepeatPasswordError();

    /**
     * Translated "Selected role's properties have changed".
     * 
     * @return translated "Selected role's properties have changed"
     */
    @DefaultStringValue("Selected role's properties have changed")
    @Key("RolePropertiesChange")
    String RolePropertiesChange();

    /**
     * Translated "save security template's comment".
     * 
     * @return translated "save security template's comment"
     */
    @DefaultStringValue("save security template's comment")
    @Key("SaveComment")
    String SaveComment();

    /**
     * Translated "save changes on selected role's properties".
     * 
     * @return translated "save changes on selected role's properties"
     */
    @DefaultStringValue("save changes on selected role's properties")
    @Key("SaveList")
    String SaveList();

    /**
     * Translated "{0} security template was correctly applied to {1} project.".
     * 
     * @return translated "{0} security template was correctly applied to {1} project."
     */
    @DefaultStringValue("{0} security template was correctly applied to {1} project.")
    @Key("TemplateSuccessfullyApplied")
    String TemplateSuccessfullyApplied();

    /**
     * Translated "Type text".
     * 
     * @return translated "Type text"
     */
    @DefaultStringValue("Type text")
    @Key("TypeText")
    String TypeText();

    /**
     * Translated "undo last action on selected role's properties".
     * 
     * @return translated "undo last action on selected role's properties"
     */
    @DefaultStringValue("undo last action on selected role's properties")
    @Key("UndoList")
    String UndoList();

    /**
     * Translated "Status is unknown".
     * 
     * @return translated "Status is unknown"
     */
    @DefaultStringValue("Status is unknown")
    @Key("UnknownStatus")
    String UnknownStatus();

    /**
     * Translated "Do you confirm you want to delete this account?".
     * 
     * @return translated "Do you confirm you want to delete this account?"
     */
    @DefaultStringValue("Do you confirm you want to delete this account?")
    @Key("WantToDeleteAccount")
    String WantToDeleteAccount();

    /**
     * Translated "Do you confirm you want to delete this template?".
     * 
     * @return translated "Do you confirm you want to delete this template?"
     */
    @DefaultStringValue("Do you confirm you want to delete this template?")
    @Key("WantToDeleteTemplate")
    String WantToDeleteTemplate();

    /**
     * Translated "Do you want to save changes on".
     * 
     * @return translated "Do you want to save changes on"
     */
    @DefaultStringValue("Do you want to save changes on")
    @Key("WantToSaveChanges")
    String WantToSaveChanges();
}
