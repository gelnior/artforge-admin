package fr.hd3d.admin.ui.test.users;

import org.junit.Test;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.admin.ui.client.model.modeldata.SinglePersonReader;
import fr.hd3d.admin.ui.client.users.controller.AccountsController;
import fr.hd3d.admin.ui.client.users.controller.UsersController;
import fr.hd3d.admin.ui.client.users.model.AccountsModel;
import fr.hd3d.admin.ui.test.modeldata.PersonReaderMock;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;
import fr.hd3d.common.ui.test.UITestCase;


public class UsersControllerTest extends UITestCase
{

    int random = (int) (1000 * Math.random());

    AccountsModel aM;
    AccountViewMock aV;
    int pCount;

    UsersViewMock mockView;

    public void setUp()
    {
        super.setUp();

        // set super user
        PermissionUtil.setDebugOn();

        // set mock reader
        SinglePersonReader.setInstance(new PersonReaderMock());

        // roles init
        aM = new AccountsModel();
        aV = new AccountViewMock(aM);
        AccountsController aController = new AccountsController(aM, aV);

        mockView = new UsersViewMock(aV);
        UsersController controller = new UsersController(mockView);
        controller.addChild(aController);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    private void init()
    {
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);

        // save total count
        pCount = aM.getStore().getCount();
    }

    private PersonModelData selectUser()
    {
        int i = 0;
        PersonModelData r = aM.getStore().getAt(i++);
        while (r.getLogin().equals("root"))
        {
            r = aM.getStore().getAt(i++);
        }
        AppEvent sel = new AppEvent(AdminEvents.USER_SELECTED);
        sel.setData(r);
        EventDispatcher.forwardEvent(sel);

        return r;
    }

    @Test
    public void testInit()
    {
        init();

        assertTrue(mockView.init);
        assertTrue(aV.init);
        assertTrue(pCount != 0);
    }

    @Test
    public void testNormalSelection()
    {
        init();

        PersonModelData p = selectUser();

        assertEquals(p, aV.user);
        assertEquals(p, aM.getSelected());

        EventDispatcher.forwardEvent(AdminEvents.REFRESH_USERS_LIST);

        assertNull(aV.user);
    }

    @Test
    public void testDemands()
    {
        init();

        EventDispatcher.forwardEvent(AdminEvents.USER_CREATE_ACCOUNT_CLICKED);

        assertEquals(aV.demands % 4, 3);

        EventDispatcher.forwardEvent(AdminEvents.USER_DELETE_ACCOUNT_CLICKED);

        assertEquals(aV.demands % 4, 1);

        EventDispatcher.forwardEvent(AdminEvents.USER_EDIT_ACCOUNT_CLICKED);

        assertEquals(aV.demands % 4, 2);
    }

    // @Test
    // public void testDeletenCreateAccount()
    // {
    // init();
    //
    // PersonModelData p = selectUser();
    //
    // EventDispatcher.forwardEvent(AdminEvents.USER_DELETE_ACCOUNT_CONFIRMED);
    //
    // int i = 0;
    // PersonModelData r = aM.getStore().getAt(i++);
    // while (r.getAccountStatus().longValue() > -1)
    // {
    // r = aM.getStore().getAt(i++);
    // }
    //
    // assertEquals(r.getAccountStatus().longValue(), -1);
    //
    // AppEvent sel1 = new AppEvent(AdminEvents.USER_SELECTED);
    // sel1.setData(r);
    // EventDispatcher.forwardEvent(sel1);
    //
    // AccountFormModel m = new AccountFormModel(p.getLogin());
    // m.createAccount();
    //
    // r = aM.getStore().getAt(--i);
    //
    // assertEquals(m.getPrincipal(), r.getLogin());
    // assertEquals(r.getAccountStatus().longValue(), -2);
    // }

    // @Test
    // public void testChangePassword()
    // {
    // init();
    //
    // int i = 0;
    // PersonModelData r = aM.getStore().getAt(i++);
    // while (r.getAccountStatus().longValue() != -2)
    // {
    // r = aM.getStore().getAt(i++);
    // }
    // AppEvent sel = new AppEvent(AdminEvents.USER_SELECTED);
    // sel.setData(r);
    // EventDispatcher.forwardEvent(sel);
    //
    // assertNotNull(r.getLogin());
    // assertEquals(r.getAccountStatus().longValue(), -2);
    //
    // AccountFormModel m = new AccountFormModel(r.getLogin());
    // m.setNewPass(m.getPrincipal() + "pass");
    // m.editAccount();
    //
    // r = aM.getStore().getAt(--i);
    //
    // assertEquals(m.getPrincipal(), r.getLogin());
    // assertEquals(r.getAccountStatus().longValue(), 0);
    // }

}
