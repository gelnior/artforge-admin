package fr.hd3d.admin.ui.test.modeldata;

import fr.hd3d.common.ui.client.modeldata.technical.ProjectSecurityTemplateModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.Hd3dListJsonReaderMock;


public class ProjectSecurityTemplateReaderMock extends Hd3dListJsonReaderMock<ProjectSecurityTemplateModelData>
{

    public ProjectSecurityTemplateReaderMock()
    {
        super();
        setModelType(ProjectSecurityTemplateModelData.getModelType());
    }

    public ProjectSecurityTemplateModelData newModelInstance()
    {
        return new ProjectSecurityTemplateModelData();
    }

}
