package fr.hd3d.admin.ui.client.templates.model;

import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.admin.ui.client.event.AdminEvents;
import fr.hd3d.common.ui.client.modeldata.technical.ProjectSecurityTemplateModelData;


/**
 * The specific model for handling the role templates not owned by the selected security template. It provides a method
 * for adding roles to the security template.
 * 
 * @author HD3D
 * 
 */
public class AllRoleModel extends RoleTemplateModel
{

    public AllRoleModel(SecurityTemplateModel parent)
    {
        super(parent);
        parent.setAll(this);
    }

    /**
     * Adds selected role to selected security template, actually adding the role from the owned store, removing it from
     * the other store and saving the security template afterwards.
     */
    public void addRoleToTemplate()
    {
        ProjectSecurityTemplateModelData template = getSelectedTemplate();
        template.getRoleIds().add(selected.getId());
        store.remove(selected);
        parent.getOwned().getStore().add(selected);
        selected = null;
        template.save();
    }

    public EventType refreshNeededEvent()
    {
        return AdminEvents.TEMP_ALL_SERVER_MODIFICATION;
    }

    public EventType refreshAllEvent()
    {
        return AdminEvents.TEMP_ALL_REFRESH_CLICKED;
    }

    public EventType addingClickEvent()
    {
        return AdminEvents.TEMP_ALL_ADD_CLICKED;
    }

    public EventType creationClickEvent()
    {
        return AdminEvents.TEMP_ALL_CREATE_CLICKED;
    }

    public EventType creationConfirmEvent()
    {
        return AdminEvents.TEMP_ALL_CREATE_CONFIRMED;
    }

    public EventType deleteClickEvent()
    {
        return AdminEvents.TEMP_ALL_DELETE_CLICKED;
    }

    public EventType editionClickEvent()
    {
        return AdminEvents.TEMP_ALL_EDIT_CLICKED;
    }

    public EventType editionConfirmEvent()
    {
        return AdminEvents.TEMP_ALL_EDIT_CONFIRMED;
    }

    public EventType removeClickEvent()
    {
        return AdminEvents.REMAIN_IDLE;
    }

    public EventType allEventType()
    {
        return AdminEvents.REMAIN_IDLE;
    }

    public EventType selectionEventType()
    {
        return AdminEvents.TEMP_ALL_SELECTED;
    }

}
