package fr.hd3d.admin.ui.client.rights.view;

import fr.hd3d.admin.ui.client.perms.view.PermsPanel;
import fr.hd3d.admin.ui.client.rights.model.RightsPermsModel;


/**
 * The permissions panel specifically designed for the 'Roles and Rights Management' application.
 * 
 * @author HD3D
 * 
 */
public class RightsPermsPanel extends PermsPanel
{

    public RightsPermsPanel(RightsPermsModel model)
    {
        super(model);
    }

}
