package fr.hd3d.admin.ui.client.factoring;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.modeldata.NameModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;


@Deprecated
public abstract class OldPermsModel
{

    protected RoleModelData role;
    protected NameModelData selected;
    protected ListStore<NameModelData> permStore;
    protected ListStore<NameModelData> banStore;

    public OldPermsModel()
    {
        permStore = new ListStore<NameModelData>();
        banStore = new ListStore<NameModelData>();
    }

    public abstract boolean isTemplate();

    public ListStore<NameModelData> getPermStore()
    {
        return this.permStore;
    }

    public ListStore<NameModelData> getBanStore()
    {
        return this.banStore;
    }

    public void setRole(RoleModelData role)
    {
        this.role = role;
    }

    public void setSelected(NameModelData selected)
    {
        this.selected = selected;
    }

    public NameModelData selected()
    {
        return selected;
    }

    public void refreshStore()
    {
        permStore.removeAll();
        banStore.removeAll();

        if (role != null)
        {
            List<String> perms = role.getPermissions();
            for (String perm : perms)
            {
                permStore.add(new NameModelData(perm));
            }
            List<String> bans = role.getBans();
            for (String ban : bans)
            {
                banStore.add(new NameModelData(ban));
            }
        }
    }

    public void refreshStore(NameModelData right, boolean add, boolean isBan)
    {
        // here role is a priori not null
        if (add && isBan)
        {
            banStore.add(right);
        }
        else if (add && !isBan)
        {
            permStore.add(right);
        }
        else if (!add && isBan)
        {
            banStore.remove(right);
        }
        else if (!add && !isBan)
        {
            permStore.remove(right);
        }
    }

    public String getRoleName()
    {
        if (role == null)
        {
            return "";
        }
        return role.getName();
    }

    public RoleModelData getRole()
    {
        return role;
    }

    public void saveChanges(RoleModelData toSave)
    {
        if (toSave != null)
        {
            toSave.save();
        }
        else
        {
            role.save();
        }
    }

    public abstract EventType addPermissionDemandEvent();

    public abstract EventType addPermissionEvent();

    public abstract EventType addBanDemandEvent();

    public abstract EventType addBanEvent();

    public abstract EventType removePermissionEvent();

    public abstract EventType removeBanEvent();

    public abstract EventType saveEvent();

    public abstract EventType undoEvent();

    public abstract EventType redoEvent();

    public abstract EventType fullUndoEvent();

}
