package fr.hd3d.admin.ui.client.model.modeldata;

import fr.hd3d.common.ui.client.modeldata.reader.Hd3dListJsonReader;


public class GenericReader extends Hd3dListJsonReader<GenericModelData>
{

    public GenericReader()
    {
        super(GenericModelData.getModelType());
    }

    public GenericModelData newModelInstance()
    {
        return new GenericModelData();
    }

}
