package fr.hd3d.admin.ui.client.mountpoint.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid.ClicksToEdit;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;

import fr.hd3d.admin.ui.client.controller.SubController;
import fr.hd3d.admin.ui.client.mountpoint.controller.MountPointController;
import fr.hd3d.admin.ui.client.mountpoint.controller.MountPointEvents;
import fr.hd3d.admin.ui.client.mountpoint.model.MountPointModel;
import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.admin.ui.client.view.SubView;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.asset.MountPointModelData;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.grid.BaseEditorGrid;
import fr.hd3d.common.ui.client.widget.grid.MultiLineCellSelectionModel;
import fr.hd3d.common.ui.client.widget.grid.SelectableGridView;


/**
 * The 'Security Templates Management' user interface.
 * 
 * @author HD3D
 * 
 */
public class MountPointTab extends SubView
{
    private ContentPanel panel = new ContentPanel();

    private MountPointModel model = new MountPointModel();
    private SubController controller = new MountPointController(this, model);

    private BaseEditorGrid<MountPointModelData> grid = new BaseEditorGrid<MountPointModelData>(this.model
            .getMountPointStore(), getColumnModel());

    public SubController init()
    {
        return controller;
    }

    private ColumnModel getColumnModel()
    {
        ArrayList<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        ColumnConfig storageColumn = GridUtils.addColumnConfig(columns, MountPointModelData.STORAGE_NAME_FIELD,
                "Storage Name");
        storageColumn.setEditor(new CellEditor(new TextField<String>()));

        ColumnConfig osColumn = GridUtils.addColumnConfig(columns, MountPointModelData.OPERATING_SYSTEM_FIELD,
                "Operating System");
        osColumn.setEditor(new CellEditor(new TextField<String>()));

        ColumnConfig pathColumn = GridUtils.addColumnConfig(columns, MountPointModelData.MOUNT_POINT_PATH_FIELD,
                "Mount point");
        pathColumn.setEditor(new CellEditor(new TextField<String>()));
        return new ColumnModel(columns);
    }

    public void initWidgets()
    {
        super.initWidgets();
        panel.setLayout(new FitLayout());
        panel.setHeaderVisible(false);
        panel.add(grid);
        this.addCenter(panel);
        this.setToolBar();
        this.addToToolBar(new ToolBarButton(Hd3dImages.getAddIcon(), "Add mount point",
                MountPointEvents.ADD_MOUNTPOINT_CLICKED));
        this.addToToolBar(new ToolBarButton(Hd3dImages.getDeleteIcon(), "Delete mount point",
                MountPointEvents.DELETE_MOUNTPOINT_CLICKED));
        this.setGridListener();
    }

    private void setGridListener()
    {
        grid.setClicksToEdit(ClicksToEdit.TWO);
        grid.setSelectionModel(new MultiLineCellSelectionModel<MountPointModelData>(
                (SelectableGridView) grid.getView(), null));

        grid.addListener(Events.AfterEdit, new Listener<BaseEvent>() {
            @Override
            public void handleEvent(BaseEvent be)
            {
                EventDispatcher.forwardEvent(MountPointEvents.AFTER_EDIT);
            }
        });
    }

    protected void setStyle()
    {
        setHeading(AdminMainView.CONSTANTS.MountPointTabHeader());
        super.setStyle();
    }

    public void displayMountPointCreator()
    {
        MountPointCreatorDisplayer.show(MountPointEvents.REFRESH_CLICKED);
    }

    public List<MountPointModelData> getMountPointSelected() throws Hd3dException
    {
        List<MountPointModelData> selection = this.grid.getSelectionModel().getSelection();
        if (selection == null || selection.isEmpty())
        {
            throw new Hd3dException("No data selected.");
        }
        return selection;
    }

    public void confirmDelete(final EventType confirmeDeleteEvent)
    {
        MessageBox.confirm("Warning", "Are you sure to delete this mount point?", new Listener<MessageBoxEvent>() {

            @Override
            public void handleEvent(MessageBoxEvent be)
            {
                Button yesButton = be.getDialog().getButtonById(Dialog.YES);
                if (be.getButtonClicked().equals(yesButton))
                {
                    EventDispatcher.forwardEvent(confirmeDeleteEvent);
                }
            }

        });
    }

    public void deselectGrid()
    {
        this.grid.getSelectionModel().deselectAll();
    }
}
