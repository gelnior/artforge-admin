package fr.hd3d.admin.ui.client.users.model;

import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.widget.form.TextField;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.OrConstraint;
import fr.hd3d.common.ui.client.service.store.BaseStore;


public class PersonFilterField extends TextField<String>
{
    private final BaseStore<PersonModelData> store;

    private final Constraint loginConstraint = new Constraint(EConstraintOperator.like,
            PersonModelData.PERSON_LOGIN_FIELD, "%", null);
    private final Constraint firstNameConstraint = new Constraint(EConstraintOperator.like,
            PersonModelData.PERSON_FIRST_NAME_FIELD, "%", null);
    private final Constraint lastNameConstraint = new Constraint(EConstraintOperator.like,
            PersonModelData.PERSON_LAST_NAME_FIELD, "%", null);
    private final OrConstraint or = new OrConstraint(loginConstraint, new OrConstraint(firstNameConstraint,
            lastNameConstraint));

    public PersonFilterField(BaseStore<PersonModelData> store)
    {
        this.store = store;
    }

    @Override
    protected void onKeyUp(FieldEvent fe)
    {
        super.onKeyUp(fe);
        loginConstraint.setLeftMember(getRawValue() + "%");
        firstNameConstraint.setLeftMember(getRawValue() + "%");
        lastNameConstraint.setLeftMember(getRawValue() + "%");
        store.addParameter(or);
        store.reload();
        store.removeParameter(or);
    }
}
