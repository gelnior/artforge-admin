package fr.hd3d.admin.ui.client.view.listener;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.widget.form.ComboBox;

import fr.hd3d.common.ui.client.listener.EventRelay;


/**
 * A plain listener specifically designed for combo boxes.
 * 
 * @author HD3D
 * 
 */
public class ComboRelay extends EventRelay
{
    ComboBox<?> parent;

    public ComboRelay(EventType type, ComboBox<?> parent)
    {
        super(type, null);
        this.parent = parent;
    }

    public void handleEvent(BaseEvent be)
    {
        data = parent.getValue();
        super.handleEvent(be);
    }
}
