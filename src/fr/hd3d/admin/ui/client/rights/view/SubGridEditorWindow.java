package fr.hd3d.admin.ui.client.rights.view;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;

import fr.hd3d.admin.ui.client.view.AdminMainView;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;


public class SubGridEditorWindow extends Dialog
{

    private TextField<String> text = new TextField<String>();
    private LabelToolItem label = new LabelToolItem();

    protected SubGridEditorWindow(String title, EventType type, String name)
    {
        setHeading(title);
        setStyle();
        setHideOnButtonClick(true);
        setModal(true);
        if (name != null)
        {
            text.setValue(name);
        }
        setButtons(Dialog.OKCANCEL);
        Button ok = getButtonById(OK);
        ok.addListener(Events.Select, new SubGridEditorWindowListener(type));
        Button cancel = getButtonById(CANCEL);
        cancel.setText(AdminMainView.CONSTANTS.Cancel());
    }

    public static SubGridEditorWindow getCreationDialog(String title, EventType type)
    {
        SubGridEditorWindow dialog = new SubGridEditorWindow(title, type, null);
        return dialog;
    }

    public static SubGridEditorWindow getEditionDialog(String title, EventType type, String name)
    {
        SubGridEditorWindow dialog = new SubGridEditorWindow(title, type, name);
        return dialog;
    }

    private void setStyle()
    {
        label.setLabel(AdminMainView.MESSAGES.TypeText());
        add(label);

        text.setAllowBlank(false);
        text.setWidth(300);
        add(text);
    }

    class SubGridEditorWindowListener implements Listener<ButtonEvent>
    {
        private EventType type;

        public SubGridEditorWindowListener(EventType type)
        {
            this.type = type;
        }

        public void handleEvent(ButtonEvent be)
        {
            String value = text.getValue();
            AppEvent event;
            if (value != null)
            {
                event = new AppEvent(type);
                event.setData(value);
            }
            else
            {
                event = new AppEvent(CommonEvents.ERROR);
                event.setData(AdminMainView.MESSAGES.NullTextFieldError());
            }
            EventDispatcher.forwardEvent(event);
        }
    }

}
